# Changelog
This document keeps track of new features and important bug fixes. Features that are moved here from the [To Do](TODO.md) when they are done. Features currently being worked on are not added here until they are viable for release.

## Since Beta 2
* Added slide move
* Added ladders and grids
* Added the Inventory
* Added the Shop System

## Since Beta 0.1
* Courses 1, 2, 3 and 4 completely reworked
* Added: Courses 5, 6 and the Red Palace
* Added: Library
* Added: palette swap scripts
* Added: refactored physics for Bowsette, slippery ground support and smoother gravity system
* Added: Power Up System, used for the [Power Crowns](/docs/power_crowns.md)
* Added: gamepad support
* Added: Course Selector
* Added: improved game over screen, with a new song and your score
* Added: Fullscreen switch and ways to select different scaling sizes
* BUGFIX: switches no longer persist after a Game Over
* BUGFIX: circular platforms no longer get out of sync
* BUGFIX: message boxes now appear correctly when you compile with YYC
* BUGFIX: Bowsette no longer gets stuck in Surprise Blocks
* BUGFIX: solved a rare bug which caused a Mushroom to go down indefinitely 
* BUGFIX: Flip Blocks no longer ignore pause
* BUGFIX: Bowsette can no longer be kicked out of the screen by enemies
* BUGFIX: Zoo Bats no longer are always inactive depending on where you put them in the course
* BUGFIX: Fix some Bowsette sprites having parts with wrong colors