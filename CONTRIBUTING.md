![Contributing](images/contributing.png)

There are many ways to contribute to Super Bowsette and other Super Crown-related projects. This repository welcomes pull requests.

Remember to always check our issue tracker. Your suggestion or task could be already in the works by someone else, so you can join forces instead of doing duplicated work.

## Super Bowsette

Before you get started, it's a good idea to read the [Game Design Document](GDD.md). This will give you an overview of what this project is.

If you want to contribute with **ideas**, [you can make a new issue](https://gitlab.com/Kenshiro/bowsette/issues/new) describing them and ways to implement them.

If you want to contribute with **graphic or audio assets**, please take a look at [this file](ASSETS.md) first. You can find what resources we need at [this issue](https://gitlab.com/Kenshiro/bowsette/issues/17) for graphic assets, and [this issue](https://gitlab.com/Kenshiro/bowsette/issues/16) for audio assets.

If you want to contribute with **code**, you can do so by submitting merge requests. For more complex changes, make sure to make an issue detailing it first.

If you want to contribute by **improving the existing documentation**, you can do so by submitting pull requests. If you're completely unfamiliar with GitLab, consider [making a issue](https://gitlab.com/Kenshiro/bowsette/issues/new) detailing your changes, so we can review and add them to the repository.

## Other projects

We have [a page about other Bowsette and Super Crown-related projects](OTHERS.md). If you're also working on a project, or know about one that isn't listed there, consider submiting a pull request or [making an issue](https://gitlab.com/Kenshiro/bowsette/issues/new) asking to add them.

We also have [an page](IDEAS.md) which contains ideas that could be used for other Bowsette and Super Crown projects. Feel free to take anything from there and develop your own project.