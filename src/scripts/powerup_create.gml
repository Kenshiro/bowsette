/// powerup_create(name, palette, skill script, jump script)
var _name = argument0;
var _palette = argument1;
var _skill_script = argument2;
var _jump_script = argument3;
return pack(_name, _palette, _skill_script, _jump_script);
