///bowsette_custom_gravity(acceleration, max speed)

//!#import bowsette_*

var argument_arr = array_create(argument_count);
for (var i = 0; i < argument_count; i++) {
	argument_arr[i] = argument[i];
}
if (live_call_ext(argument_arr)) return live_result;

var _accel = argument0;
var _max_spd = argument1;
var _height;
var _feet, _middle;
_feet = bbox_bottom;
_middle = bbox_left + (bbox_right - bbox_left)/2;

var i, _plat, _solid;

if (vsp >= 0 && !spinning && !climbing)
{
    _solid = collision_line(bbox_left, _feet + 1, bbox_right, 
    _feet + 1, obj_solid, 1, 0);
    if (_solid == noone || (_solid.sprite_index == spr_block &&
    !_solid.visible))
    {
    	var _plat_list = global.bowsette_plat_list;
    	var _plat_count = collision_line_list2(bbox_left, _feet + 1, bbox_right, 
        _feet + 1, obj_plat, false, false, _plat_list);
        //var _plat_count = ds_list_size(_plat_list);
        _plat = collision_line(bbox_left, _feet + 1, bbox_right, 
        _feet + 1, obj_plat, 0, 0);
        var _meet_plat = false;
        if (_plat_count > 0)
        {
        	for (var i = 0; i < _plat_count; ++i)
        	{
        		_plat = _plat_list[| i];
        		if (_plat.bbox_top >= _feet)
        		{
        			_meet_plat = true;
        			break;
        		}
        	}
        }
        if (_plat_count <= 0 || !_meet_plat)
        {
            on_floor = false;
            if (!floating)
            {
                if (vsp < _max_spd)
                {
                    vsp += _accel;
                    
                    if (vsp > _max_spd)
                        vsp = _max_spd;
                }
                for (i = (vsp | 0); i >= 0; i--)
                {
                	//trace(i)
                    _solid = collision_line(bbox_left, _feet + i, bbox_right, 
                    _feet + i, obj_solid, 1, 0);
                    if (_solid == noone || (_solid.sprite_index == spr_block &&
                    !_solid.visible))
                    {
                        _plat = collision_line(bbox_left + 1, _feet + i, 
                        bbox_right - 1, _feet + i, obj_plat, 0, 0);
                        //show_debug_message(y)
                        if (_plat == noone  || _plat.bbox_top < _feet)
                        {
                            y += i;
                            break;
                        }
                        else
                        {
							bowsette_touch_floor();
                        }                        
                    }
                }
            }
        }
        else
        {
            bowsette_touch_floor();
        }
    }
    else
    {
        bowsette_touch_floor();
    }
}
//trace(vsp)
if (vsp < 0)
{
    on_floor = false;
    var _vsp = abs(vsp | 0);
    //trace (_vsp);
    for (i = _vsp; i >= 0; i--)
    {
        _solid = collision_line(bbox_left, bbox_top - i, bbox_right, 
        bbox_top - i, obj_solid, 1, 0);
        if (_solid == noone || (_solid.sprite_index == spr_block &&
        !_solid.visible))
        {
            y -= i;
            if (i != _vsp)
            {
                vsp = 0;
            }
            else
            {
                vsp += _accel;
            }
            break;
        }
        else
        {
            y -= i;
            var _new_solid = collision_line(bbox_left, bbox_top, bbox_right, 
                bbox_top, obj_solid, 1, 0);
            if (_new_solid != noone)
            {
                y = _new_solid.bbox_bottom + 1 + 62;
                if (_new_solid.object_index == obj_block || object_get_parent(_new_solid) == obj_block)
                {
                    with (_new_solid) surprise_block_bump();
                }
                vsp = 0;
            }
            break;
        }
    }
}
