///course_create(course name, course number, room, course song, song loop begin, song loop end, next course, quote, unlocked status)
var _course = array_create(9);
_course[C_NAME] = argument[0];
_course[C_NUMBER] = argument[1];
_course[C_ROOM] = argument[2];
_course[C_SONG] = argument[3];
_course[C_SONG_LOOP_START] = argument[4];
_course[C_SONG_LOOP_END] = argument[5];
_course[C_NEXT] = argument[6];
_course[C_QUOTE] = argument[7];
if (argument_count > 8)
_course[C_UNLOCKED] = argument[8];
return _course;
