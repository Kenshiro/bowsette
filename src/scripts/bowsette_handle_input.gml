/// bowsette_handle_input()
if (live_call()) return live_result;
var k_running = input_check(E_INPUT.RUN);
var k_jump = input_check(E_INPUT.JUMP);
var k_jump_pressed = input_check_pressed(E_INPUT.JUMP);
var k_jump_released = input_check_released(E_INPUT.JUMP);
var k_left = input_check(E_INPUT.LEFT);
var k_right = input_check(E_INPUT.RIGHT);
var k_crunch = input_check(E_INPUT.DOWN);
var k_fire = input_check(E_INPUT.SKILL);
var h; //used for slopes
if (k_running)
{
    my_speed = 4;
    running = true;
}
else
{
    my_speed = 3;
    running = false;
}
//x = 4288
//y  =896
if (k_crunch && on_floor)
{
    crunch = true;
    mask_index = spr_bowsette_crunched_mask;
    var _solid = collision_line(bbox_left + 1, bbox_bottom + 1, bbox_right - 1, bbox_bottom + 1, obj_solid, true, false);
    var _plat = collision_line(bbox_left + 1, bbox_bottom + 1, bbox_right - 1, bbox_bottom + 1, obj_plat, true, false);
    if (_solid == noone && _plat != noone && k_jump_pressed)
    {
        y += 3;
        if (_plat.object_index == obj_moving_platform && !_plat.horizontal)
            y += 3;
        vsp = 2;
        crunch = false;
        on_floor = false;
        airtime = max_airtime;
        floating = false;
    }
}
else
{
    mask_index = spr_bowsette_mask;
    crunch = false;
    //Handle jumping according to current power up
    script_execute(global.current_power_up[PowerUp.JUMP], k_jump, k_jump_released,
    k_jump_pressed);
    var _speed, _on_ice;
    _speed = my_speed;
    if (!on_floor || global.current_power_up == global.power_up[PowerUpType.ICE])
    {
        fric = 0;
        has_fric = false;
    }
    else
    {
        _on_ice = collision_line(bbox_left, bbox_bottom + 1, bbox_right, bbox_bottom + 1, obj_ice, true, false);
        if (_on_ice != noone)
            has_fric = true;
        else
        {
            has_fric = false;
            fric = 0;
        }
    }
    

    if (k_left && (x - my_speed > 0))
    {
        if place_free(x - my_speed, y)
        {
            if (place_free(x - my_speed, y + 6))
            {
                x -= my_speed;
                walking = true;
            }
            else// if (vsp >= 0)
            for (h = 6; h >= 0; h--)
            {
                if (place_free(x - my_speed, y + h))
                {
                    x -= my_speed;
                    if (!floating)
                    {
                    y += h;
                    while (collision_line(bbox_left, bbox_bottom + 1, bbox_right, 
                    bbox_bottom + 1, obj_solid, 1, 0) == noone)
                        y += 1;
                    }
                    walking = true;
                    break;
                }
            }
            
        }
        else// if (vsp >= 0)
        for (h = 0; h <= 6; h++)
        {
            if (place_free(x - my_speed, y - h))
            {
                x -= my_speed;
                y -= h;
                y += 1;
                walking = true;
                break;
            }
        }
        looking_right = false;
        
    }
    else if (!k_left && k_right && (x + my_speed < room_width))
    {
        if place_free(x + my_speed, y)
        {
            if (place_free(x + my_speed, y + 6))
            {
                x += my_speed;
                walking = true;
            }
            else// if (vsp >= 0)
            for (h = 6; h >= 0; h--)
            {
                if (place_free(x + my_speed, y + h))
                {
                    x += my_speed;
                    if (!floating)
                    {
                        y += h;
                        if (collision_point(x, y + 1, obj_solid, 1, 0) == noone)
                            y += 1;
                    }
                    walking = true;
                    break;
                }
            }
        }
        else// if (vsp >= 0)
        for (h = 0; h <= 6; h++)
        {
            if (place_free(x + my_speed, y - h))
            {
                x += my_speed;
                y -= h;
                while (collision_line(bbox_left, bbox_bottom + 1, bbox_right, 
                bbox_bottom + 1, obj_solid, 1, 0) == noone)
                    y += 1;
                walking = true;
                break;
            }
        }
        looking_right = true;
    }
    else
    {
        walking = false;
    }
}
//Handle fire breath according to current power up
script_execute(global.current_power_up[PowerUp.SKILL], k_fire);
