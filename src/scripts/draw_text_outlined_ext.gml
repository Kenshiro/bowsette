///draw_text_outlined(x, y, _string, color, outline_color, distance, sep, w)
var _x = argument0;
var _y = argument1;
var _str = argument2;
var _color = argument3;
var _outline_color = argument4;
var _distance = argument5;
var _sep = argument6;
var _w = argument7;
var _old_color = draw_get_color();
draw_set_color(_outline_color);
draw_text_ext(_x - _distance, _y, _str, _sep, _w);
draw_text_ext(_x + _distance, _y, _str, _sep, _w);
draw_text_ext(_x, _y - _distance, _str, _sep, _w);
draw_text_ext(_x, _y + _distance, _str, _sep, _w);
draw_set_color(_color);
draw_text_ext(_x, _y, _str, _sep, _w);
draw_set_color(_old_color);
