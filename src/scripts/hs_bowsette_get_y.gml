/// hs_bowsette_get_y():float

if (instance_exists(obj_bowsette))
    return obj_bowsette.y;

return -1;
