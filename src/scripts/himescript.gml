#define himescript
/*
HimeScript - a script solution for Super Bowsette
based on the Interpreters Guide, written by YellowAfterlife.
This should be extremely similar to TXR overall.

Usage should be really simple!
*/

gml_pragma("global", "hs_init();");

enum HsToken
{
    EOF = 0,
    OP = 1,
    PAR_OPEN = 2,
    PAR_CLOSE = 3,
    NUMBER = 3,
    IDENT = 5,
    COMMA = 6,
    RETURN = 7,
    IF = 8,
    ELSE = 9,
    STRING = 10,
    BRACE_OPEN = 11,
    BRACE_CLOSE = 12,
    SET = 13,
    UNOP = 14,
    WHILE = 15,
    DO = 16,
    FOR = 17,
    SEMICO = 18,
    BREAK = 19,
    CONTINUE = 20,
    VAR = 21
}

enum HsOp
{
    SET = -1,
    MUL = $01,
    FDIV = $02,
    FMOD = $03,
    IDIV = $04,
    ADD = $10,
    SUB = $11,
    EQ = $40,
    NE = $41,
    LT = $42,
    LE = $43,
    GT = $44,
    GE = $45,
    BAND = $50,
    BOR = $60,
    MAXP = $70
}

enum HsNode
{
    NUMBER = 1,
    IDENT = 2,
    UNOP = 3,
    BINOP = 4,
    CALL = 5,
    BLOCK = 6,
    RETURN = 7,
    DISCARD = 8,
    IF_THEN = 9,
    IF_THEN_ELSE = 10,
    STRING = 11,
    SET = 12,
    WHILE = 13,
    DO_WHILE = 14,
    FOR = 15,
    BREAK = 16,
    CONTINUE = 17,
    ARGUMENT = 18,
    ARGUMENT_COUNT = 19,
    SWITCH = 28
}

enum HsUnop
{
    NEGATE = 1,
    INVERT = 2,
    DISCARD = 7,
    JUMP = 8,
    JUMP_UNLESS = 9,
    STRING = 10
}

enum HsBuildFlag
{
    NO_OPS = 1
}

enum HsAction
{
    NUMBER = 1, //push(value)
    IDENT = 2, //push(name)
    UNOP = 3, //push(-pop()
    BINOP = 4, //a = pop(); b = pop(); push(binop(op, a, b))
    CALL = 5,
    RET = 6,
    DISCARD = 7,
    JUMP = 8,
    JUMP_UNLESS = 9,
    STRING = 10,
    SET_IDENT = 11,
    BAND = 12,
    BOR = 13,
    JUMP_IF = 14,
    LABEL = 22
}

#define hs_init
hs_parse_tokens = ds_list_create();
hs_compile_list = ds_list_create();
globalvar hs_function_map; hs_function_map = ds_map_create();
globalvar hs_exec_args; hs_exec_args = ds_list_create();
globalvar hs_function_error; hs_function_error = "";
global.hs_error_val = "";
globalvar hs_constant_map; hs_constant_map = ds_map_create();
#define hs_add_function
/// hs_function_map(function name:string, function script: script, arg count: int)->void
hs_function_map[? argument0] = pack(argument1, argument2);
#define hs_throw
/// hs_throw(error_text:string, position:int)->bool
hs_error = sfmt("% at position %", argument0, argument1);
return true;

#define hs_throw_at
/// hs_throw_at(error_text:string, token: token)->bool
var _token = argument1;
if (_token[0] == HsToken.EOF)
{
    return hs_throw(argument0, "<EOF>");
} 
else return hs_throw(argument0, _token[1]);

#define hs_parse
/// hs_parse(_str)

var _str = argument0;
var _length = string_length(_str);
var _out = hs_parse_tokens;

ds_list_clear(_out);

var _pos = 1;

while (_pos <= _length)
{
    var _start = _pos;
    var _char = string_ord_at(_str, _pos);
    _pos++;
    
    switch(_char)
    {
        case 32:
        case 9:
        case 13:
        case 10: 
            break;
        
        case ord(';'): ds_list_add(_out, pack(HsToken.SEMICO, _start)); break;    
        case ord('('): ds_list_add(_out, pack(HsToken.PAR_OPEN, _start)); break;
        case ord(')'): ds_list_add(_out, pack(HsToken.PAR_CLOSE, _start)); break;
        case ord('{'): ds_list_add(_out, pack(HsToken.BRACE_OPEN, _start)); break;
        case ord('}'): ds_list_add(_out, pack(HsToken.BRACE_CLOSE, _start)); break;
        case ord(','): ds_list_add(_out, pack(HsToken.COMMA, _start)); break;
        //case ord('+'): ds_list_add(_out, pack(HsToken.OP, _start, HsOp.ADD)); break;
        //case ord('-'): ds_list_add(_out, pack(HsToken.OP, _start, HsOp.SUB)); break;
        case ord('*'): ds_list_add(_out, pack(HsToken.OP, _start, HsOp.MUL)); break;
        case ord('/'): ds_list_add(_out, pack(HsToken.OP, _start, HsOp.FDIV)); break;
        case ord('%'): ds_list_add(_out, pack(HsToken.OP, _start, HsOp.FMOD)); break;
        //case ord('='): ds_list_add(_out, pack(HsToken.OP, _start, HsOp.SET)); break;
        
        case ord('+'):
            if (string_ord_at(_str, _pos) == ord('=')) // +=
            {
                _pos++;
                ds_list_add(_out, pack(HsToken.SET, _start, HsOp.ADD));
            }
            else
            {
                ds_list_add(_out, pack(HsToken.OP, _start, HsOp.ADD)); 
            }
            break;
        
        case ord('-'):
            if (string_ord_at(_str, _pos) == ord('=')) // -=
            {
                _pos++;
                ds_list_add(_out, pack(HsToken.SET, _start, HsOp.SUB));
            }
            else
            {
                ds_list_add(_out, pack(HsToken.OP, _start, HsOp.SUB)); 
            }
            break;
        
        case ord('!'):
            if (string_ord_at(_str, _pos) == ord('=')) //!=
            {
                _pos++;
                ds_list_add(_out, pack(HsToken.UNOP, _start, HsUnop.INVERT));
            }
            else
            {
                ds_list_add(_out, pack(HsToken.UNOP, _start, HsUnop.INVERT));
            }
            break;
            
        case ord('='):
            if (string_ord_at(_str, _pos) == ord('='))
            {
                _pos++;
                ds_list_add(_out, pack(HsToken.OP, _start, HsOp.EQ));
            }
            else
            {
                ds_list_add(_out, pack(HsToken.SET, _start, HsOp.SET));
            }
            break;
            
        case ord("<"):
            if (string_ord_at(_str, _pos) == ord('=')) //<=
            {
                _pos++;
                ds_list_add(_out, pack(HsToken.OP, _start, HsOp.LE));
            }
            else
            {
                ds_list_add(_out, pack(HsToken.OP, _start, HsOp.LT));
            }
            break;
            
        case ord(">"):
            if (string_ord_at(_str, _pos) == ord('=')) //>=
            {
                _pos++;
                ds_list_add(_out, pack(HsToken.OP, _start, HsOp.GE));
            }
            else
            {
                ds_list_add(_out, pack(HsToken.OP, _start, HsOp.GT));
            }
            break;
            
        case ord('|'):
            if (string_ord_at(_str, _pos) == ord('|')) // ||
            {
                _pos++;
                ds_list_add(_out, pack(HsToken.OP, _start, HsOp.BOR));
            }
            break;
            
        case ord('&'):
            if (string_ord_at(_str, _pos) == ord('&')) // ||
            {
                _pos++;
                ds_list_add(_out, pack(HsToken.OP, _start, HsOp.BAND));
            }
            break;
        
        case ord("'"): case ord('"'):
            while (_pos <= _length)
            {
                if (string_ord_at(_str, _pos) == _char) break;
                _pos ++;
            }
            if (_pos <= _length)
            {
                _pos++;
                ds_list_add(_out, pack(HsToken.STRING, _start, string_copy(_str, _start + 1, _pos - _start - 2)));
            } else hs_throw("Unclosed string starting at", _start);
            break;
            
        default:
            if (_char >= ord('0') && _char <= ord('9'))
            {
                var _pre_dot = true;
                while (_pos <= _length)
                {
                    _char = string_ord_at(_str, _pos);
                    if (_char == ord('.'))
                    {
                        if (_pre_dot)
                        {
                            _pre_dot = false;
                            _pos++;
                        }
                        else break;
                    } 
                    else if (_char >= ord('0') && _char <= ord('9'))
                    {
                        _pos++;
                    }
                    else break;
                }
                
                var _value = real(string_copy(_str, _start, _pos - _start));
                ds_list_add(_out, pack(HsToken.NUMBER, _start, _value));
            }
            else if (_char == ord('_') || (_char >= ord('a') && _char <= ord('z')) || (_char >= ord('A') && _char <= ord('Z')))
            {
                while (_pos <= _length)
                {
                    _char = string_ord_at(_str, _pos);
                    if (_char == ord('_') || (_char >= ord('a') && _char <= ord('z')) || (_char >= ord('A') && _char <= ord('Z')))
                    {
                        _pos++;
                    } else break;
                }
                
                var _name = string_copy(_str, _start, _pos - _start);
                switch(_name)
                {
                    case "mod": ds_list_add(_out, pack(HsToken.OP, _start, HsOp.FMOD)); break;
                    case "div": ds_list_add(_out, pack(HsToken.OP, _start, HsOp.IDIV)); break;
                    case "if": ds_list_add(_out, pack(HsToken.IF, _start)); break;
                    case "else": ds_list_add(_out, pack(HsToken.ELSE, _start)); break;
                    case "return": ds_list_add(_out, pack(HsToken.RETURN, _start)); break;
                    case "for": ds_list_add(_out, pack(HsToken.FOR, _start)); break;
                    case "while": ds_list_add(_out, pack(HsToken.WHILE, _start)); break;
                    case "break": ds_list_add(_out, pack(HsToken.BREAK, _start)); break;
                    case "continue": ds_list_add(_out, pack(HsToken.CONTINUE, _start)); break;
                    default: ds_list_add(_out, pack(HsToken.IDENT, _start, _name));
                }
            }
            else
            {
                ds_list_clear(_out);
                return hs_throw(sfmt("Unexpected character % ", ord(_char)), _start);
            }
    }   
}
ds_list_add(_out, pack(HsToken.EOF, -1));
return false;

#define hs_build_ast
hs_build_list = hs_parse_tokens;
hs_build_pos = 0;
hs_build_length = ds_list_size(hs_build_list);
hs_build_can_break = false;
hs_build_can_continue = false;
/*
if (hs_build_expression(0)) return true;
if (hs_build_pos < hs_build_length - 1)
{
    return hs_throw_at('Trailing data', hs_parse_tokens[| hs_build_pos]);
}
else return false;
*/

var _nodes = array_create(0);
var _found = 0;
while (hs_build_pos < hs_build_length - 1)
{
    if (hs_build_statement()) return true;
    _nodes[_found++] = hs_build_node;
}
hs_build_node = pack(HsNode.BLOCK, 0, _nodes);
return false;
#define hs_build_expression
/// hs_build_expression(flags:HsBuildFlag)
var _flags = argument0;
var _token = hs_build_list[| hs_build_pos++];

switch(_token[0])
{
    case HsToken.NUMBER: hs_build_node = pack(HsNode.NUMBER, _token[1], _token[2]); break;
    case HsToken.STRING: hs_build_node = pack(HsNode.STRING, _token[1], _token[2]); break;
    case HsToken.IDENT: 
        /* old
        hs_build_node = pack(HsNode.IDENT, _token[1], _token[2]); break;
        */
        
        var _other_token = hs_build_list[| hs_build_pos];
        if (_other_token[0] == HsToken.PAR_OPEN)
        {
            hs_build_pos++;
            var _args = array_create(0);
            var _argc = 0;
            
            var _fn = hs_function_map[? _token[2]];
            if (_fn == undefined) return hs_throw_at(sfmt("Unknown function '%'", _token[2]), _token);
            var _fn_script = _fn[0];
            var _fn_argc = _fn[1];
            
            var _closed = false;
            while (hs_build_pos < hs_build_length)
            {
                _other_token = hs_build_list[| hs_build_pos];
                if (_other_token[0] == HsToken.PAR_CLOSE)
                {
                    hs_build_pos++;
                    _closed = true;
                    break;
                }
                
                if (hs_build_expression(0)) return true;
                _args[_argc++] = hs_build_node;
                _other_token = hs_build_list[| hs_build_pos];
                if (_other_token[0] == HsToken.COMMA)
                {
                    hs_build_pos++;
                } 
                else if (_other_token[0] != HsToken.PAR_CLOSE)
                {
                    return hs_throw_at(sfmt("Expected a ',' or ')'"), _other_token);
                }
            }
            if (!_closed) return hs_throw_at(sfmt("Unclosed () after"), _token);
            //show_debug_message(`Function name: ${script_get_name(_fn[0])}`);
            if (_fn_argc >= 0 && _argc != _fn_argc) return hs_throw_at(sfmt("% takes % argument(s), got %", _token[2], _fn[1],_argc), _token);
            hs_build_node = pack(HsNode.CALL, _token[1], _fn_script, _args, _fn_argc);
        }
        else
            hs_build_node = pack(HsNode.IDENT, _token[1], _token[2]);
            
        break;
    case HsToken.PAR_OPEN:
        if (hs_build_expression(0)) return true;
        _token = hs_build_list[|hs_build_pos++];
        if (_token[0] != HsToken.PAR_CLOSE) return hs_throw_at("Expected a ')'", _token);
        break;
        
    case HsToken.OP:
        switch(_token[2])
        {
            case HsOp.ADD:
                if (hs_build_expression(HsBuildFlag.NO_OPS)) return true;
                break;
            case HsOp.SUB:
                if (hs_build_expression(HsBuildFlag.NO_OPS)) return true;
                hs_build_node = pack(HsNode.UNOP, _token[1], HsUnop.NEGATE, hs_build_node);
                break;
            default: return hs_throw_at('Expected an expression', _token);
        }
        break;
        
    case HsToken.UNOP:
        if (hs_build_expression(HsBuildFlag.NO_OPS)) return true;
        hs_build_node = pack(HsNode.UNOP, _token[1], _token[2], hs_build_node);
        break;
        
    default: return hs_throw_at('Expected an expression', _token);
}
if ((_flags & HsBuildFlag.NO_OPS) == 0)
{
    _token = hs_build_list[| hs_build_pos];
    if (_token[0] == HsToken.OP)
    {
        hs_build_pos++;
        if (hs_build_ops(_token)) return true;
    }
}
return false;

#define hs_build_statement
var _token = hs_build_list[| hs_build_pos++];
switch(_token[0])
{
    case HsToken.RETURN:
        if (hs_build_expression(0)) return true;
        hs_build_node = pack(HsNode.RETURN, _token[1], hs_build_node);
        break;
        
    case HsToken.IF:
        if (hs_build_expression(0)) return true;
        var _cond = hs_build_node;
        if (hs_build_statement()) return true;
        var _then = hs_build_node;
        
        var _other_token = hs_build_list[| hs_build_pos];
        if (_other_token[0] == HsToken.ELSE)
        {
            hs_build_pos++;
            if (hs_build_statement()) return true;
            hs_build_node = pack(HsNode.IF_THEN_ELSE, _token[1], _cond, _then, hs_build_node);
        }
        else
        {
            hs_build_node = pack(HsNode.IF_THEN, _token[1], _cond, _then);
        }
        break;
        
    case HsToken.BRACE_OPEN:
        var _nodes = array_create(0);
        var _found = 0;
        var _closed = false;
        while (hs_build_pos < hs_build_length)
        {
            var _other_token = hs_build_list[| hs_build_pos];
            if (_other_token[0] == HsToken.BRACE_CLOSE)
            {
                hs_build_pos++;
                _closed = true;
                break;
            }
            if (hs_build_statement()) return true;
            _nodes[_found++] = hs_build_node;
        }
        if (!_closed) return hs_throw_at("Unclosed {} starting", _token);
        hs_build_node = pack(HsNode.BLOCK, _token[1], _nodes);
        break;
        
    case HsToken.WHILE:
        if (hs_build_expression(0)) return true;
        var _cond = hs_build_node;
        if (hs_build_loop_body()) return true;
        hs_build_node = pack(HsNode.WHILE, _token[1], _cond, hs_build_node);
        break;
        
    case HsToken.FOR:
        var _other_token = hs_build_list[| hs_build_pos];
        var _par = (_other_token[0] == HsToken.PAR_OPEN);
        if (_par) hs_build_pos++;
        if (hs_build_statement()) return true;
        var _init = hs_build_node;
        if (hs_build_expression(0)) return true;
        var _cond = hs_build_node;
        _other_token = hs_build_list[| hs_build_pos];
        if (_other_token[0] == HsToken.SEMICO) hs_build_pos++;
        if (hs_build_statement()) return true;
        var _post = hs_build_node;
        
        if (_par)
        {
            _other_token = hs_build_list[| hs_build_pos];
            if (_other_token[0] != HsToken.PAR_CLOSE) return hs_throw_at("Expected a ')'", _other_token);
            hs_build_pos++;
        }   
        
        if (hs_build_loop_body()) return true;
        hs_build_node = pack(HsNode.FOR, _token[1], _init, _cond, _post, hs_build_node);
        break;
        
    case HsToken.BREAK:
        if (hs_build_can_break)
        {
            hs_build_node = pack(HsNode.BREAK, _token[1]);
        } 
        else return hs_throw_at("Can't 'break' here", _token);
        break;
        
    case HsToken.CONTINUE:
        if (hs_build_can_continue)
        {
            hs_build_node = pack(HsNode.CONTINUE, _token[1]);
        } 
        else return hs_throw_at("Can't 'continue' here", _token);
        break;
    default:
        hs_build_pos -= 1;
        if (hs_build_expression(HsBuildFlag.NO_OPS)) return true;
        var _expr = hs_build_node;
        switch(_expr[0])
        {
            case HsNode.CALL:
                hs_build_node = pack(HsNode.DISCARD, _expr[1], _expr);
                break;
                
            default:
                _other_token = hs_build_list[| hs_build_pos];
                if (_other_token[0] == HsToken.SET)
                {
                    hs_build_pos++;
                    if (hs_build_expression(0)) return true;
                    hs_build_node = pack(HsNode.SET, _other_token[1], _other_token[2], _expr, hs_build_node);
                }
                else
                    return hs_throw_at(sfmt("Expected an statement"), hs_build_node);
        }
}
_token = hs_build_list[| hs_build_pos];
if (_token[0] == HsToken.SEMICO) hs_build_pos++;
#define hs_build_ops
/// hs_build_ops(first)

var _nodes = ds_list_create();
ds_list_add(_nodes, hs_build_node);
var _ops = ds_list_create();
ds_list_add(_ops, argument0);

var _token;

while (1)
{
    if (hs_build_expression(HsBuildFlag.NO_OPS))
    {
        ds_list_destroy(_nodes);
        ds_list_destroy(_ops);
        return true;
    }
    ds_list_add(_nodes, hs_build_node);
    _token = hs_build_list[| hs_build_pos];
    if (_token[0] == HsToken.OP)
    {
        hs_build_pos++;
        ds_list_add(_ops, _token);
    } else break;
}

var _length = ds_list_size(_ops);
var _max_pri = (HsOp.MAXP >> 4);
var _pri = 0;
while (_pri < _max_pri)
{
    for (var i = 0; i < _length; ++i)
    {
        _token = _ops[| i];
        if ((_token[2] >> 4) != _pri) continue;
        _nodes[| i] = pack(HsNode.BINOP, _token[1], _token[2], _nodes[| i], _nodes[|i + 1]);
        ds_list_delete(_nodes, i + 1);
        ds_list_delete(_ops, i);
        _length -= 1;
        i -= 1;
    }
    _pri++;
}

hs_build_node = _nodes[| 0];
ds_list_destroy(_nodes);
ds_list_destroy(_ops);
return false;

#define hs_compile
/// hs_compile(source code:string)
if (hs_parse(argument0)) return undefined;
if (hs_build_ast()) return undefined;
var _out = hs_compile_list;
ds_list_clear(_out);
if (hs_compile_expression(hs_build_node)) return undefined;
var _length = ds_list_size(_out);
var _arr = array_create(_length);
for (var i = 0; i < _length; ++i) _arr[i] = _out[|i];
ds_list_clear(_out);
return _arr;

#define hs_build_loop_body
/// hs_build_loop_body()

var _could_break = hs_build_can_break;
var _could_continue = hs_build_can_continue;
hs_build_can_break = true;
hs_build_can_continue = true;
var _trouble = hs_build_statement();
hs_build_can_break = _could_break;
hs_build_can_continue = _could_continue;
return _trouble;

#define hs_compile_expression
/// hs_compile_expression(node)
var _node = argument0;
var _out = hs_compile_list;

switch(_node[0])
{
    case HsNode.NUMBER: ds_list_add(_out, pack(HsAction.NUMBER, _node[1], _node[2])); break;
    case HsNode.STRING: ds_list_add(_out, pack(HsAction.STRING, _node[1], _node[2])); break;
    case HsNode.IDENT: ds_list_add(_out, pack(HsAction.IDENT, _node[1], _node[2])); break;
    
    case HsNode.UNOP:
        if (hs_compile_expression(_node[3])) return true;
        ds_list_add(_out, pack(HsAction.UNOP, _node[1], _node[2]));
        break;
        
    case HsNode.BINOP:
        if (hs_compile_expression(_node[3])) return true;
        if (hs_compile_expression(_node[4])) return true;
        ds_list_add(_out, pack(HsAction.BINOP, _node[1], _node[2]));
        break;
        
    case HsNode.CALL:
        var _args = _node[3];
        var _argc = array_length_1d(_args);
        for (var i = 0; i < _argc; ++i)
        {
            if (hs_compile_expression(_args[i])) return true;
        }
        ds_list_add(_out, pack(HsAction.CALL, _node[1], _node[2], _argc));
        break;
        
    case HsNode.BLOCK:
        var _nodes = _node[2];
        var _count = array_length_1d(_nodes);
        for (var i = 0; i < _count; ++i)
        {
            if (hs_compile_expression(_nodes[i])) return true;
        }
        break;
    
    case HsNode.RETURN:
        if (hs_compile_expression(_node[2])) return true;
        ds_list_add(_out, pack(HsAction.DISCARD, _node[1]));
        break;
        
    case HsNode.DISCARD:
        if (hs_compile_expression(_node[2])) return true;
        ds_list_add(_out, pack(HsAction.DISCARD, _node[1]));
        break;
        
    case HsNode.IF_THEN:
        if (hs_compile_expression(_node[2])) return true;
        var _jmp = pack(HsAction.JUMP_UNLESS, _node[1], 0);
        ds_list_add(_out, _jmp);
        if (hs_compile_expression(_node[3])) return true;
        _jmp[@ 2] = ds_list_size(_out);
        break;
        
    case HsNode.IF_THEN_ELSE:
        if (hs_compile_expression(_node[2])) return true;
        var _jmp_else = pack(HsAction.JUMP_UNLESS, _node[1], 0);
        ds_list_add(_out, _jmp_else);
        if (hs_compile_expression(_node[3])) return true;
        var _jmp_then = pack(HsAction.JUMP, _node[1], 0);
        ds_list_add(_out, _jmp_then);
        _jmp_else[@ 2] = ds_list_size(_out);
        if (hs_compile_expression(_node[4])) return true;
        _jmp_then[@ 2] = ds_list_size(_out);
        break;
        
    case HsNode.SET:
    
        if (_node[2] == HsOp.SET)
        {
            if (hs_compile_expression(_node[4])) return true;
        }
        else
        {
            if (hs_compile_getter(_node[3])) return true;
            if (hs_compile_expression(_node[4])) return true;
            ds_list_add(_out, pack(HsAction.BINOP, _node[1], _node[2]));
        }
        if (hs_compile_setter(_node[3])) return true;
        break;
    
        /*if (hs_compile_expression(_node[3])) return true;
        var _expr = _node[2];
        switch(_expr[0])
        {
            case HsNode.IDENT:
                ds_list_add(_out, pack(HsAction.SET_IDENT, _node[2], _expr[2]));
                break;
            default: return hs_throw_at("Expression is not setable", _expr);
        }
        break;
        */
    case HsNode.WHILE:
        var _pos_cont = ds_list_size(_out);
        if (hs_compile_expression(_node[2])) return true;
        var _jmp = pack(HsAction.JUMP_UNLESS, _node[1], 0);
        ds_list_add(_out, _jmp);
        var _pos_start = ds_list_size(_out);
        if (hs_compile_expression(_node[3])) return true;
        ds_list_add(_out, pack(HsAction.JUMP, _node[1], _pos_cont));
        var _pos_break = ds_list_size(_out);
        _jmp[@ 2] = _pos_break;
        hs_compile_patch_break_continue(_pos_start, _pos_break, _pos_break, _pos_cont);
        break;
        
    case HsNode.FOR:
        if (hs_compile_expression(_node[2])) return true;
        var _pos_loop = ds_list_size(_out);
        if (hs_compile_expression(_node[3])) return true;
        var _jmp = pack(HsAction.JUMP_UNLESS, _node[1], 0);
        ds_list_add(_out, _jmp);
        var _pos_start = ds_list_size(_out);
        if (hs_compile_expression(_node[5])) return true;
        var _pos_cont = ds_list_size(_out);
        if (hs_compile_expression(_node[4])) return true;
        ds_list_add(_out, pack(HsAction.JUMP, _node[1], _pos_loop));
        var _pos_break = ds_list_size(_out);
        _jmp[@ 2] = _pos_break;
        hs_compile_patch_break_continue(_pos_start, _pos_break, _pos_break, _pos_cont);
        break;
        
    case HsNode.BREAK: ds_list_add(_out, pack(HsAction.JUMP, _node[1], -10)); break;
    case HsNode.CONTINUE: ds_list_add(_out, pack(HsAction.JUMP, _node[1], -11)); break;
    
    default: return hs_throw_at(sfmt("Cannot compile node type %", string(_node[0])), _node);    
}

return false;

#define hs_compile_patch_break_continue
/// hs_compile_break_continue(start pos, end pos, break pos, continue pos)

var _start = argument0;
var _till = argument1;
var _break = argument2;
var _continue = argument3;
var _out = hs_compile_list;
for (var i = _start; i < _till; ++i) 
{
    var _node = _out[|i];
    if (_node[0] == HsAction.JUMP)
    {
        switch (_node[2])
        {
            case -10: if (_break >= 0) _node[@2] = _break; break;
            case -11: if (_continue >= 0) _node[@2] = _continue; break;
        }
    }
}

#define hs_compile_setter
/// hs_compile_getter(node)
var _node = argument0;
var _out = hs_compile_list;

switch(_node[0])
{
    case HsNode.IDENT:
        var _identifier_name = _node[2];
        
        if (ds_map_exists(hs_constant_map, _identifier_name))
        {
            return hs_throw_at("You can't change the value of constants.", _node);
        }
        else
        {
            ds_list_add(_out, pack(HsAction.SET_IDENT, _node[1], _identifier_name));
        }
        
        return false;
        
    default: return hs_throw_at("Expression is not settable.", _node);
}

#define hs_compile_getter
/// hs_compile_getter(node)

var _node = argument0;
var _out = hs_compile_list;
var _value;

switch(_node[0])
{
    case HsNode.IDENT:
        var _identifier_name = _node[2];
        
        if (ds_map_exists(hs_constant_map, _identifier_name))
        {
            _value = hs_constant_map[? _identifier_name];
            if (is_string(_value))
            {
                ds_list_add(_out, pack(HsAction.STRING, _node[1], _value));
            }
            else
            {
                ds_list_add(_out, pack(HsAction.NUMBER, _node[1], _value));
            }
        }
        else
        {
            ds_list_add(_out, pack(HsAction.IDENT, _node[1], _identifier_name));
        }
        
        return false;
        
    default: return hs_throw_at("Expression is not gettable", _node);
}

#define hs_exec_exit
/// hs_exec_exit(error_text:string, action:array, stack:stack)->bool
ds_stack_destroy(argument2);
hs_throw(argument0, argument1[1]);
return undefined;
#define hs_execute
/// hs_execute(array<actions>):any
show_debug_message('Called!');
var _arr = argument0;
var _length = array_length_1d(_arr);
var _pos = 0;
var _stack = ds_stack_create();
while (_pos < _length)
{
    var _node = _arr[_pos++];
    switch(_node[0])
    {
        case HsAction.NUMBER: ds_stack_push(_stack, _node[2]); break;
        case HsAction.STRING: ds_stack_push(_stack, _node[2]); break;
        case HsAction.UNOP: 
            var _v = ds_stack_pop(_stack);
            var _new_bool;
            if (_v)
                _new_bool = false;
            else
                _new_bool = true;
            if (_node[2] == HsUnop.INVERT)
            {
                ds_stack_push(_stack, _new_bool);
            }
            else if (is_string(_v)) 
                return hs_exec_exit("Can't apply unary to string", _node, _stack);
            else
                ds_stack_push(_stack, -ds_stack_pop(_stack)); 
            break;
        case HsAction.BINOP:
            var _b = ds_stack_pop(_stack);
            var _a = ds_stack_pop(_stack);
            if (_node[2] == HsOp.EQ)
            {
                _a = (_a == _b);
            }
            else if (_node[2] == HsOp.NE)
            {
                _a = (_a != _b);
            }
            else if (is_string(_a) || is_string(_b))
            {
                if (_node[2] == HsOp.ADD)
                {
                    if (is_string(_a)) _a = string(_a);
                    if (is_string(_b)) _b = string(_b);
                    _a += _b;
                } else return hs_exec_exit(sfmt("Can't apply operator % to % and %.", _node[2], typeof(_a), typeof(_b)), _node, _stack);
            }
            else switch(_node[2])
            {
                case HsOp.ADD: _a += _b; break;
                case HsOp.SUB: _a -= _b; break;
                case HsOp.MUL: _a *= _b; break;
                case HsOp.FDIV: _a /= _b; break;
                case HsOp.FMOD: if (_b != 0) _a %= _b; else a = 0; break;
                case HsOp.IDIV: if (_b != 0) _a = _a div _b; else a = 0; break;
                case HsOp.LT: _a = (_a < _b); break;
                case HsOp.LE: _a = (_a <= _b); break;
                case HsOp.GT: _a = (_a > _b); break;
                case HsOp.GE: _a = (_a >= _b); break;
                default: return hs_exec_exit(sfmt("Can't apply operator %", string(_node[2])), _node, _stack);
            }
            ds_stack_push(_stack, _a);
            break;
            
        case HsAction.IDENT:
            var _v = variable_instance_get(id, _node[2]);
            if (is_real(_v) || is_int64(_v) || is_bool(_v))
            {
                ds_stack_push(_stack, _v);
            } else return hs_exec_exit(sfmt("Variable % is not a number", _node[2]), _node, _stack);
            break;
        
        case HsAction.SET_IDENT:
            variable_instance_set(id, _node[2], ds_stack_pop(_stack));
            break;
            
        case HsAction.CALL:
            var _args = hs_exec_args;
            ds_list_clear(_args);
            var i = _node[3];
            var _v;
            while (--i >= 0) _args[| i] = ds_stack_pop(_stack);
            hs_function_error = undefined;
            switch(_node[3])
            {
                case 0: _v = script_execute(_node[2]); break;
                case 1: _v = script_execute(_node[2], _args[| 0]); break;
                case 2: _v = script_execute(_node[2], _args[| 0], _args[| 1]); break;
                case 3: _v = script_execute(_node[2], _args[| 0], _args[| 1], _args[| 2]); break;
                //add more after
                
                default: return hs_exec_exit(sfmt("Too many arguments (%)", string(_node[3])), _node, _stack);
            }
            if (hs_function_error == undefined)
                ds_stack_push(_stack, _v);
            else
                return hs_exec_exit(hs_function_error, _node, _stack);
            break;
            
        case HsAction.RET: _pos = _length; break;
        case HsAction.DISCARD: ds_stack_pop(_stack); break;
        case HsAction.JUMP: _pos = _node[2]; break;
        case HsAction.JUMP_UNLESS:
            if (ds_stack_pop(_stack))
            {
                
            }
            else
            {
                _pos = _node[2];
            }
            break;
        case HsAction.JUMP_IF:
            if (ds_stack_pop(_stack)) _pos = _node[2];
            break;
            
        case HsAction.BAND:
            if (ds_stack_top(_stack))
            {
                ds_stack_pop(_stack);
            }
            else 
                _pos = _node[2];
            break;
            
        case HsAction.BOR:
            if (ds_stack_top(_stack))
            {
                _pos = _node[2];
            }
            else 
                ds_stack_pop(_stack);
            break;
        default: return hs_exec_exit(sfmt("Can't run action %", string(_node[0])), _node, _stack);
    }
}

var _result;
if (ds_stack_empty(_stack))
    _result = 0;
else
    _result = ds_stack_pop(_stack);
ds_stack_destroy(_stack);
hs_error = "";
return _result;