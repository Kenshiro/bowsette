/// particle_init_all()
//Snow particle
/* Thanks to NightFrost for making this */
var _p_snow = part_type_create();
//part_type_shape(_p_snow, pt_shape_snow);
part_type_sprite(_p_snow, spr_snow_flake5, false, false, true);
part_type_color_mix(_p_snow, c_white, $D0A0A0);
//part_type_orientation(_p_snow, 0, 360, 0, 1, false);
part_type_direction(_p_snow, 200, 250, 0, 2);
part_type_speed(_p_snow, 1, 1, 0, 0.1);
part_type_life(_p_snow, 1200, 1200);
part_type_scale(_p_snow, 1, 1);
//part_type_size(_p_snow, 0.05, 0.15, 0, 0.005);
part_type_alpha3(_p_snow, 1, 1, 1);
global.snow_particle = _p_snow;
var _p_snow_system = part_system_create();
part_system_depth(_p_snow_system, -7);
global.snow_particle_system = _p_snow_system;
