/// make_standin(_id, _object = obj_standin)
var _id = argument[0];
var _object; if (argument_count > 1) _object = argument[1]; else _object = obj_standin;
if (!instance_exists(_id)) exit;
var _is_bowsette = false;
if (_id.object_index == obj_bowsette)
    _is_bowsette = true;
var _standin = instance_create_v(_id.x, _id.y, _object, _id, _is_bowsette);
_standin.sprite_index = _id.sprite_index;
_standin.image_index = _id.image_index;
_standin.image_alpha = _id.image_alpha;
_standin.image_angle = _id.image_angle;
_standin.image_blend = _id.image_blend;
_standin.image_xscale = _id.image_xscale;
_standin.image_yscale = _id.image_yscale;
_standin.depth = _id.depth;
_standin.visible = _id.visible;
_standin.image_speed = 0;
if (argument_count < 2)
    instance_deactivate_object(_id);
return _standin;
