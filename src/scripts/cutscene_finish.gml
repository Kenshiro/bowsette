/// cutscene_finish()->Void
global.__cutscene = false;
global.movement_lock = false;
show_debug_message("Finishing cutscene.");
with(obj_cutscene_manager) instance_destroy();
