/// play_course_song(course:Course)->Void
var _course/*:course*/ = argument0;
show_debug_message("Playing a song.");
var _song_name = course_get_song_name(_course);
show_debug_message("Song name: " + _song_name);
var _song/*:song*/ = game_get_song(_song_name);
var _is_perfect_loop = song_get_perfect_loop(_song);
var _song_asset = song_get_asset(_song);
if (global.checkpoint && course_get_name(_course) == "Vanilla Ice") {
    _song_asset = song_course6_night;
    trace("Night!");
}
if (_is_perfect_loop)
{
    global.song = audio_play_sound(_song_asset, 0, true);
}
else
{
    global.song = audio_play_sound(_song_asset, 0, false);
}
