/// hs_bowsette_get_x():float
if (instance_exists(obj_bowsette))
    return obj_bowsette.x;

return -1;
