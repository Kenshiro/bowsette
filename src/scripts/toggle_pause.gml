///toggle_pause()
//audio_play_sound(snd_pause, 0, false);
if (!global.pause_lock)
{
    global.pause_lock = true;
    with(obj_controller) alarm[1] = 20;
    if (global.pause)
    {
        audio_sound_gain(global.song, 1 * global.music_volume / 100, 100);
        show_debug_message("Unpausing...");
        instance_destroy(obj_pause);
    }
    else
    {
        instance_create(0, 0, obj_pause);
        audio_sound_gain(global.song, 0.5 * global.music_volume / 100, 100);
        play_sound(snd_pause);
        show_debug_message("Pausing...");
    }
    global.pause = !global.pause;
}
