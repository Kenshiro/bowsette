///enemy_init()
has_created_text = false;
died = false;
my_exp = 50; //base exp
my_score = 50;
falling = false;
burned = false;
deactivable = true;
hp = 1;
distance_to_deactivate = 1000;
if (object_index == obj_mage_clown)
{
    distance_to_deactivate = 1500;
}
solid_enemy = true;
died_alpha = 0;
vsp = 0;
vsp_max = 5;
vsp_acc = 0.2;
on_floor = true;
damaged = false;
damaged_alarm = 0;
turn_cooldown = 0;
