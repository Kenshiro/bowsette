/// pause_menu_update_page():Void
var _book/*:book*/ = library_manager_get_current_book();
var _name = library_book_get_name(_book);
var _unlocked, _book_id;
show_debug_message("book name: " + string(_name));
switch(_name)
{
    case "Lands":
        _book_id = Book.LAND;
        break;
    case "Creatures":
        _book_id = Book.CREATURES;
        break;
    case "Palaces":
        _book_id = Book.PALACES;
        break;
}
_unlocked = get(global.page_unlocked[_book_id], _book[library_book_current_page_pos]);
trace("Unlocked status: " + string(_unlocked));
var _current_page = string(_book[library_book_current_page_pos] + 1);
var _page_count = string(library_book_get_page_count(_book));
page_number = _current_page + "/" + _page_count;
if (_unlocked)
{
    var _page/*:page*/ = library_book_get_current_page(_book);
    title = "Page " + _current_page + ": " + library_page_get_title(_page);
    content = library_page_get_text(_page);
    asset = library_page_get_asset(_page);
    trace("Book info: ", title, content);
    var _seen = get(global.page_read[_book_id], _book[library_book_current_page_pos]);
    if (!_seen)
    {
        //set(global.page_read[_book_id], _book.current_page_pos, true);
        save_system_read_page(_book_id, _book[library_book_current_page_pos]);
    }
}
unlocked_page = _unlocked;
