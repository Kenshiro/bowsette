/// get(result, ...)
//Thanks to FrostyCat for the idea and original implementation

var result = argument[0];
for (var i = 1; i < argument_count; i++) 
{
    var k = argument[i];
    if (is_string(k))
        result = ds_map_find_value(result, k);
    else
        result = result[k];
}
return result;  
  /*
  if (is_int32(k) || is_int64(k)) {
    k = real(k);
  }
  switch (typeof(result)+typeof(k)) {
    case "numbernumber":
      if (k < 0) {
        k += ds_list_size(result);
      }
      result = result[| k];
    break;
    case "numberstring":
      result = result[? k];
    break;

    case "arraynumber":
      if (k < 0) {
        k += array_length_1d(result);
      }
      result = result[k];
    break;

    default:
      show_error("Invalid index " + string(k) + " (" + string(i) + ")", true);
    break;
  }
  return result;*/


