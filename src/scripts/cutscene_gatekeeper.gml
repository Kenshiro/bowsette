/// cutscene_gatekeeper(ctx)->Int
var l_ctx = argument[0];
if (!is_array(l_ctx)) return array_create(11);
while (true) switch (l_ctx[1/* label */]) {
case 0/* [L1,c1] begin */:
    l_ctx[@3/* _state */] = 0;
    l_ctx[@4/* _gatekeeper_talking */] = 0;
    l_ctx[@5/* _phrase */] = 0;
    l_ctx[@6/* _talker */] = obj_c4_gatekeeper;
    obj_bowsette.walking = 1;
    audio_sound_gain(global.song, (0.3 * global.music_volume) / 100, 500);
case 1/* [L9,c1] while */:
    if (obj_bowsette.x == 12608) { l_ctx[@1/* label */] = 3/* end while */; continue; }
    obj_bowsette.x = approach(obj_bowsette.x, 12608, obj_bowsette.my_speed);
    obj_bowsette.walking = 1;
    l_ctx[@0/* yield */] = 1; l_ctx[@1/* label */] = 2; return true;
case 2/* [L13,c5] post yield */:
    l_ctx[@1/* label */] = 1/* while */; continue;
case 3/* [L9,c1] end while */:
    obj_bowsette.walking = 0;
    l_ctx[@0/* yield */] = 25; l_ctx[@1/* label */] = 4; return true;
case 4/* [L16,c1] post yield */:
    l_ctx[@4/* _gatekeeper_talking */] = 1;
    l_ctx[6/* _talker */].balloon = instance_create_v(l_ctx[6/* _talker */].x, l_ctx[6/* _talker */].y, obj_custom_txtballoon, l_ctx[6/* _talker */].phrase_unlocked[0]);
    l_ctx[@7/* _balloon */] = l_ctx[6/* _talker */].balloon;
    l_ctx[7/* _balloon */].textbox_sprite = spr_fancybox_white_bigger2;
case 5/* [L21,c1] while */:
    if (!instance_exists(l_ctx[7/* _balloon */])) { l_ctx[@1/* label */] = 7/* end while */; continue; }
    l_ctx[@0/* yield */] = 1; l_ctx[@1/* label */] = 6; return true;
case 6/* [L23,c5] post yield */:
    l_ctx[@1/* label */] = 5/* while */; continue;
case 7/* [L21,c1] end while */:
    l_ctx[@4/* _gatekeeper_talking */] = 0;
    l_ctx[@0/* yield */] = 5; l_ctx[@1/* label */] = 8; return true;
case 8/* [L26,c1] post yield */:
    l_ctx[6/* _talker */].image_xscale = -1;
    l_ctx[@0/* yield */] = 5; l_ctx[@1/* label */] = 9; return true;
case 9/* [L28,c1] post yield */:
    l_ctx[@10/* __repeat */] = 100;
case 10/* [L29,c1] check for */:
    if (l_ctx[10/* __repeat */] < 1) { l_ctx[@1/* label */] = 13; continue; }
    obj_c4_gatekeeper.x = approach(obj_c4_gatekeeper.x, 12832, 1);
    obj_c4_gatekeeper.y = approach(obj_c4_gatekeeper.y, 1536, 1);
    l_ctx[@0/* yield */] = 1; l_ctx[@1/* label */] = 11; return true;
case 11/* [L33,c5] post yield, [L29,c1] post for */:
    l_ctx[@10/* __repeat */] -= 1;
    l_ctx[@1/* label */] = 10; continue;
case 13/* [L29,c1] end for */:
    l_ctx[@0/* yield */] = 25; l_ctx[@1/* label */] = 14; return true;
case 14/* [L35,c1] post yield */:
    l_ctx[6/* _talker */].image_index = 1;
    l_ctx[6/* _talker */].floating = 0;
    l_ctx[@0/* yield */] = 35; l_ctx[@1/* label */] = 15; return true;
case 15/* [L38,c1] post yield */:
    l_ctx[@8/* _n */] = 0;
    l_ctx[6/* _talker */].destroying_blocks = 1;
    l_ctx[@10/* __repeat */] = array_length_1d(l_ctx[6/* _talker */].block);
case 16/* [L42,c1] check for */:
    if (l_ctx[10/* __repeat */] < 1) { l_ctx[@1/* label */] = 19; continue; }
    l_ctx[@9/* _block */] = instance_create_v(l_ctx[6/* _talker */].block[l_ctx[8/* _n */]].x, l_ctx[6/* _talker */].block[l_ctx[8/* _n */]].y, obj_not_solid, spr_gatekeeper_block);
    l_ctx[9/* _block */].image_index++;
    instance_destroy(l_ctx[6/* _talker */].block[l_ctx[8/* _n */]]);
    audio_play_sound(snd_gatekeeper_destroy_block, 0, 0);
    l_ctx[@8/* _n */]++;
    l_ctx[@0/* yield */] = 25; l_ctx[@1/* label */] = 17; return true;
case 17/* [L49,c5] post yield, [L42,c1] post for */:
    l_ctx[@10/* __repeat */] -= 1;
    l_ctx[@1/* label */] = 16; continue;
case 19/* [L42,c1] end for */:
    l_ctx[6/* _talker */].destroying_blocks = 0;
    l_ctx[6/* _talker */].image_index = 0;
    l_ctx[@0/* yield */] = 30; l_ctx[@1/* label */] = 20; return true;
case 20/* [L53,c1] post yield */:
    l_ctx[6/* _talker */].image_xscale = 1;
    l_ctx[@0/* yield */] = 25; l_ctx[@1/* label */] = 21; return true;
case 21/* [L55,c1] post yield */:
    l_ctx[@10/* __repeat */] = 100;
case 22/* [L56,c1] check for */:
    if (l_ctx[10/* __repeat */] < 1) { l_ctx[@1/* label */] = 25; continue; }
    l_ctx[6/* _talker */].x = approach(l_ctx[6/* _talker */].x, l_ctx[6/* _talker */].xstart, 1);
    l_ctx[6/* _talker */].y = approach(l_ctx[6/* _talker */].y, l_ctx[6/* _talker */].ystart, 1);
    l_ctx[@0/* yield */] = 1; l_ctx[@1/* label */] = 23; return true;
case 23/* [L60,c5] post yield, [L56,c1] post for */:
    l_ctx[@10/* __repeat */] -= 1;
    l_ctx[@1/* label */] = 22; continue;
case 25/* [L56,c1] end for */:
    l_ctx[@0/* yield */] = 5; l_ctx[@1/* label */] = 26; return true;
case 26/* [L62,c1] post yield */:
    l_ctx[6/* _talker */].floating = 1;
    l_ctx[@4/* _gatekeeper_talking */] = 1;
    l_ctx[6/* _talker */].balloon = instance_create_v(l_ctx[6/* _talker */].x, l_ctx[6/* _talker */].y, obj_custom_txtballoon, l_ctx[6/* _talker */].phrase_unlocked[1]);
    l_ctx[@7/* _balloon */] = l_ctx[6/* _talker */].balloon;
    l_ctx[7/* _balloon */].textbox_sprite = spr_fancybox_white_bigger2;
case 27/* [L68,c1] while */:
    if (!instance_exists(l_ctx[7/* _balloon */])) { l_ctx[@1/* label */] = 29/* end while */; continue; }
    l_ctx[@0/* yield */] = 1; l_ctx[@1/* label */] = 28; return true;
case 28/* [L70,c5] post yield */:
    l_ctx[@1/* label */] = 27/* while */; continue;
case 29/* [L68,c1] end while */:
    l_ctx[@0/* yield */] = 5; l_ctx[@1/* label */] = 30; return true;
case 30/* [L72,c1] post yield */:
    l_ctx[6/* _talker */].image_xscale = -1;
    l_ctx[@0/* yield */] = 5; l_ctx[@1/* label */] = 31; return true;
case 31/* [L74,c1] post yield */:
    l_ctx[@10/* __repeat */] = 300;
case 32/* [L75,c1] check for */:
    if (l_ctx[10/* __repeat */] < 1) { l_ctx[@1/* label */] = 35; continue; }
    l_ctx[6/* _talker */].x = approach(l_ctx[6/* _talker */].x, 14016, 2);
    l_ctx[6/* _talker */].y = approach(l_ctx[6/* _talker */].y, 1504, 2);
    l_ctx[@0/* yield */] = 1; l_ctx[@1/* label */] = 33; return true;
case 33/* [L79,c5] post yield, [L75,c1] post for */:
    l_ctx[@10/* __repeat */] -= 1;
    l_ctx[@1/* label */] = 32; continue;
case 35/* [L75,c1] end for */:
    instance_destroy(l_ctx[6/* _talker */]);
    audio_sound_gain(global.song, (1 * global.music_volume) / 100, 500);
default/* [L82,c67] end */: l_ctx[@0/* yield */] = 0; return false;
}

/*//!#gmcr
/// cutscene_gatekeeper()->Int
#gmcr
var _state = 0;
var _gatekeeper_talking = false;
var _phrase = 0;
var _talker = obj_c4_gatekeeper;
obj_bowsette.walking = true;
audio_sound_gain(global.song, 0.3 * global.music_volume / 100, 500);
while (obj_bowsette.x != 12608)
{
    obj_bowsette.x = approach(obj_bowsette.x, 12608, obj_bowsette.my_speed);
    obj_bowsette.walking = true;
    yield 1;
}
obj_bowsette.walking = false;
yield 25;
_gatekeeper_talking = true;
_talker.balloon = instance_create_v(_talker.x, _talker.y, obj_custom_txtballoon, _talker.phrase_unlocked[0]);
var _balloon = _talker.balloon;
_balloon.textbox_sprite = spr_fancybox_white_bigger2;
while (instance_exists(_balloon))
{
    yield 1;
}
_gatekeeper_talking = false;
yield 5;
_talker.image_xscale = -1;
yield 5;
repeat (100)// || obj_c4_gatekeeper.y != 1536)
{
    obj_c4_gatekeeper.x = approach(obj_c4_gatekeeper.x, 12832, 1);
    obj_c4_gatekeeper.y = approach(obj_c4_gatekeeper.y, 1536, 1);
    yield 1;
}
yield 25;
_talker.image_index = 1;
_talker.floating = false;
yield 35;
var _n = 0;
_talker.destroying_blocks = true;
var _block;
repeat(array_length_1d(_talker.block))
{
    _block = instance_create_v(_talker.block[_n].x,_talker.block[_n].y, obj_not_solid, spr_gatekeeper_block);
    _block.image_index++;
    instance_destroy(_talker.block[_n]);
    audio_play_sound(snd_gatekeeper_destroy_block, 0, false);
    _n++;
    yield 25;
}
_talker.destroying_blocks = false;
_talker.image_index = 0;
yield 30;
_talker.image_xscale = 1;
yield 25;
repeat(100)
{
    _talker.x = approach(_talker.x, _talker.xstart, 1);
    _talker.y = approach(_talker.y, _talker.ystart, 1);
    yield 1;
}
yield 5;
_talker.floating = true;
_gatekeeper_talking = true;
_talker.balloon = instance_create_v(_talker.x, _talker.y, obj_custom_txtballoon, _talker.phrase_unlocked[1]);
_balloon = _talker.balloon;
_balloon.textbox_sprite = spr_fancybox_white_bigger2;
while (instance_exists(_balloon))
{
    yield 1;
}
yield 5;
_talker.image_xscale = -1;
yield 5;
repeat(300)
{
    _talker.x = approach(_talker.x, 14016, 2);
    _talker.y = approach(_talker.y, 1504, 2);
    yield 1;
}
instance_destroy(_talker);
audio_sound_gain(global.song, 1 * global.music_volume / 100, 500);
//!#gmcr*/
