/// base_crown_init()
with(obj_bowsette)
{
    max_airtime = 60;
    breath_fire_time = 0;
    breath_fire_time_max = 45;
    breath_fire_recharge_time = 1 * room_speed;
    global.fire_power = 1;
    maximum_vsp = 12;
    spinning = false;
    v_accel = 0.35;
}
