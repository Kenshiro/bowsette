///bowsette_update_input()
var k_running = keyboard_check(ord('Z'));
var k_jump = keyboard_check(ord('X'));
var k_jump_released = keyboard_check_released(ord('X'));
var k_left = keyboard_check(vk_left);
var k_right = keyboard_check(vk_right);
var k_crunch = keyboard_check(vk_down);
var k_fire = keyboard_check(ord('C'));
var h; //used for slopes
if (k_running)
{
    my_speed = 4;
    running = true;
}
else
{
    my_speed = 3;
    running = false;
}

if (k_crunch && on_floor)
{
    crunch = true;
    mask_index = spr_bowsette_crunched_mask;
}
else
{
    mask_index = spr_bowsette_mask;
    crunch = false;
    if (k_jump_released)
        jump_lock = false;
    if (k_left && (x - my_speed > 0))
    {
        if place_free(x - my_speed, y)
        {
            if (place_free(x - my_speed, y + 6))
            {
                x -= my_speed;
                walking = true;
            }
            else for (h = 6; h >= 0; h--)
            {
                if (place_free(x - my_speed, y + h))
                {
                    x -= my_speed;
                    y += h;
                    walking = true;
                    break;
                }
            }
            
        }
        else for (h = 0; h < 6; h++)
        {
            if (place_free(x - my_speed, y - h))
            {
                x -= my_speed;
                y -= h;
                walking = true;
                break;
            }
            draw_set_color(c_red)
            depth = -200
            draw_point(x - my_speed, y - h)
        }
        looking_right = false;
        
    }
    else if (!k_left && k_right && (x + my_speed < room_width))
    {
        if place_free(x + my_speed, y)
        {
            if (place_free(x + my_speed, y + 6))
            {
                x += my_speed;
                walking = true;
            }
            else for (h = 6; h >= 0; h--)
            {
                if (place_free(x + my_speed, y + h))
                {
                    x += my_speed;
                    y += h;
                    walking = true;
                    break;
                }
            }
        }
        else for (h = 0; h < 6; h++)
        {
            if (place_free(x + my_speed, y - h))
            {
                x += my_speed;
                y -= h;
                walking = true;
                break;
            }
        }
        looking_right = true;
    }
    else
    {
        walking = false;
    }
    
    if (on_floor && k_jump && !jump_lock)
    {
        if (running)
            vspeed = -my_jump - 1;
        else
            vspeed = -my_jump;
        image_index = 0;
        image_speed = 0.1;
        jump_lock = true;
        audio_play_sound(snd_jump, 0, false);
    }
    if (vspeed < 0 && k_jump_released)
    {
        vspeed /= 2;
    }
    if (vspeed >= 0 && global.float_enabled)
    {
        if (k_jump)
        {
            if (airtime < max_airtime)
            {
                floating = true;
            }
            else
            {
                floating = false;
            }
        }
        else if (k_jump_released || floating)
        {
            airtime = max_airtime;
            floating = false;
        }
        if (floating)
        {
            vspeed = 0;
            gravity = 0;
            airtime++;
            if (airtime >= max_airtime)
                floating = false;
        }
    }
}
if (breath_fire)//else
{
    breath_fire_time++;
    if (breath_fire_time == 5)
    {
        var _fire_dir, _fire_y;
        _fire_y = y - 42;
        if (crunch) _fire_y += 16;
        else if (on_floor) _fire_y += 4;
        if (looking_right) _fire_dir = 1;
        else _fire_dir = -1;
        var _fire = instance_create_v(x + 15 * _fire_dir, _fire_y,
        obj_bowsette_fire, 7 * _fire_dir);
        audio_play_sound(snd_breath_fire, 0, false);
        _fire.image_xscale = _fire_dir;
    }
    if (breath_fire_time >= breath_fire_time_max)
    {
        breath_fire_time = 0;
        
        if ! (k_fire)
            breath_fire = false;
    }
}
else if (k_fire && global.fire_enabled)
{
    breath_fire = true;
}
