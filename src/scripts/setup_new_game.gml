/// setup_new_game(_difficulty)
var _difficulty = argument0;

global.current_course = game_get_course("Sweet Sweet Vibrancy");
global.switch_enabled = array_create(4);
global.coins = 0;
global.hp = 2;
global.score_points = 0;
global.level = 1;
global.exp_count = 0;
global.diamonds = 0;
global.diamond_collected = array_create(4);
global.diamonds_before_death = 0;
global.max_hp = 2;
global.checkpoint = false;
global.__cutscene = false;
global.pause = false;
global.lock_controls = false;
global.pause_lock = false;
global.double_jump = false;
global.fire_power = 1;
global.fire_enabled = true;
global.float_enabled = true;
global.hud_alpha = 1;
global.song = -1;
global.game_over = false;
global.night = false;
global.item_inventory = inventory_create(8);
global.onoff_pink_enabled = false;
global.onoff_orange_enabled = false;
global.onoff_cyan_enabled = false;

switch(_difficulty)
{
    default:
        global.live_count = 5;
        global.exp_max = global.level * 1000;
        break;
        
    case Difficulty.HARD:
        global.live_count = 3;
        global.exp_max = global.level * 2500;
        break;    
    
    case Difficulty.GODLIKE:
        global.live_count = 1;
        global.exp_max = global.level * 99999;
        global.hp = 1;
        global.max_hp = 1;
        break;
}
set_difficulty(_difficulty);
