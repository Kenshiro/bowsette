///bowsette_update_sprite()
if (damaged)
{
    sprite_index = global.bowsette_sprite[Spriteset.DAMAGED];
}
else if (breath_fire)
{
    sprite_index = global.bowsette_sprite[Spriteset.STANDING_BREATHFIRE];
    if (on_floor)
    {
        if (crunch)
        {
            sprite_index = global.bowsette_sprite[Spriteset.CRUNCHING_BREATHFIRE];
        }
        else if (walking)
        {
            if (running)
            {
                sprite_index = global.bowsette_sprite[Spriteset.RUNNING_BREATHFIRE];
                image_speed = 0.25;
            }
            else
            {
                sprite_index = global.bowsette_sprite[Spriteset.WALKING_BREATHFIRE];
                image_speed = 0.2;
            }
        }
        else
        {
            sprite_index = global.bowsette_sprite[Spriteset.STANDING_BREATHFIRE];
            image_speed = 0.1;
        }
    }
    else
    {
        image_speed = 0.2;
        if (vsp > 0)
            sprite_index = global.bowsette_sprite[Spriteset.FALLING_BREATHFIRE];
        else if (vsp < 0)
            sprite_index = global.bowsette_sprite[Spriteset.JUMPING_BREATHFIRE];
        else if (floating)
            sprite_index = global.bowsette_sprite[Spriteset.FLOATING_BREATHFIRE];
    }
}
else
{
    if (sliding)
    {
        sprite_index = global.bowsette_sprite[Spriteset.SLIDING];
    }
    else
    {
        if (on_floor)
        {
            if (crunch)
            {
                sprite_index = global.bowsette_sprite[Spriteset.CRUNCHING];
            }
            else if (walking)
            {
                if (running)
                {
                    sprite_index = global.bowsette_sprite[Spriteset.RUNNING];
                    image_speed = 0.25;
                }
                else
                {
                    sprite_index = global.bowsette_sprite[Spriteset.WALKING];
                    image_speed = 0.2;
                }
            }
            else
            {
                sprite_index = global.bowsette_sprite[Spriteset.STANDING];
                image_speed = 0.1;
            }
        }
        else
        {
            if (spinning)
            {
                sprite_index = global.bowsette_sprite[Spriteset.SPINNING];
                image_speed = 0.3;
            }
            else if (climbing)
            {
                sprite_index = global.bowsette_sprite[Spriteset.CLIMBING];
            }
            else
            {
                image_speed = 0.2;
                if (vsp > 0)
                    sprite_index = global.bowsette_sprite[Spriteset.FALLING];
                else if (vsp < 0)
                    sprite_index = global.bowsette_sprite[Spriteset.JUMPING];
                else if (floating)
                    sprite_index = global.bowsette_sprite[Spriteset.FLOATING];
            }
        }
    }
}
if (looking_right)
    image_xscale = 1;
else
    image_xscale = -1;

if (invulnerable)
{
    if (blink_alarm == 0)
        blink_alarm = 5;
        
    if (blink_alarm && !--blink_alarm)
    {
        if (image_alpha == 1)
        {
            image_alpha = 0.5;
        }
        else image_alpha = 1;
    }
    invulnerable_time++;
    if (invulnerable_time >= invulnerable_time_max)
    {
        invulnerable_time = 0;
        invulnerable = false;
        image_alpha = 1;
    }
}
