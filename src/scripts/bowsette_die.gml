///bowsette_die(?burned:Bool)->Instance
var _burned;
if (argument_count > 0)
    _burned = argument[0];
else
    _burned = false;
if (obj_controller.bowsette_died)
    return undefined;

global.live_count--;
global.current_power_up = global.power_up[PowerUpType.NORMAL];
obj_controller.bowsette_died = true;
var oo = instance_create_v(obj_bowsette.x, obj_bowsette.y, obj_bowsette_dead, _burned);
oo.image_xscale = obj_bowsette.image_xscale;
instance_destroy(obj_bowsette);
audio_play_sound(snd_death, 0, false);
return oo;
