/// move(speed: Float, move direction: Float)
/// @arg speed
/// @arg direction
 
var _spd = argument0;
var _dir = argument1;
 
var _xtarg = x+lengthdir_x(_spd, _dir);
var _ytarg = y+lengthdir_y(_spd, _dir);
 
if (place_free(_xtarg, _ytarg))
{
    x = _xtarg;
    y = _ytarg;
}
else
{
    var _sweep_interval = 10;
    for ( var _angle = _sweep_interval; _angle <= 90; _angle += _sweep_interval)
    {
        for ( var _multiplier = -1; _multiplier <= 1; _multiplier += 2)
        {      
            var _angle_to_check = _dir + _angle * _multiplier;
            _xtarg = x + lengthdir_x(_spd, _angle_to_check);
            _ytarg = y + lengthdir_y(_spd, _angle_to_check);     
            if (place_free(_xtarg, _ytarg))
            {
                x = _xtarg;
                y = _ytarg;  
                exit;       
            }   
        }
    }
}
