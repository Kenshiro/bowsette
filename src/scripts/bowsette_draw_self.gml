/// bowsette_draw_self()
//if (live_call()) return live_result;

if (!surface_exists(global.bowsette_surface))
    global.bowsette_surface = surface_create(96, 80);
var _old_alpha = draw_get_alpha();

draw_set_alpha(1)
surface_set_target(global.bowsette_surface);
draw_clear_alpha(c_white, 0);
shader_set(shader_colorchar);
texture_set_stage(shader_get_sampler_index(shader_colorchar, "texColmap"),
sprite_get_texture(global.current_power_up[PowerUp.PALETTE], 0));
draw_sprite_ext(sprite_index, image_index, 48, 80, image_xscale, image_yscale,
image_angle, c_white, 1);
//draw_rectangle(0, 0, 96, 80, 0)

shader_reset();
surface_reset_target();
draw_surface_ext(global.bowsette_surface, x - 48, y - 80, 1, 1,
0, c_white, image_alpha);


//draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale,
//image_angle, c_white, 0);
draw_set_alpha(_old_alpha);
