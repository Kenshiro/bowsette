/// cutscene_play()->Bool
if (__cutscene_timer && !--__cutscene_timer)
{
    if (script_execute(cutscene_script, __cutscene))
    {
        __cutscene_timer = __cutscene[0];
        return false;
    }
    else
    {
        //finish cutscene
        hud_fadein = true;
        return true;
    }
}
