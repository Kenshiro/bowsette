/// base_crown_handle_skill(_fire_key)
var _fire_key = argument0;
if (breath_fire)
{
    breath_fire_time++;
    if (breath_fire_time == 5)
    {
        var _fire_dir, _fire_y;
        _fire_y = y - 42;
        if (crunch) _fire_y += 16;
        else if (on_floor) _fire_y += 4;
        if (looking_right) _fire_dir = 1;
        else _fire_dir = -1;
        var _fire = instance_create_v(x + 15 * _fire_dir, _fire_y,
        obj_bowsette_fire,
        (7 + (global.current_power_up == global.power_up[PowerUpType.FIRE])) * _fire_dir);
        audio_play_sound(snd_breath_fire, 0, false);
        _fire.image_xscale = _fire_dir;
    }
    if (breath_fire_time >= breath_fire_time_max)
    {
        breath_fire_time = 0;
        
        if (!_fire_key)
            breath_fire = false;
    }
}
else if (_fire_key && global.fire_enabled && !spinning)
{
    breath_fire = true;
}
