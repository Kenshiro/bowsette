/// worldmap_hud_update(place_info:array<any>)->Void
//!#import worldmap_animation_state as WAS
var _place_info = argument0;
var _completed_status = "NOT COMPLETED";
if (_place_info[place.locked])
    _completed_status = "COMPLETED";
obj_worldmap_hud.text = _place_info[place.name] + "#STATUS: " + _completed_status;
obj_worldmap_hud.animation_state = worldmap_animation_state.INFORMATION_IN;
