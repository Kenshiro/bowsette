/// enemy_generic_movement(_avoid_pit)
//if (live_call(argument0)) return live_result;
var _avoid_pit = argument0;
var _old_x, _old_y, _col_pos, _enemy, h, _met_a_wall, _hit_room_border;
_old_x = x;
_old_y = y;
if (hs > 0)
{
    _col_pos = bbox_right;
    _hit_room_border = (_col_pos >= room_width);
}
else
{
    _col_pos = bbox_left;
    _hit_room_border = (_col_pos <= 0);
}
_met_a_wall = (collision_line(_col_pos + hs, bbox_top + 1, _col_pos + hs, bbox_bottom - 1, obj_solid, true, false) != noone);
_enemy = collision_line(_col_pos + hs, bbox_top + 1, _col_pos + hs, bbox_bottom - 1, 
obj_araikuma, 0, true);
if (_hit_room_border || (_avoid_pit && place_free(_col_pos + hs * 2, y + 8) && 
collision_point(_col_pos + hs * 4, bbox_bottom + 8, obj_plat, 0, 0) == noone && on_floor) ||
(collision_line(_col_pos, bbox_top + 1, _col_pos, bbox_bottom - 1, obj_solid, 0, 0) != noone && on_floor) ||
(_enemy != noone && !_enemy.died && _enemy.solid_enemy) || _met_a_wall)
{
    hs *= -1;
    image_xscale *= -1;
}

var _hs = hs;
_hs += rhs;
_hs2 = floor(_hs);
rhs = _hs - _hs2;
//x += _hs2;
rhs -= floor(rhs);
var _destiny_x = x + _hs2;
if (place_free(_destiny_x, y))
{
    if (place_free(_destiny_x, y + 6))
    {
        x = _destiny_x;
    }
    else
    for (h = 4; h >= 0; --h)
    {
        if (place_free(_destiny_x, y + h))
        {
            x = _destiny_x;
            y = y + h;
            while (collision_line(bbox_left, bbox_bottom + 1, bbox_right, bbox_bottom + 1, obj_solid, 1, 0) == noone)
                y += 1;
            break;
        }
    }
}
else
{
    for (h = 1; h <= 8; ++h)
    {
        if (place_free(_destiny_x, y - h))
        {
            x = _destiny_x;
            y = y - h;
                while (collision_line(bbox_left, bbox_bottom + 1, bbox_right, bbox_bottom + 1, obj_solid, 1, 0) == noone)
                    y += 1;
            break;
        }
    }
}
