/// enemy_check_fire_collision(?is fire insta kill)
//for common enemies.
var _fire_instakill;
if (argument_count >= 1)
    _fire_instakill = argument[0];
else
    _fire_instakill = false;
var _killed = false;

var _fire = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom,
obj_bowsette_fire, false, false);
if (_fire != noone)
{
    if (variable_instance_exists(id, "hp") && !_fire_instakill)
    {
        hp -= _fire.damage;
        if (hp > 0)
        {
            damaged = true;
            damaged_alarm = 6;
        }
        else
        {
            _killed = true;
        }
    }
    else
    {
        _killed = true;
    }
    
    if (_killed)
    {
        burned = true;
        add_exp(my_score);
        hspeed = 0;
        vspeed = 0;
        died = true;
    }
    instance_destroy(_fire);    
    return true;
}

return false;
