/// update_resolution()
switch(argument0)
{
    case 0:
        window_set_size(480, 270);
        display_set_gui_size(480, 270);
        show_debug_message("Switching to 270p.");
        break;
    case 1:
        window_set_size(960, 540);
        display_set_gui_size(960, 540);
        show_debug_message("Switching to 540p.");
        break;
    case 2:
        window_set_size(1920, 1080);
        display_set_gui_size(1920, 1080);
        show_debug_message("Switching to 1080p.");
        break;
}
update_font_size();
