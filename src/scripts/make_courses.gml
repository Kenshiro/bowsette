/// make_courses()
global.current_course_number = 0;
global.bonus_course_number = 0;
global.bonus_course = false;
global.checkpoint = false;
global.pipe = false;

global.switch_enabled = array_create(4);
//global.bonus_courses = array_create(4);
global.current_course = game_get_course("Brand New Grassland");
//global.courses = array_create(10);
/*global.Course.name = pack("Brand New Grassland", "Mysterious Cave",
"Princess in the Sky with Diamonds", "Stay the Night", "TBD",
"Vanilla Ice", "The Fragrance of Black Coffee", "TBD", "Kinoko Teikoku");
global.bonus_course_name = pack("Yellow Palace", "Blue Palace", "Red Palace",
"Green Palace");*/
/*
var arr0 = array_create(2);
arr0[0] = "A walk in the park. No need to rush things.";
arr0[1] = "Take it easy, R-E-L-A-X. Wanna have something to drink?"
global.courses[0] = new Course(
    "Brand New Grassland", 
    1, 
    rm_course1,
    song_course1, 
    -1, 
    -1,
    1,
    arr0,
    course_unlocked[1]);

var arr1 = array_create(1);
arr1[0] = "I should've brought a Super Repel.";
global.courses[1] = new Course(
    "Mysterious Cave", 
    2, 
    rm_course2,
    song_cave, 
    -1, 
    -1,
    1,
    arr1,
    course_unlocked[2]);

var arr2 = array_create(2);
arr2[0] = "My heart, my wings... let us fly!";
arr2[1] = "Don't worry, these diamonds are unbreakable!";
global.courses[2] = new Course(
    "Princess in the Sky with Diamonds",
    3,
    rm_course3,
    song_course3_placeholder,
    -1,
    -1,
    3,
    arr2,
    course_unlocked[3]);
    
var arr3 = array_create(3);
arr3[0] = "On the night that left me scarred, you saved my life";
arr3[1] = "Nightly reminder that love always wins."
arr3[2] = "If no one is special, maybe you can be what you want to be.# Your life is your own, OK?";
global.courses[3] = new Course(
    "Stay the Night",
    4,
    rm_course4,
    song_course4_placeholder,
    -1,
    -1,
    4,
    arr3,
    course_unlocked[4]);
    
var arr4 = array_create(3);
arr4[0] = "Blacker than a moonless night, hotter and more bitter than hell itself.#That is coffee.";
arr4[1] = "Remember to smile no matter how bad it gets."
arr4[2] = "Bit by bit, my heart was charmed by that radiant smile.";
global.courses[4] = new Course(
    "The Fragrance of Black Coffee",
    5,
    rm_course5,
    song_course4_placeholder,
    -1,
    -1,
    5,
    arr4,
    course_unlocked[5]);
    
var arr5 = array_create(1);
arr5[0] = "Beware the miasma of the void... Vanilla Ice.";
global.courses[5] = new Course(
    "Vanilla Ice",
    6,
    rm_course6,
    song_course4_placeholder,
    -1,
    -1,
    3,
    arr5,
    course_unlocked[6]);
    
global.bonus_courses[0] = new Course(
    "Yellow Palace",
    11,
    rm_yellow_palace,
    song_bonus_course,
    -1,
    -1,
    1,
    "");
    
global.bonus_courses[1] = new Course(
    "Red Palace",
    12,
    rm_red_palace,
    song_red_heels,
    -1,
    -1,
    1,
    "");
    
global.bonus_courses[2] = new Course(
    "Blue Palace",
    13,
    rm_blue_palace,
    song_red_heels,
    -1,
    -1,
    1,
    "");
*/
