/// surprise_block_bump()
if (obj_bowsette.vsp <= 0)
{
    show_debug_message("Collided, vspeed was <= 0!")
    if (!moved)
    {
        if (!dead && !jumping && !falling)
        {
            jumping = true;
            var _mushroom = collision_line(x, y - 1, x + sprite_width, y - 1,
            obj_base_mushroom, false, false);
            if (_mushroom != noone)
            {
                _mushroom.vsp = -6;
                _mushroom.y -= 3;
            }
        }
        if (!visible)
        {
            visible = true;
            solid = true;
            while (collision_line(bbox_left, bbox_bottom + 1, bbox_right, 
            bbox_bottom + 1, obj_bowsette, 0, 0) != noone)
                obj_bowsette.y += 1;
        }
        
        /*while (collision_line(bbox_left, bbox_bottom, 
        bbox_right, bbox_bottom, obj_bowsette, 0, 0) != noone)*/
        while (obj_bowsette.bbox_top < y + sprite_height)
            obj_bowsette.y += 1;
        obj_bowsette.vsp = 0;
        moved = true;
    }
    if ! (already_colided)
    {
        audio_play_sound(snd_bump, 0, false);
        already_colided = true;
    }
}
