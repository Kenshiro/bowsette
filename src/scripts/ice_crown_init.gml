/// ice_crown_init()
if (live_call()) return live_result;
with(obj_bowsette)
{
    max_airtime = 60;
    breath_fire_time = 0;
    breath_fire_time_max = 45;
    breath_fire_recharge_time = 1 * room_speed;
    global.fire_power = 1;
    maximum_vsp = 12;
    spinning = false;
    v_accel = 0.35;
    //transformation
    var _system = global.snow_particle_system;
    var _emitter = part_emitter_create(_system);
    var _particle = global.snow_particle;
    part_emitter_region(_system, _emitter, bbox_left - 10, bbox_right + 10, bbox_top - 10, bbox_bottom + 10, ps_shape_diamond, ps_distr_linear);
    part_type_color_mix(_particle, make_color_rgb(144, 208, 255), make_color_rgb(64, 96, 248)); //blue colors that Ice Bowsette uses
    part_type_direction(_particle, 265, 275, 0, 3);
    part_type_speed(_particle, 1.5, 3, 0, 1);
    part_emitter_burst(_system, _emitter, _particle, 20);
    part_emitter_destroy(_system, _emitter);
    //reset the snow particle
    part_type_color_mix(_particle, c_white, $D0A0A0);
    part_type_direction(_particle, 200, 250, 0, 2);
    part_type_speed(_particle, 1, 1, 0, 0.1);
}
