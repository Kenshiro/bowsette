/// update_font_size()
var _screen_width, _screen_height, _font_course, _font_flavor;
switch(display_get_gui_width())
{
    case 1920:
        _screen_width = 1920;
        _screen_height = 1080;
        _font_course = global.vinizinho_48;
        _font_flavor = global.vinizinho_24i;
        global.scale = 2;
    break;
    
    case 960:
        _screen_width = 960;
        _screen_height = 540;
        _font_course = global.vinizinho_24;
        _font_flavor = global.vinizinho_18i;
        global.scale = 1;
    break;
    
    case 480:
        _screen_width = 480;
        _screen_height = 270;
        _font_course = global.vinizinho_12;
        _font_flavor = global.vinizinho_12i;
        global.scale = 0.5;
    break;
}

global.font_course_name = _font_course;
global.font_flavor = _font_flavor;
global.font_textbox = _font_course;
global.screen_width = _screen_width;
global.screen_height = _screen_height;
