/// draw_text_outlined_alt(x, y, _string, color, outline_color, ?distance)
var _x = argument[0];
var _y = argument[1];
var _str = argument[2];
var _color = argument[3];
var _outline_color = argument[4];
if (argument_count > 5)
    var _distance = argument[5];
else
    var _distance = 1;
var _old_color = draw_get_color();
draw_set_color(_outline_color);
draw_text(_x - _distance, _y  - _distance, _str);
draw_text(_x + _distance, _y  - _distance, _str);
draw_text(_x - _distance, _y  + _distance, _str);
draw_text(_x + _distance, _y  + _distance, _str);
draw_set_color(_color);
draw_text(_x, _y, _str);
draw_set_color(_old_color);
