/// fire_crown_handle_jump(_key_jump, _key_jump_released, jump pressed)
var argument_arr = array_create(argument_count);
for (var i = 0; i < argument_count; i++) {
argument_arr[i] = argument[i];
}
if (live_call_ext(argument_arr)) return live_result;
var _key_jump = argument0;
var _key_jump_released = argument1;
var _key_jump_pressed = argument2;
if (_key_jump_released)
    jump_lock = false;
    
if ((on_floor || (climbing && climbing_cooldown == 0)) && _key_jump && !jump_lock)
{
    if (running)
        vsp = -my_jump - 1;
    else
        vsp = -my_jump;
    jump_count = 1;
    _key_jump_pressed = false;
    image_index = 0;
    image_speed = 0.1;
    jump_lock = true;
    climbing = false;
    climbing_cooldown = 8;
    audio_play_sound(snd_jump, 0, false);
}
if (vsp < 0)
{
    if (_key_jump_released)
        vsp /= 2;
}
if (vsp >= 0 && global.float_enabled)
{
    if (_key_jump && !on_floor)
    {
        airtime = max_airtime;
        maximum_vsp = 3;
        floating = false;
        if (!instance_exists(obj_firecrown_glide))
        {
            instance_create(x, y, obj_firecrown_glide);
        }
    }
    else
    {
        maximum_vsp = 12;
        if (_key_jump_released || floating)
        {
            airtime = max_airtime;
            floating = false;
        }
    }
    if (floating)
    {
        vsp = 0;
        airtime++;
        if (airtime >= max_airtime)
            floating = false;
    }
}
if (_key_jump_pressed && !on_floor)
{
    if (jump_count <= 1)
    {
        if (running)
            vsp = -round(my_jump) - 1;
        else
            vsp = -round(my_jump);
        jump_count = 2;
        instance_create(x, y, obj_firecrown_firejump);
    }
    else
    {
        if (spin_time == -1 && !breath_fire)
        {
            spinning = true;
            image_index = 0;
            created_fire_balls = false;
            audio_play_sound(snd_fire_spin, 0, false);
            if (vsp > 0)
                vsp = 0;
                //vsp = vsp | 0;
            spin_time = 35;//spin_time_max;
            maximum_vsp = 1;
        }
    }
}
//trace(created_fire_balls)
if (spinning)
{
if ((image_index | 0) == 3 && !created_fire_balls)
{
    instance_create_v(x + 20, y - 37, obj_firecrown_fireball, 3);
    instance_create_v(x - 20, y - 37, obj_firecrown_fireball, -3);
    created_fire_balls = true;
}
}
if (spin_time && !--spin_time)
{
    spin_time = -1;
}
