/// keyboard_key_code(keyname:string)
var k = global.keyboard_name_to_key[?string_lower(argument0)];
if (k == undefined) return -1;
return k;
