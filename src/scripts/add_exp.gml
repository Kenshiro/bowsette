/// add_exp(_exp, _hide_number = false)
var _exp = argument[0];
var _hide_number; if (argument_count > 1) _hide_number = argument[1]; else _hide_number = false;
var _current_difficulty = get_difficulty();
if  (!_hide_number)
{
    instance_create_v(x, y, obj_fluctuating_text, string(_exp));
}
if (_current_difficulty == Difficulty.GODLIKE || global.level >= 10) 
{
    global.score_points += _exp * 4;
    exit;
}

if (global.exp_count + _exp < global.exp_max)
{
    global.exp_count += _exp;
    var _score = _exp;
    if (get_difficulty() == Difficulty.HARD) _score *= 2;
    global.score_points += _score;
}
else
{
    _exp = global.exp_count - global.exp_max + _exp;
    var _score = _exp;
    if (get_difficulty() == Difficulty.HARD) _score *= 2;
    global.score_points += (argument[0] - _score);
    level_up();
    instance_create_v(obj_bowsette.x, obj_bowsette.y, obj_fluctuating_text, "LEVEL UP!");
    audio_play_sound(snd_message, 0, false);
    global.exp_count = 0;
    if (_current_difficulty == Difficulty.NORMAL)
        global.exp_max = global.level * 1.5 * 1000;
    else
        global.exp_max = global.level * 2500;
    //do level up animation
    add_exp(_exp, true);
}
