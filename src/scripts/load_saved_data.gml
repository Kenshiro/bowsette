/// load_saved_data()
//!#import bowsette_interfaces_*
/*
ini_open(SAVE_FILE);
high_score = ini_read_real("Player Info", "highscore", 0);
global.music_volume = ini_read_real("Config", "Music Volume", 100);
global.fullscreen_mode = ini_read_real("Config", "Fullscreen Mode", 0);
global.scale = ini_read_real("Config", "Scale", 1);
*/
enum Book
{
    LAND,
    CREATURES,
    PALACES
}
/*
global.page_have[Book.LAND] = string_split(ini_read_string("Books", "Land", "0,0,0,0,0,0,0"), ",");
global.page_have[Book.CREATURES] = string_split(ini_read_string("Books", "Creatures", "0,0,0,0,0,0,0"), ",");
global.page_have[Book.PALACES] = string_split(ini_read_string("Books", "Palaces", "0,0,0,0,0,0,0"), ",");
global.page_read[Book.LAND] = string_split(ini_read_string("Books", "Land", "0,0,0,0,0,0,0"), ",");
global.page_read[Book.CREATURES] = string_split(ini_read_string("Books", "Creatures", "0,0,0,0,0,0,0"), ",");
global.page_read[Book.PALACES] = string_split(ini_read_string("Books", "Palaces", "0,0,0,0,0,0,0"), ",");
var _book_size = array_length_1d(global.page_have[Book.LAND]);
for (i = 0; i < _book_size; ++i)
{
    set(global.page_read[Book.LAND], i, real(get(global.page_read[Book.LAND], i)));
    set(global.page_have[Book.LAND], i, real(get(global.page_have[Book.LAND], i)));
    set(global.page_read[Book.CREATURES], i, real(get(global.page_read[Book.CREATURES], i)));
    set(global.page_have[Book.CREATURES], i, real(get(global.page_have[Book.CREATURES], i)));
    set(global.page_read[Book.PALACES], i, real(get(global.page_read[Book.PALACES], i)));
    set(global.page_have[Book.PALACES], i, real(get(global.page_have[Book.PALACES], i)));
}
*/
/*global.keyboard_keys_p1 = new E_INPUT();
global.keyboard_keys_p1[E_INPUT.UP] = keyboard_key_string(ini_read_string("Player 1 Controls", "Up", "up"));*/
/*signal("Scale", global.scale);
signal("Fullscreen Mode", global.fullscreen_mode);
ini_close();
*/
