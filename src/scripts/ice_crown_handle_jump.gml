/// ice_crown_handle_jump(jump, jump released, jump pressed)
var _key_jump = argument0;
var _key_jump_released = argument1;
var _key_jump_pressed = argument2;
if (_key_jump_released)
    jump_lock = false;
    
if ((on_floor || (climbing && climbing_cooldown == 0)) && _key_jump && !jump_lock)
{
    if (running)
        vsp = -my_jump - 1;
    else
        vsp = -my_jump;
    image_index = 0;
    image_speed = 0.1;
    jump_lock = true;
    climbing = false;
    climbing_cooldown = 8;
    audio_play_sound(snd_jump, 0, false);
}
if (vsp < 0 && _key_jump_released)
{
    vsp /= 2;
}
if (vsp >= 0 && global.float_enabled)
{
    if (_key_jump && !on_floor)
    {
        if (airtime < max_airtime)
        {
            floating = true;
        }
        else
        {
            floating = false;
        }
    }
    else if (_key_jump_released || floating)
    {
        airtime = max_airtime;
        floating = false;
    }
    if (floating)
    {
        vsp = 0;
        airtime++;
        if (airtime >= max_airtime)
            floating = false;
    }
}

