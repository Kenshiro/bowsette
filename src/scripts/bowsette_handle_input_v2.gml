/// bowsette_handle_input->Void
if (live_call()) return live_result;
var _k_running = input_check(E_INPUT.RUN) || (get_difficulty() == Difficulty.GODLIKE);
var _k_jump = input_check(E_INPUT.JUMP);
var _k_jump_pressed = input_check_pressed(E_INPUT.JUMP);
var _k_jump_released = input_check_released(E_INPUT.JUMP);
var _k_left = input_check(E_INPUT.LEFT);
var _k_right = input_check(E_INPUT.RIGHT) || (get_difficulty() == Difficulty.GODLIKE);
var _k_left_doublepressed = input_check_double(E_INPUT.LEFT);
var _k_right_doublepressed = input_check_double(E_INPUT.RIGHT);
var _k_crunch = input_check(E_INPUT.DOWN);
var _k_up = input_check(E_INPUT.UP);
var _k_fire = input_check(E_INPUT.SKILL);
var h; //used for slopes
var _speed, _on_ice;

if (climbing_cooldown > 0) climbing_cooldown--;

if (_k_running)
{
    my_speed = speed_running;
    running = true;
}
else
{
    my_speed = speed_walking;
    running = false;
}
if (!sliding && _k_crunch && on_floor)
{
    crunch = true;
    mask_index = spr_bowsette_crunched_mask;
    var _solid = collision_line(bbox_left + 1, bbox_bottom + 1, bbox_right - 1, bbox_bottom + 1, obj_solid, true, false);
    var _plat = collision_line(bbox_left + 1, bbox_bottom + 1, bbox_right - 1, bbox_bottom + 1, obj_plat, true, false);
    if (_solid == noone && _plat != noone && _k_jump_pressed)
    {
        y += 3;
        if (_plat.object_index == obj_moving_platform && !_plat.horizontal)
            y += 3;
        vsp = 2;
        crunch = false;
        on_floor = false;
        airtime = max_airtime;
        floating = false;
    }
    if ((looking_right && _k_right_doublepressed) || (!looking_right && _k_left_doublepressed))
    {
        sliding = true;
        sliding_current_time = 0;
        sliding_current_speed = sliding_start_speed + 1;
        //y = 200
    }
}
else
{
    if (!sliding)
        mask_index = spr_bowsette_mask;
    else
        mask_index = spr_bowsette_sliding_mask;
    crunch = false;
    //Handle jumping according to current power up
    script_execute(global.current_power_up[PowerUp.JUMP], _k_jump, _k_jump_released,
    _k_jump_pressed);
    var _ladder_list = ds_list_create();
    collision_rectangle_list(bbox_left, bbox_top, bbox_right, bbox_bottom, obj_ladder, false, false, _ladder_list, false);
    if (!climbing && climbing_cooldown == 0 && (_k_up || _k_crunch) && ds_list_size(_ladder_list) > 0)
    {
        if (!sliding)
        {
            for (var i = 0; i < ds_list_size(_ladder_list); ++i)
            {
                var _ladder_obj = _ladder_list[| i];
                if (!(bbox_top > _ladder_obj.bbox_top && abs(bbox_left - _ladder_obj.bbox_left) < 20 && abs(bbox_right - _ladder_obj.bbox_right) < 20)) continue;
                if (_k_up && place_free(x, y - 3))
                {
                    climbing = true;
                    on_floor = false;
                    airtime = 0;
                    y -= 3;
                    vsp = 0;
                    if (!_ladder_obj.is_grid)
                    {
                        x = _ladder_obj.x + 16;
                    }
                    break;
                }
                else if (_k_crunch && place_free(x, y + 3))
                {
                    climbing = true;
                    on_floor = false;
                    y += 3;
                    vsp = 0;
                    airtime = 0;
                    if (!_ladder_obj.is_grid)
                    {
                        x = _ladder_obj.x + 16;
                    }
                    break;
                }
            }
        }
    }
    else
    {
        _on_ice = (collision_line(bbox_left, bbox_bottom + 1, bbox_right, bbox_bottom + 1, obj_ice, true, false) != noone || collision_line(bbox_left, bbox_bottom + 1, bbox_right, bbox_bottom + 1, obj_ice_plat, true, false) != noone);
        if (_on_ice && global.current_power_up != global.power_up[PowerUpType.ICE])
        {
            has_fric = true;
            if (!sliding)
            {
                if (_k_left)
                {
                    spd = approach(spd, -my_speed, 0.05);
                    looking_right = false;
                }
                else if (_k_right)
                {
                    spd = approach(spd, my_speed, 0.05);
                    looking_right = true;
                }
                else
                {
                    spd = approach(spd, 0, 0.05);
                }
            }
            else
            {
                if (looking_right)
                    spd = sliding_current_speed;
                else
                    spd = -sliding_current_speed;
            }
        }
        else
        {
            has_fric = false;
            fric = 0;
            if (!sliding)
            {
                if (_k_left)
                {
                    spd = -my_speed;
                    looking_right = false;
                }
                else if (_k_right)
                {
                    spd = my_speed;
                    looking_right = true;
                }
                else
                {
                    spd = 0;
                }
            }
            else
            {
                if (looking_right)
                    spd = sliding_current_speed;
                else
                    spd = -sliding_current_speed;
            }
        }
        var _hs = spd;
        _hs += rhs;
        if (_hs >= 0)
            rounded_speed = round(_hs);
        else
            rounded_speed = -round(abs(_hs)); //rounding works differently with negative numbers
        rhs = _hs - rounded_speed;
        var _destiny_x = x + rounded_speed;
        var _old_x = x, _old_y = y;
        if ((rounded_speed != 0 && _destiny_x < room_width && x + _destiny_x > 0) || climbing)
        {
            if (!climbing)
            {
                if (place_free(_destiny_x, y))
                {
                    if (place_free(_destiny_x, y + 6))
                    {
                        x = _destiny_x;
                        walking = true;
                    }
                    else
                    for (h = 4; h >= 0; --h)
                    {
                        if (place_free(_destiny_x, y + h))
                        {
                            x = _destiny_x;
                            if (!floating)
                            {
                                y = y + h;
                                while (collision_line(bbox_left, bbox_bottom + 1, bbox_right, bbox_bottom + 1, obj_solid, 1, 0) == noone)
                                    y += 1;
                            }
                            walking = true;
                            break;
                        }
                    }
                }
                else
                {
                    for (h = 1; h <= 8; ++h)
                    {
                        if (place_free(_destiny_x, y - h))
                        {
                            x = _destiny_x;
                            y = y - h;
                                while (collision_line(bbox_left, bbox_bottom + 1, bbox_right, bbox_bottom + 1, obj_solid, 1, 0) == noone)
                                    y += 1;
                            walking = true;
                            break;
                        }
                    }
                    if (has_fric && _old_x == x && _old_y == y && h > 8)
                        spd = 0; //stop if slide into a wall
                }
            }
            else
            {
                /*var _hs = spd;
                _hs += rhs;
                    rounded_speed = (_hs | 0);
                rhs = _hs - rounded_speed;
                var _destiny_x = x + rounded_speed;
                var _old_x = x, _old_y = y;*/
                var _ladder_obj = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, obj_ladder, false, false);
                var _destiny_x = x, _destiny_y = y, _on_ladder = true;
                if (_ladder_obj.is_grid)
                {
                    if (_k_left)
                        _destiny_x = x - 3;
                    if (_k_right)
                        _destiny_x = x + 3;
                }
                if (_k_up)
                    _destiny_y = y - 3;
                if (_k_crunch)
                    _destiny_y = y + 3;
                    
                if (_destiny_x != x || _destiny_y != y)
                {
                    image_speed = 0.1;
                    if (place_free(_destiny_x, _destiny_y))
                    {
                        x = _destiny_x;
                        y = _destiny_y;
                        _on_ladder = collision_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, obj_ladder, false, false) != noone;
                        if (!_on_ladder)
                        {
                            climbing = false;
                            climbing_cooldown = 8;
                        }
                    }
                    else if (_k_crunch)
                    {
                        for (var i = y; i < _destiny_y; ++i)
                        {
                            if (place_free(_destiny_x, i))
                            {
                                y = i;
                                climbing = false;
                                on_floor = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    image_speed = 0;
                }
            }
        }
        else
        {
            if (!_on_ice || (!_k_left && !_k_right && spd == 0))
            walking = false;
        }
    }
    if (sliding)
    {
        if (on_floor)
        {
            if (_on_ice)
                sliding_current_speed = approach(sliding_current_speed, 0, 0.07);
            else
                sliding_current_speed = approach(sliding_current_speed, 0, 0.14);
                
            if (sliding_current_speed == 0)
            {
                sliding = false;
                if (_k_crunch)
                    crunch = true;
            }
        }
    }
    
    ds_list_destroy(_ladder_list);
}
//Handle fire breath according to current power up
if (!sliding && !climbing)
    script_execute(global.current_power_up[PowerUp.SKILL], _k_fire);
