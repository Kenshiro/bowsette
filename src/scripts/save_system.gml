#define save_system
//* Super Bowsette's save system */
#define save_system_load_data
/// save_system_load_data()->Void
ini_open(SAVE_FILE);
if (ini_read_string("Misc", "Version", "") == "")
{
    ini_close();
    file_delete(SAVE_FILE);
    ini_open(SAVE_FILE);
}
ini_write_string("Misc", "Version", VERSION);
// Library
global.unlocked_courses = save_system_string_to_bool_array(ini_read_string("Unlocked Courses", "status", "000000"));
global.page_unlocked[Book.LAND] = save_system_string_to_bool_array(ini_read_string("Library", "LandUnlocked", "0000000"));
global.page_read[Book.LAND] = save_system_string_to_bool_array(ini_read_string("Library", "LandRead", "0000000"));
global.page_unlocked[Book.CREATURES] = save_system_string_to_bool_array(ini_read_string("Library", "CreaturesUnlocked", "0000000000"));
global.page_read[Book.CREATURES] = save_system_string_to_bool_array(ini_read_string("Library", "CreaturesRead", "0000000000"));
global.page_unlocked[Book.PALACES] = save_system_string_to_bool_array(ini_read_string("Library", "PalacesUnlocked", "000000"));
global.page_read[Book.PALACES] = save_system_string_to_bool_array(ini_read_string("Library", "PalacesRead", "000000"));
// Score
global.top_score = ini_read_real("Player Info", "top_highscore", 0);
// Options
global.music_volume = ini_read_real("Config", "Music Volume", 100);
global.fullscreen_mode = ini_read_real("Config", "Fullscreen Mode", 0);
global.scale = ini_read_real("Config", "Scale", 1);
global.got_first_page = ini_read_real("Misc", "FP", 0);
// Change scale and screen mode according to settings
bowsette_interfaces_signal("Scale", global.scale);
ini_close();
#define save_system_unlock_course
/// save_system_unlock_course(course name:String)->Void
ini_open(SAVE_FILE);
var _course_name = argument0;
var _course_position = game_get_course_position(game_get_course(_course_name));
global.unlocked_courses[_course_position] = true;
var _unlocked_courses = save_system_bool_array_to_string(global.unlocked_courses);
ini_write_string("Unlocked Courses", "status", _unlocked_courses);
ini_close();
#define save_system_unlock_page
/// save_system_unlock_page(book: Book.Enum, Page Number:Int)->Void
ini_open(SAVE_FILE);
var _book = argument0;
var _pos = argument1;
//global.page_unlocked[_book] = true;
set(global.page_unlocked, _book, _pos, true);
var _ini_key = "";
switch(_book)
{
    case Book.LAND:
        _ini_key = "LandUnlocked";
        break;
    case Book.CREATURES:
        _ini_key = "CreaturesUnlocked";
        break;
    case Book.PALACES:
        _ini_key = "PalacesUnlocked";
        break;
}
ini_write_string("Library", _ini_key, save_system_bool_array_to_string(global.page_unlocked[_book]));
ini_close();
#define save_system_read_page
/// save_system_unlock_page(book: Book.Enum, Page Number:Int)->Void
ini_open(SAVE_FILE);
var _book = argument0, _pos = argument1;
var _ini_key = "";
set(global.page_read, _book, _pos, true);
switch(_book)
{
    case Book.LAND:
        _ini_key = "LandRead";
        break;
    case Book.CREATURES:
        _ini_key = "CreaturesRead";
        break;
    case Book.PALACES:
        _ini_key = "PalacesRead";
        break;
}
ini_write_string("Library", _ini_key, save_system_bool_array_to_string(global.page_read[_book]));
ini_close();
#define save_system_bool_array_to_string
/// save_system_bool_array_to_string(values:Array<Bool>)->String
var _arr = argument0;
var _str = "";
for (var i = 0; i < array_length_1d(_arr); ++i)
{
    if (_arr[i]) _str += "1";
    else _str += "0";
}
return _str;
#define save_system_string_to_bool_array
/// save_system_string_to_bool_array(values:String)->Array<Bool>
var _str = argument0;
var _len = string_length(_str);
var _arr = array_create(_len);
for (var i = 0; i < _len; ++i)
{
    if (string_byte_at(_str, i + 1) == 49)
        _arr[i] = 1;
    else
        _arr[i] = 0;
}
return _arr;
#define save_system_save_config
/// save_system_save_config()->Void
ini_open(SAVE_FILE);
ini_write_real("Config", "Fullscreen Mode", global.fullscreen_mode);
ini_write_real("Config", "Music Volume", global.music_volume);
ini_write_real("Config", "Scale", global.scale);
ini_close();
#define save_system_update_score
/// save_system_update_score()->Void
ini_open(SAVE_FILE);
ini_write_real("Player Info", "top_highscore", global.top_score);
ini_close();
#define save_system_save_first_page
/// save_system_save_first_page()->Void
ini_open(SAVE_FILE);
ini_write_real("Misc", "FP", 1);
ini_close();