/// create_all_fonts()->Void
enum Font { SMALL, MEDIUM, BIG, BIGGEST };

global.vinizinho_24 = font_add("vinizinho.ttf", 24, true, false, 32, 128);
global.vinizinho_48 = font_add("vinizinho.ttf", 48, true, false, 32, 128);
global.vinizinho_18 = font_add("vinizinho.ttf", 18, true, false, 32, 128);
global.vinizinho_18i = font_add("vinizinho.ttf", 18, true, true, 32, 128);
global.vinizinho_24i = font_add("vinizinho.ttf", 24, true, true, 32, 128);
global.vinizinho_26 = font_add("vinizinho.ttf", 26, true, false, 32, 128);
global.vinizinho_12 = font_add("vinizinho.ttf", 12, true, false, 32, 128);
global.vinizinho_12i = font_add("vinizinho.ttf", 12, true, true, 32, 128);
global.grapesoda[Font.SMALL] = font_add("GrapeSoda.ttf", 12, false, false, 32, 128);
global.grapesoda[Font.MEDIUM] = font_add("GrapeSoda.ttf", 16, false, false, 32, 128);
global.grapesoda[Font.BIG] = font_add("GrapeSoda.ttf", 22, false, false, 32, 128);
global.grapesoda[Font.BIGGEST] = font_add("GrapeSoda.ttf", 28, false, false, 32, 128);
global.grapesoda_test = font_add("GrapeSoda.ttf", 48, false, false, 32, 128);
global.ui_font[Font.SMALL] = font_add_sprite_ext(spr_font_small, 
'!"#$%&' + "'" + "()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`{|}~",
false, 0);
global.ui_font[Font.BIG] = font_add_sprite_ext(spr_font_big, 
'!"#$%&' + "'" + "()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`{|}~",
false, 0);

global.ui_font[Font.MEDIUM] = font_add_sprite_ext(spr_font, 
'!"#$%&' + "'" + "()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`{|}~",
false, 0);
