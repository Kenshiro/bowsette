/// draw_pause_option
//!#import ui_option_* in option
//!#import ui_haxe_boot_*

var _option/*:option*/ = argument0;
var _x/*:int*/ = argument1;
var _y/*:int*/ = argument2;
var _color/*:color*/ = argument3;

var _class = ui_haxe_boot_get_class(_option);
var _offset = 16;

switch(ui_haxe_boot_get_class(_option))
{
    case mt_ui_action:
    case mt_ui_action_with_confirmation:
        draw_set_halign(fa_center);
        draw_text_outlined(_x, _y, _option[ui_option_name], _color, c_black, 2);
        break;
    case mt_ui_toggle:
        var _value;
        draw_set_halign(fa_right);
        draw_text_outlined(_x, _y, _option[ui_option_name], _color, c_black, 2);
        draw_set_halign(fa_left);
        if (_option[ui_option_value] == true)
            _value = "ON";
        else
            _value = "OFF";
        draw_text_outlined(_x + _offset, _y, _value, _color, c_black, 2);
    break;
    case mt_ui_slider:
        draw_set_halign(fa_right);
        draw_text_outlined(_x - _offset, _y, _option[ui_option_name], _color, c_black, 2);
        draw_set_halign(fa_left);
        draw_text_outlined(_x + _offset, _y, string(_option[ui_option_value]) + "%", _color, c_black, 2);
    break;
    case mt_ui_multiple_values:
        draw_set_halign(fa_right);
        draw_text_outlined(_x - _offset, _y, _option[ui_option_name], _color, c_black, 2);
        draw_set_halign(fa_left);
        draw_text_outlined(_x + _offset, _y, get(_option, ui_multiple_values_values_list, _option[ui_option_value]), _color, c_black, 2);
    break;
}
