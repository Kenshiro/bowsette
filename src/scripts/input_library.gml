#define input_library
//  INPUT v1.0.0
//  2018/05/28
//  @jujuadams
//  Ported to GMS1 by @pudarida

#define __input_handle_general
/// @param index
/// @param pressed_index
/// @param analogue_value

input_state[ argument0, E_STATE.NEW_ON ]            = true;
input_state[ argument0, E_STATE.NEW_PRESSED_INDEX ] = argument1;
input_state[ argument0, E_STATE.ANALOGUE ]          = argument2;

#define input_clear
for( var _y = E_STATE.__SIZE-1; _y >= 0; --_y ) {
	for( var _x = E_INPUT.__SIZE-1; _x >= 0; --_x ) {
		input_state[ _x, _y ] = false;
	}
}


#define input_handle_gamepad_axis
/// input_handle_gamepad_axis(input_index, device, axis, threshold)

if ( gamepad_is_connected( argument1 ) ) {
    if ( gamepad_axis_value( argument1, argument2 )*sign( argument3 ) > abs( argument3 ) ) {
        __input_handle_general( argument0, argument2 + 0.1 + argument1/100, gamepad_axis_value( argument1, argument2 )*sign( argument3 ) );
    }
}

#define input_handle_gamepad_button
/// input_handle_gamepad_button(input_index, device, button)

if ( gamepad_is_connected( argument1 ) ) {
    if ( gamepad_button_check( argument1, argument2 ) ) {
        __input_handle_general( argument0, argument2 + 0.2 + argument1/100, 1 );
    }
}

#define input_handle_keyboard
/// input_handle_keyboard(input_index, keyboard_button, [keyboard_button])

for( var _i = 1; _i < argument_count; _i++ ) {
    if ( keyboard_check( argument[_i] ) ) __input_handle_general( argument[0], argument[_i], 1 );
}

#define input_handle_mouse
/// input_handle_mouse(input_index, button, [device])

var _device = 0;
if ((argument_count >= 3) && (argument[2] != undefined)) {
 _device = argument[2];
}

if ( device_mouse_check_button( _device, argument1 ) ) __input_handle_general( argument0, argument1 + 0.3 + _device/100, 1 );

#define input_refresh_begin
//This script clears the momentary state for each input slot
//The "input_handle_*" functions modify these values
for( var _i = 0; _i < E_INPUT.__SIZE; _i++ ) {
    input_state[ _i, E_STATE.NEW_ON ]            = false;
    input_state[ _i, E_STATE.NEW_PRESSED_INDEX ] = undefined;
    input_state[ _i, E_STATE.ANALOGUE ]          = 0;
}

#define input_refresh_end
for( var _i = 0; _i < E_INPUT.__SIZE; _i++ ) {
    
    //Clear out our states
	input_state[ _i, E_STATE.PRESSED  ] = false;
	input_state[ _i, E_STATE.RELEASED ] = false;
	input_state[ _i, E_STATE.REPEATED ] = false;
	input_state[ _i, E_STATE.LONG     ] = false;
	input_state[ _i, E_STATE.DOUBLE   ] = false;
    
    if ( input_state[ _i, E_STATE.NEW_ON ] ) {
        //If this input is ON this frame...
        
    	if ( input_state[ _i, E_STATE.ON ] ) {
            //...and the input was ON last frame too...
            
    		if ( current_time - input_state[ _i, E_STATE.REPEAT_TIME ] >= repeat_delay ) {
                //...then trigger the REPEATED state if we've been holding the button for long enough
    			input_state[ _i, E_STATE.REPEATED ] = true;
    			input_state[ _i, E_STATE.REPEAT_TIME ] = current_time;
    		}
            
            //Update the LONG state based on the time since we set the input to PRESSED and now
    		input_state[ _i, E_STATE.LONG ] = ( current_time - input_state[ _i, E_STATE.PRESSED_TIME ] >= long_delay );
            
    	} else {
            
            //...and the input was OFF last frame then we've switched on the input slot
    		input_state[ _i, E_STATE.PRESSED ] = true;
    		input_state[ _i, E_STATE.REPEATED ] = true;
		    
    		if ( input_state[ _i, E_STATE.PRESSED_INDEX ] == input_state[ _i, E_STATE.NEW_PRESSED_INDEX ] ) {
                //If we've pressed the same button as we did last time the button was pressed, check to see if this counts as a double tap
    			input_state[ _i, E_STATE.DOUBLE ] = ( current_time - input_state[ _i, E_STATE.PRESSED_TIME ] <= double_delay );
    		} else {
                //If we've pressed a different button than we did last time the button was pressed, update our input state
    			input_state[ _i, E_STATE.PRESSED_INDEX ] = input_state[ _i, E_STATE.NEW_PRESSED_INDEX ];
    		}
            
            //Set some timers to record when we first switched on this input slot
    		input_state[ _i, E_STATE.PRESSED_TIME ] = current_time;
    		input_state[ _i, E_STATE.REPEAT_TIME  ] = current_time;
    	}
        
    } else {
        //If this input is OFF this frame...
        
    	if ( input_state[ _i, E_STATE.ON ] ) {
            //...and we were ON the last frame, the input has been RELEASED
            input_state[ _i, E_STATE.RELEASED ] = true;
        }
        
    }
    
    //Update the ON state for input checking, and for comparison next frame
    input_state[ _i, E_STATE.ON ] = input_state[ _i, E_STATE.NEW_ON ];
    
}

#define input_check
/// input_check(input_index)
//  Checks if an input slot is currently ON, i.e. an input button is held down

return obj_input_controller.input_state[ argument0, E_STATE.ON ];

#define input_check_pressed
/// input_check_pressed(input_index)
//  Checks if an input slot has been newly activated this frame

return obj_input_controller.input_state[ argument0, E_STATE.PRESSED ];

#define input_check_released
/// input_check_released(input_index)
//  Checks if an input slot has been newly deactivated this frame

return obj_input_controller.input_state[ argument0, E_STATE.RELEASED ];

#define input_check_repeated
/// input_check_repeated(input_index)
//  Checks if an input slot is firing a REPEAT state for long presses

return obj_input_controller.input_state[ argument0, E_STATE.REPEATED ];

#define input_check_long
/// input_check_long(input_index)
//  Checks if an input slot has been held down for a long period of time

return obj_input_controller.input_state[ argument0, E_STATE.LONG ];

#define input_check_double
/// input_check_double(input_index)
//  Checks if an input slot has been double-tapped

return obj_input_controller.input_state[ argument0, E_STATE.DOUBLE ];

#define input_check_analogue
/// input_check_analogue(input_index)
//  Checks the analogue value of an input

return obj_input_controller.input_state[ argument0, E_STATE.ANALOGUE ];