///custom_gravity(acceleration: Float, max speed: Int)->Void
if (live_call(argument0,argument1)) return live_result;
var _accel/*:float*/ = argument0;
var _max_spd/*:int*/ = argument1;
var _height/*:int*/;
var _feet/*:int*/ = bbox_bottom, _middle/*:float*/ = bbox_left + (bbox_right - bbox_left)/2;
var _found_ground/*:bool*/ = false;
var i, _plat, _solid;

if (vsp >= 0)
{
    _solid = collision_line(bbox_left + 2, _feet + 1, bbox_right - 2, 
    _feet + 1, obj_solid, 1, 0);
    
    if (_solid == noone || (_solid.sprite_index == spr_block &&
    !_solid.visible))
    {
        _plat = collision_line(bbox_left, _feet + 1, bbox_right, 
        _feet + 1, obj_plat, 0, 0);
        if (_plat == noone || _plat.bbox_top < _feet)
        {
            on_floor = false;
            if (vsp < _max_spd)
            {
                vsp += _accel;
                
                if (vsp > _max_spd)
                    vsp = _max_spd;
            }
            for (i = (vsp | 0); i >= 1; i--)
            {
                _solid = collision_line(bbox_left + 2, _feet + i, bbox_right - 2, 
                _feet + i, obj_solid, 1, 0);
                if (_solid == noone || (_solid.sprite_index == spr_block &&
                !_solid.visible))
                {
                    _plat = collision_line(bbox_left + 2, _feet + i, 
                    bbox_right - 2, _feet + i, obj_plat, 0, 0);
                    if (_plat == noone  || _plat.bbox_top < _feet)
                    {
                        y += i;
                       /* if (i > 1)
                            on_floor = false;
                        else
                        {
                            on_floor = true;
                            vsp = 0;
                        }*/
                        break;
                    }
                    else
                    {
                        /*vsp = 0;
                        y += (i - 1);
                        break;*/
                        on_floor = true;
                        continue;
                    }
                }
                else
                {
                   /* vsp = 0;
                    y += (i - 1);
                    on_floor = true;
                    break;*/
                    continue;
                }
            }
        }
        else
        {
            vsp = 0;
            on_floor = true;
        }
    }
    else
    {
        vsp = 0;
        on_floor = true;
    }
}
if (vsp < 0)
{
    on_floor = false;
    var _vsp = (abs(vsp | 0));
    for (i = _vsp; i >= 0; i--)
    {
        _solid = collision_line(bbox_left + 2, bbox_top - i, bbox_right - 2, 
        bbox_top - i, obj_solid, 1, 0);
        if (_solid == noone || (_solid.sprite_index == spr_block &&
        !_solid.visible))
        {
            y -= i;
            if (i != _vsp)
            {
                vsp = 0;
            }
            else
            {
                vsp += _accel;
            }
            break;
        }
        else
        {
            y -= i;
            while (collision_line(bbox_left, bbox_top, bbox_right, 
                bbox_top, obj_solid, 1, 0) != noone)
                    y += 1;
            vsp = 0;
            break;
        }
    }
}

