/// bowsette_jumped_on_top(bowsette object)
//assumes a Bowsette object exists.
var _bowsette, _extra, _my_extra;
_bowsette = argument0;
if (_bowsette.vsp > 0)
    _extra = _bowsette.vsp;
else
    _extra = 0;
if (vsp < 0)
    _my_extra = -vsp;
else
    _my_extra = 0;
if (_bowsette.y - _extra <= bbox_top + 5 + _my_extra && (_bowsette.just_colided ||_bowsette.vsp >= 0))
    return true;
return false;
