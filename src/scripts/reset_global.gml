///reset_global()->Void
//collectables, hud, general
//!#import collectibles_inventory.* as Inventory
global.diamonds = 0;
global.diamond_collected = array_create(4);
global.diamonds_before_death = 0;
global.max_hp = 2;
global.live_count = 5;
global.checkpoint = false;
global.__cutscene = false;
//control
global.pause = false;
global.lock_controls = false;
global.pause_lock = false;
global.double_jump = false;
global.fire_power = 1;
global.fire_enabled = true;
global.float_enabled = true;
global.hud_alpha = 1;
global.song = -1;
global.show_hud = false;
global.game_over = false;
global.night = false;
global.item_inventory = inventory_create(8);
global.onoff_pink_enabled = false;
global.onoff_orange_enabled = false;
global.onoff_cyan_enabled = false;
