/// string_split(this:string, del:string)->array<string>
/// @Author YellowAfterlife
var this = argument[0];
var del = argument[1];
var str = this;
var num = 0;
var arr = array_create((string_count(del, str) + 1));
for (var pos = string_pos(del, str); pos > 0; pos = string_pos(del, str)) {
	arr[@num] = string_copy(str, 1, pos - 1);
	++num;
	str = string_delete(str, 1, pos);
}
arr[@num] = str;
return arr;
