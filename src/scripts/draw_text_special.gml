/// draw_text_special(x, y, string, line width, default color, ?cutoff)
var _x = argument[0];
var _y = argument[1];
var _text = argument[2];
var _line_width = argument[3];
if (_line_width == -1)
    _line_width = 99999;
var _default_color = argument[4];
if (argument_count > 5 && argument[5] != -1)
    var _cutoff = argument[5];
else
    var _cutoff = 99999;
var cnt,current_width,delay,space,line,line_height,length,modifier;
cnt = 0;
current_width = 0;
delay = 2;
space = 0;
line = 0;
_line_height = string_height("ABDCEFGHIJKLMNOPQRSTUVWXYZ");
length = 0;
modifier = "";
modifier_adjust = 0;
draw_set_color(_default_color);
while (cnt <= string_length(_text) && cnt <= _cutoff)
{
    cnt++;
    var word_length_check = "";
    var word_length_check_counter = 0;
    
    //Check for modifier
    if (string_char_at(_text, cnt) == "\")
    {
        modifier = string_char_at(_text, ++cnt);
        ++cnt;
    }

    while (string_char_at(_text,cnt) != " " && 
    cnt <= string_length(_text))
    {
        if (string_char_at(_text, cnt) != "\")
            word_length_check += string_char_at(_text, cnt);
        else
        {
            cnt++;
            word_length_check_counter++;
        }
        cnt++;
        word_length_check_counter++;
    }
    if (current_width + string_width(word_length_check)-modifier_adjust >= _line_width)
    {
        current_width = 0;
        line++;
    }
    cnt -= word_length_check_counter;
    current_char = string_char_at(_text, cnt);
    
    //Draw current char according to current modifier.
    switch(modifier)
    {
        case "R": //Red Color
        {
            draw_set_color(c_red);
            break;
        }
        
        case "G": //Green Color
        {
            draw_set_color(c_green);
            break;
        }
        
        case "B": //Blue Color
        {
            draw_set_color(c_blue);
            break;
        }
        
        case "N": //Normal Color
        {
            draw_set_color(_default_color);
            break;
        }
        
        case "P": //Purple Color
        {
            draw_set_color(c_purple);
            break;
        }
        
        case "O": //Orange
        {
            draw_set_color(c_orange);
            break;
        }
        
        case "A": //Aqua
        {
            draw_set_color(c_aqua);
            break;
        }
        
    }
    draw_text(_x + current_width, _y + line * _line_height, current_char);
    current_width += string_width(current_char);
}
