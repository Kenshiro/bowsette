/// enemy_generic_movement(_avoid_pit)
//if (live_call(argument0)) return live_result;
var _avoid_pit = argument0;
var _col_pos, _enemy;
/*if (hspeed > 0)
{
    _col_pos = bbox_right;
    _enemy = collision_line(_col_pos, bbox_top + 1, _col_pos, bbox_bottom - 1, 
    obj_basic_enemy, 0, true); 
    if (_col_pos >= room_width || (_avoid_pit && place_free(_col_pos + hspeed*2, y + 8) && 
    //collision_line(bbox_right, y + 1, bbox_right + hspeed*2 + 1, y + 1, obj_plat, 0, 0) == noone) ||
    collision_point(_col_pos + 8, bbox_bottom + 8, obj_plat, 0, 0) == noone && on_floor) ||
    collision_line(_col_pos, bbox_top + 1, _col_pos, bbox_bottom - 1, obj_solid, 0, 0) != noone ||
    (_enemy!= noone && !_enemy.died && _enemy.solid_enemy))
    {
        hspeed *= -1;
        image_xscale *= -1;
    }
}
else
{
    _col_pos = bbox_left;
    _enemy = collision_line(_col_pos, bbox_top + 1, _col_pos, bbox_bottom - 1,
    obj_basic_enemy, 0, true);
    if (_col_pos <= 0 || (_avoid_pit && place_free(bbox_left + hspeed, y + 1) && 
    collision_point(_col_pos - 8, bbox_bottom + 8, obj_plat, 0, 0) == noone &&
    on_floor) || collision_line(_col_pos, bbox_top + 1, _col_pos, bbox_bottom - 1, obj_solid, 0, 0) != noone ||
    (_enemy!= noone && !_enemy.died && _enemy.solid_enemy))
    {
        hspeed *= -1;
        image_xscale *= -1;
    }
}*/

var _old_x, _old_y, h, _met_a_wall, _hit_room_border;
_old_x = x;
_old_y = y;
if (hs > 0)
{
    _col_pos = bbox_right;
    _hit_room_border = (_col_pos >= room_width);
}
else
{
    _col_pos = bbox_left;
    _hit_room_border = (_col_pos <= 0);
}

var _hs = hs;
_hs += rhs;
_hs2 = floor(_hs);
rhs = _hs - _hs2;
rhs -= floor(rhs);
var _destiny_x = x + _hs2;
_enemy = collision_line(_col_pos + hs, bbox_top + 1, _col_pos + hs, bbox_bottom - 1, 
obj_basic_enemy, 0, true);
if ((_avoid_pit && on_floor && place_free(_col_pos, y + 16) && !place_meeting(_col_pos, y + 16, obj_plat)) || _enemy != noone || _hit_room_border)
{
    hs *= -1;
    if (turn_cooldown <= 0)
    {
        image_xscale *= -1;
        turn_cooldown = 15;
    }
    exit;
}

if (place_free(_destiny_x, y))
{
    
    var _plat = collision_point(_destiny_x, y + 6, obj_plat, false, false);
    if (place_free(_destiny_x, y + 6) && (_plat == noone || _plat.y < y))
    {
        if (!_avoid_pit)
            x = _destiny_x;
        else if (on_floor)
        {
            hs *= -1;
            image_xscale *= -1;
            exit;
        }
    }
    else
    for (h = 0; h < 6; ++h)
    {
        var _plat = collision_point(_destiny_x, y + h, obj_plat, false, false);
        if (place_free(_destiny_x, y + h) && (_plat == noone || _plat.y <= y))
        {
            x = _destiny_x;
            y = y + h;
            //while (collision_line(bbox_left, bbox_bottom + 1, bbox_right, bbox_bottom + 1, obj_solid, 1, 0) == noone)
               // y += 1;
            exit;
        }
        /*
        else if (h == 0 && _avoid_pit && on_floor)
        {
            hs *= -1;
            image_xscale *= -1;
            exit;
        }
        */
    }
}
else
{
    for (h = 0; h <= 8; ++h)
    {
        var _plat = collision_point(_destiny_x, y + h, obj_plat, false, false);
        if (place_free(_destiny_x, y - h) /*&& (_plat == noone || _plat.y < y)*/)
        {
            x = _destiny_x;
            y = y - h;
                //while (collision_line(bbox_left, bbox_bottom + 1, bbox_right, bbox_bottom + 1, obj_solid, 1, 0) == noone)
                    //y += 1;
            exit;
        }
        else if (h == 8 && _avoid_pit && on_floor)
        {
            hs *= -1;
            image_xscale *= -1;
            exit;
        }
    }
}
_met_a_wall = (collision_line(_col_pos + hs, bbox_top + 1, _col_pos + hs, bbox_bottom - 1, obj_solid, true, false) != noone);

if (_hit_room_border || (place_free(_col_pos + hs , y + 12) && 
collision_point(_col_pos + hs * 4, bbox_bottom + 8, obj_plat, 0, 0) == noone && on_floor && _avoid_pit) ||
(collision_line(_col_pos, bbox_top + 1, _col_pos, bbox_bottom - 1, obj_solid, 0, 0) != noone && on_floor) ||
(_enemy != noone && !_enemy.died && _enemy.solid_enemy) || _met_a_wall)
{
    hs *= -1;
    image_xscale *= -1;
}
