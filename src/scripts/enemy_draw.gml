/// enemy_draw()
if (burned)
{
    draw_set_alpha(1);
    draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, c_white, image_alpha);
    d3d_set_fog(1, c_gray, 0, 0);
    draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, c_white, died_alpha);
    d3d_set_fog(0, 0, 0, 0);
    draw_set_alpha(1);
}
else if (damaged)
{
    draw_self();
    draw_set_alpha(0.75);
    d3d_set_fog(1, c_orange, 0, 0);
    draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, c_white, 0.75);
    d3d_set_fog(0, 0, 0, 0);
    draw_set_alpha(1);
}
else draw_self();
