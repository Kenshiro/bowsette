///draw_exp_bar(pos x, pos y, width, height)
var argument_arr = array_create(argument_count);
for (var i = 0; i < argument_count; i++) {
	argument_arr[i] = argument[i];
}
if (live_call_ext(argument_arr)) return live_result;
var _pos_x = argument0;
var _pos_y = argument1;
var _width = argument2;
var _height = argument3;

var _exp_proportion;
var _exp_bar_text;

if (global.level >= 10)
{
    _exp_proportion = 1;
    _exp_bar_text = "LEVEL MAX"
}
else
{
    _exp_proportion = (global.exp_count / global.exp_max);
    _exp_bar_text = string(global.exp_count) + "/" + string(global.exp_max);
}

//draw_set_color(exp_color);
draw_rectangle(view_xview[0] + _pos_x, view_yview[0] + _pos_y,
view_xview[0] + _pos_x + _exp_proportion * _width,
view_yview[0] + _pos_y + _height, false);

draw_set_color(c_white)
draw_set_font(global.ui_font[Font.SMALL]);
draw_set_valign(fa_middle);
draw_set_halign(fa_center);
/*draw_text(view_xview[0] + _pos_x + (_width/2), view_yview[0] + _pos_y + (_height/2),
"BOWSETTE LV." + string(global.level));*/

/*draw_text(view_xview[0] + _pos_x + (_width/2), view_yview[0] + _pos_y + (_height/2) + 1,
_exp_bar_text);*/

/*draw_set_color(c_black);
draw_rectangle(view_xview[0] + _pos_x, view_yview[0] + _pos_y,
view_xview[0] + _pos_x + _width, view_yview[0] + _pos_y + _height, true);
draw_rectangle(view_xview[0] + _pos_x - 1, view_yview[0] + _pos_y - 1,
view_xview[0] + _pos_x + _width + 1, view_yview[0] + _pos_y + _height + 1, true);
*/
