/// bowsette_handle_combo()
if (global.combo_count < global.combo_max_count)
{
    global.combo_count++;
    add_exp(my_exp * global.combo_count);
}
else
{
    global.live_count++;
    instance_create_v(x, y, obj_fluctuating_text, "1UP!");
    audio_play_sound(snd_1up_mushroom, 0, false);
    add_exp(my_exp * global.combo_count, true);
}
play_sound(global.combo_sound[global.combo_count]);
