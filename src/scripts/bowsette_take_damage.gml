///bowsette_take_damage([knockback]:int)->bool

var _knockback;

if (argument_count > 0) _knockback = argument[0];

else _knockback = 2;

if (!instance_exists(obj_bowsette) || obj_bowsette.invulnerable) return false;

if (global.current_power_up != global.power_up[PowerUpType.NORMAL])
{
    powerup_switch(global.power_up[PowerUpType.NORMAL]);
}
else 
{
    global.hp--;
    if (global.hp <= 0)
    {
        bowsette_die(object_index == obj_turned_fire);
        return true;
    }
}

with(obj_bowsette)
{
    damaged = true;
    audio_play_sound(snd_bowsette_damage, 0, false);
    floating = false;
    if (collision_line(obj_bowsette.bbox_left, obj_bowsette.bbox_top, obj_bowsette.bbox_right, obj_bowsette.bbox_top, obj_solid, true, false) == noone)
    {
        vsp = -5;
        y -= 1;
    }
    invulnerable = true;
    spd = 0;
    rhs = 0;
    sliding = false;
    sliding_current_speed = 0;
    damage_knockback = _knockback;
}

return true;
