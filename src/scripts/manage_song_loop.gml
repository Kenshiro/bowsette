///manage_song_loop(song, loop start position, loop end position)
var _song = argument0;
var _loop_start = argument1;
var _loop_end = argument2;

if (_loop_start == -1) exit;
if (audio_is_playing(_song))
{
    var _pos = audio_sound_get_track_position(_song);
    if (_pos >= _loop_end)
    {
        audio_sound_set_track_position(_song, _loop_end);
    }
}
