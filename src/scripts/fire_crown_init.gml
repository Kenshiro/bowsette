/// fire_crown_init()
with(obj_bowsette)
{
    max_airtime = 80;
    breath_fire_time = 0;
    breath_fire_time_max = 25;
    breath_fire_recharge_time = 0.6 * room_speed;
    global.fire_power = 2;
    maximum_vsp = 12;
    enabled_double_jump = true;
    spin_time = -1;
    spin_time_max = 108;
}
