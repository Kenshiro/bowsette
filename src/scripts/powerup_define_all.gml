/// powerup_define_all()
//!#import power_up.* as PowerUp
enum Spriteset
{
    STANDING,
    WALKING,
    RUNNING,
    JUMPING,
    FALLING,
    FLOATING,
    CRUNCHING,
    STANDING_BREATHFIRE,
    WALKING_BREATHFIRE,
    RUNNING_BREATHFIRE,
    JUMPING_BREATHFIRE,
    FALLING_BREATHFIRE,
    FLOATING_BREATHFIRE,
    CRUNCHING_BREATHFIRE,
    DAMAGED,
    SPINNING,
    CLIMBING,
    SLIDING
};

enum PowerUp
{
    NAME = 0,
    PALETTE = 1,
    INIT = 2,
    SKILL = 3,
    JUMP = 4
};

enum PowerUpType
{
    NORMAL,
    FIRE,
    ICE,
    THUNDER
}


var _spriteset_normal = pack(spr_bowsette, spr_bowsette_walking,
spr_bowsette_running, spr_bowsette_jumping, spr_bowsette_falling, spr_bowsette_floating,
spr_bowsette_crunch, spr_bowsette_breathfire, spr_bowsette_walking_breathfire,
spr_bowsette_running_breathfire, spr_bowsette_jumping_breathfire, 
spr_bowsette_falling_breathfire, spr_bowsette_floating_breathfire,
spr_bowsette_crunch_breathfire);

_spriteset_normal[array_length_1d(_spriteset_normal)] = spr_bowsette_damaged;
_spriteset_normal[array_length_1d(_spriteset_normal)] = spr_bowsette_spinning;
_spriteset_normal[array_length_1d(_spriteset_normal)] = spr_bowsette_climbing;
_spriteset_normal[array_length_1d(_spriteset_normal)] = spr_bowsette_sliding;

for (var i = 0; i < array_length_1d(_spriteset_normal); i++)
{
    global.bowsette_sprite[i] = powerup_generate_sprite(_spriteset_normal[i], spr_palette_normal);
}


global.power_up[PowerUpType.NORMAL] = array_create(5);
set(global.power_up[PowerUpType.NORMAL], PowerUp.NAME, "Basic Bowsette");
set(global.power_up[PowerUpType.NORMAL], PowerUp.PALETTE, spr_palette_reduced);
set(global.power_up[PowerUpType.NORMAL], PowerUp.INIT, base_crown_init);
set(global.power_up[PowerUpType.NORMAL], PowerUp.SKILL, base_crown_handle_skill);
set(global.power_up[PowerUpType.NORMAL], PowerUp.JUMP, base_crown_handle_jump);

global.power_up[PowerUpType.FIRE] = array_create(5);
set(global.power_up[PowerUpType.FIRE], PowerUp.NAME, "Fire Bowsette");
set(global.power_up[PowerUpType.FIRE], PowerUp.PALETTE, spr_palette_fire);
set(global.power_up[PowerUpType.FIRE], PowerUp.INIT, fire_crown_init);
set(global.power_up[PowerUpType.FIRE], PowerUp.SKILL, fire_crown_handle_skill);
set(global.power_up[PowerUpType.FIRE], PowerUp.JUMP, fire_crown_handle_jump);

global.power_up[PowerUpType.ICE] = array_create(5);
set(global.power_up[PowerUpType.ICE], PowerUp.NAME, "Ice Bowsette");
set(global.power_up[PowerUpType.ICE], PowerUp.PALETTE, spr_palette_ice);
set(global.power_up[PowerUpType.ICE], PowerUp.INIT, ice_crown_init);
set(global.power_up[PowerUpType.ICE], PowerUp.SKILL, ice_crown_handle_skill);
set(global.power_up[PowerUpType.ICE], PowerUp.JUMP, ice_crown_handle_jump);


global.current_power_up = global.power_up[PowerUpType.NORMAL];
global.bowsette_surface = surface_create(96, 80);
