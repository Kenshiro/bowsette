///add_coin()
global.coins++;
/*
if (global.coins >= 100)
{
    global.coins = 0;
    global.live_count++;
    play_sound(snd_1up_mushroom);
    instance_create_v(x, y, obj_fluctuating_text, "1UP");
}
*/
global.coins_this_course++;
switch(get_difficulty())
{
    case Difficulty.NORMAL:
        global.score_points += 100;
        break;
        
    case Difficulty.HARD:
        global.score_points += 200;
        break;
        
    case Difficulty.GODLIKE:
        global.score_points += 400;
        break;
}

