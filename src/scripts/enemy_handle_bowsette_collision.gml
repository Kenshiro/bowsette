/// enemy_handle_bowsette_collision()
//function used by most standard enemies that die when you jump on top of them
//if (live_call()) return live_result;
//!#import bowsette.* as bowsette
var _bowsette = enemy_collided_with_bowsette();
if (_bowsette != noone)
{
    if (bowsette_jumped_on_top(_bowsette))
    {
        falling = true;
        bowsette_handle_combo();
        bowsette_do_small_jump();
        _bowsette.just_colided = true;
        _bowsette.y = bbox_top - 1;
        hp--;
        if (hp == 0)
            died = true;
        image_yscale = -1;
        vspeed = 6;
        hspeed = 0;
        return 1;
    }
    else if (!_bowsette.invulnerable)
    {
        bowsette_take_damage();
        return 2;
    }
}

return 0;
