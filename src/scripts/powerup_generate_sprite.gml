/// powerup_generate_sprite(sprite, color palette)
// Thanks to vdweller for the original implementation

var argument_arr = array_create(argument_count);
for (var i = 0; i < argument_count; i++) {
	argument_arr[i] = argument[i];
}
//if (live_call_ext(argument_arr)) return live_result;

var _surface, _old_sprite, _new_sprite, _palette, _width, _height, _image_count;
_old_sprite = argument0;
_width = sprite_get_width(_old_sprite);
_height = sprite_get_height(_old_sprite);
_image_count = sprite_get_number(_old_sprite);
_palette = argument1;
var _surface = surface_create(_width * _image_count, _height);

surface_set_target(_surface);
draw_clear_alpha(0, 0);
shader_set(shader_basecolor);
texture_set_stage(shader_get_sampler_index(shader_basecolor, "texColmap"),
sprite_get_texture(_palette, 0));

for (var i = 0; i < _image_count; i++)
{
    if (i == 0)
    {
        draw_sprite(_old_sprite, 0, 48, 80);
        _new_sprite = sprite_create_from_surface(_surface, 0, 0, _width, _height, 
        false, false, 48, 80);
    }
    else
    {
        draw_sprite(_old_sprite, i, 48 + 96 * i, 80);
        sprite_add_from_surface(_new_sprite, _surface, 96 * i, 0, _width,
        _height, false, false);
    }
}

shader_reset();
surface_reset_target();
surface_free(_surface);
sprite_save_strip(_new_sprite, "sheet.png");
return _new_sprite;
