/// set(result, ...)
/*Thanks to FrostyCat for the basis/original idea*/

var result = argument[0];
for (var i = 1; i < argument_count-2; i++) 
{
    var k = argument[i];
    if (is_string(k))
        result = ds_map_find_value(result, k);
    else
        result = result[k];
}

var key = argument[argument_count-2];
var value = argument[argument_count-1];

if (is_string(key))
{
    ds_map_replace(result, key, value);
}
else
{
    result[@ key] = value;
}
