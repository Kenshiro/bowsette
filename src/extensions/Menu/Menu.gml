#define ui_init
// Generated at 2020-04-09 01:33:41 (627ms) for v1.4.1804+
//{ prototypes
globalvar mq_course; mq_course = [/* 0:name */undefined, /* 1:flavor_texts */undefined, /* 2:room */undefined, /* 3:number */0, /* 4:sideloaded */undefined, /* 5:song_name */undefined];
globalvar mq_collectibles_catalog; mq_collectibles_catalog = [/* 0:items */undefined];
globalvar mq_song; mq_song = [/* 0:name */undefined, /* 1:perfect_loop */undefined, /* 2:loop_start */0, /* 3:loop_end */0, /* 4:asset */undefined];
globalvar mq_inventory; mq_inventory = [];
globalvar mq_item; mq_item = [/* 0:name */undefined, /* 1:desc */undefined, /* 2:price */0, /* 3:spr_asset */undefined, /* 4:audio_asset */undefined];
globalvar mq_ui_haxe_class; mq_ui_haxe_class = [/* 0:index */0, /* 1:name */undefined, /* 2:superClass */undefined];
globalvar mq_library_book; mq_library_book = [/* 0:name */undefined, /* 1:pages */undefined, /* 2:current_page_pos */0];
globalvar mq_library_page; mq_library_page = [/* 0:title */undefined, /* 1:text */undefined, /* 2:asset */undefined, /* 3:unlocked */undefined, /* 4:seen */undefined];
globalvar mq_overworld_shop_shop_controller; mq_overworld_shop_shop_controller = [/* 0:items */undefined];
globalvar mq_ui_option; mq_ui_option = [/* 0:name */undefined, /* 1:value */0, /* 2:max_value */0, /* 3:question */undefined, /* 4:selectable */undefined, /* 5:action */undefined];
globalvar mq_ui_toggle; mq_ui_toggle = [/* 0:name */undefined, /* 1:value */0, /* 2:max_value */0, /* 3:question */undefined, /* 4:selectable */undefined, /* 5:action */undefined];
globalvar mq_ui_slider; mq_ui_slider = [/* 0:name */undefined, /* 1:value */0, /* 2:max_value */0, /* 3:question */undefined, /* 4:selectable */undefined, /* 5:action */undefined];
globalvar mq_ui_action; mq_ui_action = [/* 0:name */undefined, /* 1:value */0, /* 2:max_value */0, /* 3:question */undefined, /* 4:selectable */undefined, /* 5:action */undefined];
globalvar mq_ui_action_with_confirmation; mq_ui_action_with_confirmation = [/* 0:name */undefined, /* 1:value */0, /* 2:max_value */0, /* 3:question */undefined, /* 4:selectable */undefined, /* 5:action */undefined];
globalvar mq_ui_multiple_values; mq_ui_multiple_values = [/* 0:name */undefined, /* 1:value */0, /* 2:max_value */0, /* 3:question */undefined, /* 4:selectable */undefined, /* 5:action */undefined, /* 6:values_list */undefined];
globalvar mq_ui_empty_option; mq_ui_empty_option = [/* 0:name */undefined, /* 1:value */0, /* 2:max_value */0, /* 3:question */undefined, /* 4:selectable */undefined, /* 5:action */undefined];
//}
//{ functions
globalvar f_bowsette_interfaces_make_pause_menu_resume; f_bowsette_interfaces_make_pause_menu_resume = asset_get_index("bowsette_interfaces_make_pause_menu_resume");
globalvar f_bowsette_interfaces_library_menu_action; f_bowsette_interfaces_library_menu_action = asset_get_index("bowsette_interfaces_library_menu_action");
globalvar f_bowsette_interfaces_make_pause_menu_options_menu; f_bowsette_interfaces_make_pause_menu_options_menu = asset_get_index("bowsette_interfaces_make_pause_menu_options_menu");
globalvar f_bowsette_interfaces_make_pause_menu_quit; f_bowsette_interfaces_make_pause_menu_quit = asset_get_index("bowsette_interfaces_make_pause_menu_quit");
globalvar f_bowsette_interfaces_signal; f_bowsette_interfaces_signal = asset_get_index("bowsette_interfaces_signal");
globalvar f_bowsette_interfaces_pause_menu_action; f_bowsette_interfaces_pause_menu_action = asset_get_index("bowsette_interfaces_pause_menu_action");
globalvar f_gmtest_itest_module_run_tests; f_gmtest_itest_module_run_tests = asset_get_index("gmtest_itest_module_run_tests");
globalvar f_tmodules_tmgame_init_run_tests; f_tmodules_tmgame_init_run_tests = asset_get_index("tmodules_tmgame_init_run_tests");
globalvar f_tmodules_tmnew_game_run_tests; f_tmodules_tmnew_game_run_tests = asset_get_index("tmodules_tmnew_game_run_tests");
globalvar f_tmodules_tmrelease_run_tests; f_tmodules_tmrelease_run_tests = asset_get_index("tmodules_tmrelease_run_tests");
globalvar f_tmodules_tmsave_system_run_tests; f_tmodules_tmsave_system_run_tests = asset_get_index("tmodules_tmsave_system_run_tests");
globalvar f_ui_menu_signal; f_ui_menu_signal = asset_get_index("ui_menu_signal");
globalvar f_ui_menu_back_singal; f_ui_menu_back_singal = asset_get_index("ui_menu_back_singal");
globalvar f_ui_option_action; f_ui_option_action = asset_get_index("ui_option_action");
//}
//{ metatype
globalvar mt_course; mt_course = ui_haxe_class_create(7, "course");
globalvar mt_collectibles_catalog; mt_collectibles_catalog = ui_haxe_class_create(8, "collectibles_catalog");
globalvar mt_song; mt_song = ui_haxe_class_create(9, "song");
globalvar mt_inventory; mt_inventory = ui_haxe_class_create(10, "inventory");
globalvar mt_item; mt_item = ui_haxe_class_create(11, "item");
globalvar mt_ui_haxe_class; mt_ui_haxe_class = ui_haxe_class_create(13, "ui_haxe_class");
globalvar mt_library_book; mt_library_book = ui_haxe_class_create(16, "library_book");
globalvar mt_library_page; mt_library_page = ui_haxe_class_create(17, "library_page");
globalvar mt_overworld_shop_shop_controller; mt_overworld_shop_shop_controller = ui_haxe_class_create(18, "overworld_shop_shop_controller");
globalvar mt_ui_option; mt_ui_option = ui_haxe_class_create(19, "ui_option");
globalvar mt_ui_toggle; mt_ui_toggle = ui_haxe_class_create(20, "ui_toggle");
globalvar mt_ui_slider; mt_ui_slider = ui_haxe_class_create(21, "ui_slider");
globalvar mt_ui_action; mt_ui_action = ui_haxe_class_create(22, "ui_action");
globalvar mt_ui_action_with_confirmation; mt_ui_action_with_confirmation = ui_haxe_class_create(23, "ui_action_with_confirmation");
globalvar mt_ui_multiple_values; mt_ui_multiple_values = ui_haxe_class_create(24, "ui_multiple_values");
globalvar mt_ui_empty_option; mt_ui_empty_option = ui_haxe_class_create(25, "ui_empty_option");
//}
// course:
globalvar g_course_sideloaded_course_rooms; g_course_sideloaded_course_rooms = ds_map_create();
// game:
globalvar g_game_courses; g_game_courses = array_create(0);
globalvar g_game_bonus_courses; g_game_bonus_courses = array_create(0);
globalvar g_game_course_map; g_game_course_map = ds_map_create();
globalvar g_game_song_map; g_game_song_map = ds_map_create();
globalvar g_game_song_name_list; g_game_song_name_list = array_create(0);
globalvar g_game_global_item_catalog; g_game_global_item_catalog = collectibles_catalog_create();
// gmtest.gmtest:
globalvar g_gmtest_gmtest_test_count; g_gmtest_gmtest_test_count = 0;
globalvar g_gmtest_gmtest_successes; g_gmtest_gmtest_successes = 0;
globalvar g_gmtest_gmtest_errors; g_gmtest_gmtest_errors = 0;
globalvar g_gmtest_gmtest_current_module;
// library.manager:
globalvar g_library_manager_lands; g_library_manager_lands = library_book_create("Lands");
globalvar g_library_manager_creatures; g_library_manager_creatures = library_book_create("Creatures");
globalvar g_library_manager_palaces; g_library_manager_palaces = library_book_create("Palaces");
globalvar g_library_manager_current_book; g_library_manager_current_book = g_library_manager_lands;
globalvar g_library_manager_current_book_number; g_library_manager_current_book_number = 0;
globalvar g_library_manager_current_page_number; g_library_manager_current_page_number = 0;

//{ ui.array_hx

#define ui_array_hx_indexOf
// ui_array_hx_indexOf(arr:array<T>, v:T, i:int)->int
var _arr = argument[0], _i = argument[2];
var _len = array_length_1d(_arr);
if (_i < 0) {
    _i += _len;
    if (_i < 0) _i = 0;
}
while (_i < _len) {
    if (_arr[_i] == argument[1]) return _i;
    ++_i;
}
return -1;

//}

//{ ui.Std

#define ui_Std_string
// ui_Std_string(value:dynamic)->string
var _value = argument[0];
if (_value == undefined) return "null";
if (is_string(_value)) return _value;
if (is_real(_value)) {
    var _s = string_format(_value, 0, 16);
    var _n, _i;
    if (os_browser != -1) {
        _n = string_length(_s);
        _i = _n;
        while (_i > 0) {
            switch (string_ord_at(_s, _i)) {
                case 48:
                    --_i;
                    continue;
                case 46: --_i; break;
            }
            break;
        }
    } else {
        _n = string_byte_length(_s);
        _i = _n;
        while (_i > 0) {
            switch (string_byte_at(_s, _i)) {
                case 48:
                    --_i;
                    continue;
                case 46: --_i; break;
            }
            break;
        }
    }
    return string_copy(_s, 1, _i);
}
return string(_value);

//}

//{ ui.string_hx

#define ui_string_hx_trim
// ui_string_hx_trim(str:string)->string
var _str = argument[0];
var _char;
var _len = string_length(_str);
var _till = _len;
while (_till > 0) {
    _char = string_ord_at(_str, _till);
    if (_char == 32 || _char > 8 && _char < 14) --_till; else break;
}
if (_till < _len) _str = string_copy(_str, 1, _till);
var _start = 1;
while (_start <= _till) {
    _char = string_ord_at(_str, _start);
    if (_char == 32 || _char > 8 && _char < 14) ++_start; else break;
}
if (_start > 1) _str = string_delete(_str, 1, _start - 1);
return _str;

//}

//{ course

#define course_create
// course_create(name:string, flavorTexts:array<string>, roomName:string, songName:string, number:int, sideloaded:null<bool> = false, ?sideloadedRoom:asset)
var this; this[1,0/* metatype */] = mt_course;
array_copy(this, 0, mq_course, 0, 6);
var _name = argument[0], _sideloaded, _sideloadedRoom;
if (argument_count > 5) _sideloaded = argument[5]; else _sideloaded = false;
if (argument_count > 6) _sideloadedRoom = argument[6]; else _sideloadedRoom = undefined;
this[@0/* name */] = _name;
this[@1/* flavor_texts */] = argument[1];
this[@5/* song_name */] = argument[3];
this[@3/* number */] = argument[4];
this[@4/* sideloaded */] = _sideloaded;
if (_sideloaded) {
    this[@2/* room */] = _sideloadedRoom;
    g_course_sideloaded_course_rooms[?_name] = _sideloadedRoom;
} else this[@2/* room */] = asset_get_index(argument[2]);
return this;

#define course_add_flavor_text
// course_add_flavor_text(this:course, text:string)
var this = argument[0];
ui_haxe_boot_wset(this[1/* flavor_texts */], array_length_1d(this[1/* flavor_texts */]), argument[1]);

#define course_get_random_flavor
// course_get_random_flavor(this:course)->string
var this = argument[0];
if (array_length_1d(this[1/* flavor_texts */]) > 0) return ui_haxe_boot_wget(this[1/* flavor_texts */], irandom(array_length_1d(this[1/* flavor_texts */]) - 1)); else return "";

#define course_get_name
// course_get_name(this:course)->string
var this = argument[0];
return this[0/* name */];

#define course_get_course_song
// course_get_course_song(this:course)->song
var this = argument[0];
return game_get_song(this[5/* song_name */]);

#define course_get_song_name
// course_get_song_name(this:course)->string
var this = argument[0];
return this[5/* song_name */];

#define course_get_room
// course_get_room(this:course)->asset
var this = argument[0];
return this[2/* room */];

#define course_name_equals
// course_name_equals(this:course, name:string)->bool
var this = argument[0], _name = argument[1];
_name = string_upper(ui_string_hx_trim(_name));
return _name == string_upper(ui_string_hx_trim(this[0/* name */]));

//}

//{ collectibles.catalog

#define collectibles_catalog_create
// collectibles_catalog_create()
var this; this[1,0/* metatype */] = mt_collectibles_catalog;
array_copy(this, 0, mq_collectibles_catalog, 0, 1);
this[@0/* items */] = ds_map_create();
return this;

#define collectibles_catalog_add_item
// collectibles_catalog_add_item(this:catalog, item:item)
var this = argument[0], _item1 = argument[1];
if (ds_map_find_value(this[0/* items */], _item1[0/* name */]) == undefined) ds_map_set(this[0/* items */], _item1[0/* name */], _item1); else trace("src/collectibles/Catalog.hx:15:", "Error: there is already an item with the same name.");

#define collectibles_catalog_find_item
// collectibles_catalog_find_item(this:catalog, name:string)->item
var this = argument[0];
return ds_map_find_value(this[0/* items */], argument[1]);

//}

//{ game

#define game_init
// game_init()
var _courseData_0_name = "Brand New Grassland";
var _courseData_0_song_name = "Brand New Grassland Theme";
var _courseData_0_flavor_texts = ["A walk in the park. No need to rush things.", "Take it easy, R-E-L-A-X. Wanna have something to drink?"];
var _courseData_0_room_name = "rm_course1";
var _courseData_1_name = "Mysterious Cavern";
var _courseData_1_song_name = "Culvert";
var _courseData_1_flavor_texts = ["I should've brought a Super Repel."];
var _courseData_1_room_name = "rm_course2";
var _courseData_2_name = "Princess in the Sky with Diamonds";
var _courseData_2_song_name = "Course 3 PH";
var _courseData_2_flavor_texts = ["My heart, my wings... let us fly!", "Don't worry, these diamonds are unbreakable!"];
var _courseData_2_room_name = "rm_course3";
var _courseData_3_name = "Stay the Night";
var _courseData_3_song_name = "Space";
var _courseData_3_flavor_texts = ["On the night that left me scarred, you saved my life", "Nightly reminder that love always wins.", "If no one is special, maybe you can be what you want to be.# Your life is your own, OK?"];
var _courseData_3_room_name = "rm_course4";
var _courseData_4_name = "The Fragrance of Black Coffee";
var _courseData_4_song_name = "Amber Forest";
var _courseData_4_flavor_texts = ["Blacker than a moonless night, hotter and more bitter than hell itself.#That is coffee.", "Remember to smile no matter how bad it gets.", "Bit by bit, my heart was charmed by that radiant smile."];
var _courseData_4_room_name = "rm_course5";
var _courseData_5_name = "Vanilla Ice";
var _courseData_5_song_name = "Snowy Cottage - Day";
var _courseData_5_flavor_texts = ["Beware the miasma of the void... Vanilla Ice.", "Those who fly are justified by wearing the badge 'WE ARE THE CHOSEN FAIRIES!'."];
var _courseData_5_room_name = "rm_course6";
var _courseData_6_name = "Sweet Sweet Vibrancy";
var _courseData_6_song_name = "Space";
var _courseData_6_flavor_texts = ["Gimmicks, tricks, gossip, steps - and have some fun!"];
var _courseData_6_room_name = "rm_course7";
var _course1;
_course1 = course_create(_courseData_0_name, _courseData_0_flavor_texts, _courseData_0_room_name, _courseData_0_song_name, 0);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_0_name] = _course1;
_course1 = course_create(_courseData_1_name, _courseData_1_flavor_texts, _courseData_1_room_name, _courseData_1_song_name, 1);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_1_name] = _course1;
_course1 = course_create(_courseData_2_name, _courseData_2_flavor_texts, _courseData_2_room_name, _courseData_2_song_name, 2);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_2_name] = _course1;
_course1 = course_create(_courseData_3_name, _courseData_3_flavor_texts, _courseData_3_room_name, _courseData_3_song_name, 3);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_3_name] = _course1;
_course1 = course_create(_courseData_4_name, _courseData_4_flavor_texts, _courseData_4_room_name, _courseData_4_song_name, 4);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_4_name] = _course1;
_course1 = course_create(_courseData_5_name, _courseData_5_flavor_texts, _courseData_5_room_name, _courseData_5_song_name, 5);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_5_name] = _course1;
_course1 = course_create(_courseData_6_name, _courseData_6_flavor_texts, _courseData_6_room_name, _courseData_6_song_name, 6);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_6_name] = _course1;
var _courseData_0_name1 = "Yellow Palace";
var _courseData_0_song_name1 = "Yellow Palace Theme";
var _courseData_0_flavor_texts1 = ["Welcome to the Yellow Palace."];
var _courseData_0_room_name1 = "rm_yellow_palace";
var _courseData_1_name1 = "Red Palace";
var _courseData_1_song_name1 = "Red Heels";
var _courseData_1_flavor_texts1 = ["Welcome to the Red Palace."];
var _courseData_1_room_name1 = "rm_red_palace";
var _courseData_2_name1 = "Blue Palace";
_course1 = course_create(_courseData_0_name1, _courseData_0_flavor_texts1, _courseData_0_room_name1, _courseData_0_song_name1, 0);
g_game_bonus_courses[@array_length_1d(g_game_bonus_courses)] = _course1;
g_game_course_map[?_courseData_0_name1] = _course1;
_course1 = course_create(_courseData_1_name1, _courseData_1_flavor_texts1, _courseData_1_room_name1, _courseData_1_song_name1, 1);
g_game_bonus_courses[@array_length_1d(g_game_bonus_courses)] = _course1;
g_game_course_map[?_courseData_1_name1] = _course1;
_course1 = course_create(_courseData_2_name1, ["Welcome to the Blue Palace."], "rm_blue_palace", "Yellow Palace Theme", 2);
g_game_bonus_courses[@array_length_1d(g_game_bonus_courses)] = _course1;
g_game_course_map[?_courseData_2_name1] = _course1;
var _songData_0_name = "Yellow Palace Theme";
var _songData_0_perfect_loop = true;
var _songData_0_song_asset_name = "song_bonus_course";
var _songData_1_name = "Red Heels";
var _songData_1_perfect_loop = true;
var _songData_1_song_asset_name = "song_red_heels";
var _songData_2_name = "Brand New Grassland Theme";
var _songData_2_perfect_loop = true;
var _songData_2_song_asset_name = "song_course1";
var _songData_3_name = "Culvert";
var _songData_3_perfect_loop = true;
var _songData_3_song_asset_name = "song_course2";
var _songData_4_name = "Course 3 PH";
var _songData_4_perfect_loop = true;
var _songData_4_song_asset_name = "song_course3_placeholder";
var _songData_5_name = "Space";
var _songData_5_perfect_loop = true;
var _songData_5_song_asset_name = "song_course4_placeholder";
var _songData_6_name = "Amber Forest";
var _songData_6_perfect_loop = true;
var _songData_6_song_asset_name = "song_course5";
var _songData_7_name = "Snowy Cottage - Day";
var _songData_7_perfect_loop = true;
var _songData_7_song_asset_name = "song_course6_day";
var _songData_8_name = "Snowy Cottage - Night";
var _songData_8_perfect_loop = true;
var _songData_8_song_asset_name = "song_course6_night";
var _song1;
if (_songData_0_perfect_loop) {
    _song1 = song_create(_songData_0_name, _songData_0_song_asset_name);
    var _songName = song_get_name(_song1);
    g_game_song_map[?_songName] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName;
}
if (_songData_1_perfect_loop) {
    _song1 = song_create(_songData_1_name, _songData_1_song_asset_name);
    var _songName1 = song_get_name(_song1);
    g_game_song_map[?_songName1] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName1;
}
if (_songData_2_perfect_loop) {
    _song1 = song_create(_songData_2_name, _songData_2_song_asset_name);
    var _songName2 = song_get_name(_song1);
    g_game_song_map[?_songName2] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName2;
}
if (_songData_3_perfect_loop) {
    _song1 = song_create(_songData_3_name, _songData_3_song_asset_name);
    var _songName3 = song_get_name(_song1);
    g_game_song_map[?_songName3] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName3;
}
if (_songData_4_perfect_loop) {
    _song1 = song_create(_songData_4_name, _songData_4_song_asset_name);
    var _songName4 = song_get_name(_song1);
    g_game_song_map[?_songName4] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName4;
}
if (_songData_5_perfect_loop) {
    _song1 = song_create(_songData_5_name, _songData_5_song_asset_name);
    var _songName5 = song_get_name(_song1);
    g_game_song_map[?_songName5] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName5;
}
if (_songData_6_perfect_loop) {
    _song1 = song_create(_songData_6_name, _songData_6_song_asset_name);
    var _songName6 = song_get_name(_song1);
    g_game_song_map[?_songName6] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName6;
}
if (_songData_7_perfect_loop) {
    _song1 = song_create(_songData_7_name, _songData_7_song_asset_name);
    var _songName7 = song_get_name(_song1);
    g_game_song_map[?_songName7] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName7;
}
if (_songData_8_perfect_loop) {
    _song1 = song_create(_songData_8_name, _songData_8_song_asset_name);
    var _songName8 = song_get_name(_song1);
    g_game_song_map[?_songName8] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName8;
}
var _bookData_0_text = "This land was once a lifeless wasteland until recently, when a mage came and cast a powerful spell on it. Many Red and Blue Youseis have been residing here since then.";
var _bookData_0_title = "Brand New Grassland";
var _bookData_0_asset = "spr_land_page1_asset";
var _bookData_1_text = "86 underground caves form together and make up the massive Hachi-Roku system. 30 years ago, youseis found large amounts of potable water and Kokuseki sediments within the caverns, and an effort has been made since then to extract both.";
var _bookData_1_title = "Hachi-Roku Caverns";
var _bookData_1_asset = "spr_land_page2_asset";
var _bookData_2_text = "To more easily explore the Hachi-Roku caverns, the yousei built an infrastructure that would directly connect The Old Grasslands to one of the caves. Since they can fly, not much effort was put into making it safe.";
var _bookData_2_title = "Fantastic Cloud Infrastructure";
var _bookData_2_asset = "spr_land_page3_asset";
var _bookData_3_text = "The Old Grasslands are rural fields that belong to the yousei. One landmark is the Awakara Bridge, which is the main route for transporting coffee and other commodities. It is safe to cross at daytime, but clowns might come to prank you at night.";
var _bookData_3_title = "The Old Grasslands";
var _bookData_3_asset = "spr_land_page4_asset";
var _bookData_4_text = "The Amber Forest is famous for maintaining its colors as different seasons pass by. It is mostly habituated by wild animals and youseis that work on coffee production.";
var _bookData_4_title = "Amber Forest";
var _bookData_4_asset = "spr_land_page5_asset";
var _bookData_5_text = "A collection of villages exist in the North Mountains. These mountains form the barrier between youkai and yousei lands. The area was named by yousei, who noticed that the snow naturally tasted like vanilla ice cream.";
var _bookData_5_title = "Vanilla Ice: Part 1";
var _bookData_5_asset = "spr_land_page6_asset";
var _bookData_6_text = "Some yousei also speak cryptically of voids when asked. This has not been observed anywhere else, and could be related to living with youkais.";
var _bookData_6_title = "Vanilla Ice: Part 2";
var _bookData_6_asset = "spr_land_page6_asset";
var _title;
_title = _bookData_0_title;
library_book_add_page(g_library_manager_lands, _bookData_0_title, _bookData_0_text, _bookData_0_asset);
_title = _bookData_1_title;
library_book_add_page(g_library_manager_lands, _bookData_1_title, _bookData_1_text, _bookData_1_asset);
_title = _bookData_2_title;
library_book_add_page(g_library_manager_lands, _bookData_2_title, _bookData_2_text, _bookData_2_asset);
_title = _bookData_3_title;
library_book_add_page(g_library_manager_lands, _bookData_3_title, _bookData_3_text, _bookData_3_asset);
_title = _bookData_4_title;
library_book_add_page(g_library_manager_lands, _bookData_4_title, _bookData_4_text, _bookData_4_asset);
_title = _bookData_5_title;
library_book_add_page(g_library_manager_lands, _bookData_5_title, _bookData_5_text, _bookData_5_asset);
_title = _bookData_6_title;
library_book_add_page(g_library_manager_lands, _bookData_6_title, _bookData_6_text, _bookData_6_asset);
_title = "Blue Yousei";
library_book_add_page(g_library_manager_creatures, _title, "Blue Youseis are a cast of youseis that are born with natural magic aptitude. Only a few properly study magic, however, and many prefer to spend their time playing childish pranks. Legend says that their hearts never grow old.", "spr_creatures_page1_asset");
_title = "Red Yousei";
library_book_add_page(g_library_manager_creatures, _title, "Red Youseis are a cast of low magic aptitude youseis, perceived as more intelligent by other casts of yousei. Physically, they are undistinguishable from Blue Youseis, to the point where the two classes often live together.", "spr_creatures_page2_asset");
_title = "Spooky: Part 1";
library_book_add_page(g_library_manager_creatures, _title, "Spookies are artificial creatures with humanoid bodies and nocturnal habits. They don't have to eat, have a lifespan of 40 years, and possess intelligence comparable to that of humans.", "spr_creatures_page3_asset");
_title = "Spooky: Part 2";
library_book_add_page(g_library_manager_creatures, _title, "The very first Spooky was created by a human scientist who tried to create a slave. He died due to illness, and his creation became free.#Spookies are genderless, and thus it is unknown how they multiply.", "spr_creatures_page3_asset");
_title = "Mage Clown: Part 1";
library_book_add_page(g_library_manager_creatures, _title, "200 years ago, a civil war between human mages happened. One branch wanted to share their research with yousei casts, but another group saw this as a risk, as they used that very magic to produce goods that were sold to youseis. ", "spr_creatures_page4_asset");
_title = "Mage Clown: Part 2";
library_book_add_page(g_library_manager_creatures, _title, "The war was ended when an elder yousei unleashed a surprise attack on the humans that opposed them. The soul splitting attack was so powerful, that the affected humans ended up having their souls mix with members of a passing circus. ", "spr_creatures_page4_asset");
_title = "Mage Clown: Part 3";
library_book_add_page(g_library_manager_creatures, _title, "Even 200 years later, Mage Clown folk come out at night to entertain visitors in the area. It's not clear whether they age or not, but they don't act like they will move out from this world any time soon.", "spr_creatures_page4_asset");
_title = "Araikuma";
library_book_add_page(g_library_manager_creatures, _title, "The Araikumas are wild animals that live in the Amber Forest. They are very friendly and can be domesticated. If an Araikuma likes someone, it will mindlessly jump at them. If well fed, an Araikuma can grow to massive sizes.", "spr_creatures_page5_asset");
_title = "Ice Servant: Part 1";
library_book_add_page(g_library_manager_creatures, _title, "Sentient constructs of pure steel, made by yousei as protectors. It takes a yousei with strong levels of magic to make one, though after creation they are fully autonomous and can be commanded even by lesser yousei casts.", "spr_creatures_page6_asset");
_title = "Ice Servant: Part 2";
library_book_add_page(g_library_manager_creatures, _title, "They feature Artificial Intelligence, Yousechain, Fairy of Things, Small Services architecture, Quantum Magic, and many more. Despite this knowledge, no one knows how they work. Even the youseis that built them.", "spr_creatures_page6_asset");
_title = "Yellow Palace: Part 1";
library_book_add_page(g_library_manager_palaces, _title, "During the Great War between the youseis and youkais, the youseis clans united to maximize tool and weapon production. The war ended with no winners - all countries instead having their governments crumble into smaller communities.", "spr_palaces_page1_asset");
_title = "Yellow Palace: Part 2";
library_book_add_page(g_library_manager_palaces, _title, "One of the youseis' most powerful creations at the time were the Power Crowns. They infused the power of elemental warriors into four different crowns, then built four massive factories that would make replicas of these crowns. ", "spr_palaces_page1_asset");
_title = "Yellow Palace: Part 3";
library_book_add_page(g_library_manager_palaces, _title, "Recently, the Yellow Palace, a repurposed Thunder Crown factory, had its interior modified. A village leader, in response to dragons appearing and stealing livestock, is using it to produce mushrooms which could boost one's vitality.", "spr_palaces_page1_asset");
_title = "Red Palace: Part 1";
library_book_add_page(g_library_manager_palaces, _title, "The Red Palace was once a huge factory that replicated the Fire Crown. After the war between youseis and youkais ended, the factory got reformed as a palace and was guarded by Red Youseis since then.", "spr_palaces_page2_asset");
_title = "Red Palace: Part 2";
library_book_add_page(g_library_manager_palaces, _title, "The original Fire Crown was a normal crown which got infused with the soul of Aluza, a reputable martial artist with commendable fire magic and acrobatic skills. The replicas could give their users a fraction of her abilities. ", "spr_palaces_page2_asset");
_title = "Red Palace: Part 3";
library_book_add_page(g_library_manager_palaces, _title, "Nowadays, the Red Palace's entrance is blocked by a mercenary mage of unknown origins. He is instructed by the yousei to not accept bribery in exchange for entry into the palace.", "spr_palaces_page2_asset");
collectibles_catalog_add_item(g_game_global_item_catalog, item_create("Bread", "A delicious piece of bread.", 10, "spr_item_bread"));
collectibles_catalog_add_item(g_game_global_item_catalog, item_create("Banana", "Get some energy! This banana restores 1 Life Point.", 10, "spr_item_banana"));
collectibles_catalog_add_item(g_game_global_item_catalog, item_create("Apple", "Just a single apple. It's not worth much.", 5, "spr_item_apple"));
collectibles_catalog_add_item(g_game_global_item_catalog, item_create("Shiny Diamond", "A huge diamond. It's probably worth a lot.", 100, "spr_diamond"));

#define game_get_course
// game_get_course(name:string)->course
return g_game_course_map[?argument[0]];

#define game_get_song
// game_get_song(name:string)->song
var _name = argument[0];
if (g_game_song_map[?_name] != undefined) {
    return g_game_song_map[?_name];
} else {
    trace("src/bowsette/Game.hx:55:", 'Warning: unknown song "' + _name + '". Returning the first song added instead.');
    return g_game_song_map[?g_game_song_name_list[0]];
}

#define game_add_song
// game_add_song(song:song)
var _song1 = argument[0];
var _songName = song_get_name(_song1);
g_game_song_map[?_songName] = _song1;
g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName;

#define game_initialize_course_data
// game_initialize_course_data()
var _courseData_0_name = "Brand New Grassland";
var _courseData_0_song_name = "Brand New Grassland Theme";
var _courseData_0_flavor_texts = ["A walk in the park. No need to rush things.", "Take it easy, R-E-L-A-X. Wanna have something to drink?"];
var _courseData_0_room_name = "rm_course1";
var _courseData_1_name = "Mysterious Cavern";
var _courseData_1_song_name = "Culvert";
var _courseData_1_flavor_texts = ["I should've brought a Super Repel."];
var _courseData_1_room_name = "rm_course2";
var _courseData_2_name = "Princess in the Sky with Diamonds";
var _courseData_2_song_name = "Course 3 PH";
var _courseData_2_flavor_texts = ["My heart, my wings... let us fly!", "Don't worry, these diamonds are unbreakable!"];
var _courseData_2_room_name = "rm_course3";
var _courseData_3_name = "Stay the Night";
var _courseData_3_song_name = "Space";
var _courseData_3_flavor_texts = ["On the night that left me scarred, you saved my life", "Nightly reminder that love always wins.", "If no one is special, maybe you can be what you want to be.# Your life is your own, OK?"];
var _courseData_3_room_name = "rm_course4";
var _courseData_4_name = "The Fragrance of Black Coffee";
var _courseData_4_song_name = "Amber Forest";
var _courseData_4_flavor_texts = ["Blacker than a moonless night, hotter and more bitter than hell itself.#That is coffee.", "Remember to smile no matter how bad it gets.", "Bit by bit, my heart was charmed by that radiant smile."];
var _courseData_4_room_name = "rm_course5";
var _courseData_5_name = "Vanilla Ice";
var _courseData_5_song_name = "Snowy Cottage - Day";
var _courseData_5_flavor_texts = ["Beware the miasma of the void... Vanilla Ice.", "Those who fly are justified by wearing the badge 'WE ARE THE CHOSEN FAIRIES!'."];
var _courseData_5_room_name = "rm_course6";
var _courseData_6_name = "Sweet Sweet Vibrancy";
var _courseData_6_song_name = "Space";
var _courseData_6_flavor_texts = ["Gimmicks, tricks, gossip, steps - and have some fun!"];
var _courseData_6_room_name = "rm_course7";
var _course1;
_course1 = course_create(_courseData_0_name, _courseData_0_flavor_texts, _courseData_0_room_name, _courseData_0_song_name, 0);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_0_name] = _course1;
_course1 = course_create(_courseData_1_name, _courseData_1_flavor_texts, _courseData_1_room_name, _courseData_1_song_name, 1);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_1_name] = _course1;
_course1 = course_create(_courseData_2_name, _courseData_2_flavor_texts, _courseData_2_room_name, _courseData_2_song_name, 2);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_2_name] = _course1;
_course1 = course_create(_courseData_3_name, _courseData_3_flavor_texts, _courseData_3_room_name, _courseData_3_song_name, 3);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_3_name] = _course1;
_course1 = course_create(_courseData_4_name, _courseData_4_flavor_texts, _courseData_4_room_name, _courseData_4_song_name, 4);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_4_name] = _course1;
_course1 = course_create(_courseData_5_name, _courseData_5_flavor_texts, _courseData_5_room_name, _courseData_5_song_name, 5);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_5_name] = _course1;
_course1 = course_create(_courseData_6_name, _courseData_6_flavor_texts, _courseData_6_room_name, _courseData_6_song_name, 6);
g_game_courses[@array_length_1d(g_game_courses)] = _course1;
g_game_course_map[?_courseData_6_name] = _course1;
var _courseData_0_name1 = "Yellow Palace";
var _courseData_0_song_name1 = "Yellow Palace Theme";
var _courseData_0_flavor_texts1 = ["Welcome to the Yellow Palace."];
var _courseData_0_room_name1 = "rm_yellow_palace";
var _courseData_1_name1 = "Red Palace";
var _courseData_1_song_name1 = "Red Heels";
var _courseData_1_flavor_texts1 = ["Welcome to the Red Palace."];
var _courseData_1_room_name1 = "rm_red_palace";
var _courseData_2_name1 = "Blue Palace";
_course1 = course_create(_courseData_0_name1, _courseData_0_flavor_texts1, _courseData_0_room_name1, _courseData_0_song_name1, 0);
g_game_bonus_courses[@array_length_1d(g_game_bonus_courses)] = _course1;
g_game_course_map[?_courseData_0_name1] = _course1;
_course1 = course_create(_courseData_1_name1, _courseData_1_flavor_texts1, _courseData_1_room_name1, _courseData_1_song_name1, 1);
g_game_bonus_courses[@array_length_1d(g_game_bonus_courses)] = _course1;
g_game_course_map[?_courseData_1_name1] = _course1;
_course1 = course_create(_courseData_2_name1, ["Welcome to the Blue Palace."], "rm_blue_palace", "Yellow Palace Theme", 2);
g_game_bonus_courses[@array_length_1d(g_game_bonus_courses)] = _course1;
g_game_course_map[?_courseData_2_name1] = _course1;
var _songData_0_name = "Yellow Palace Theme";
var _songData_0_perfect_loop = true;
var _songData_0_song_asset_name = "song_bonus_course";
var _songData_1_name = "Red Heels";
var _songData_1_perfect_loop = true;
var _songData_1_song_asset_name = "song_red_heels";
var _songData_2_name = "Brand New Grassland Theme";
var _songData_2_perfect_loop = true;
var _songData_2_song_asset_name = "song_course1";
var _songData_3_name = "Culvert";
var _songData_3_perfect_loop = true;
var _songData_3_song_asset_name = "song_course2";
var _songData_4_name = "Course 3 PH";
var _songData_4_perfect_loop = true;
var _songData_4_song_asset_name = "song_course3_placeholder";
var _songData_5_name = "Space";
var _songData_5_perfect_loop = true;
var _songData_5_song_asset_name = "song_course4_placeholder";
var _songData_6_name = "Amber Forest";
var _songData_6_perfect_loop = true;
var _songData_6_song_asset_name = "song_course5";
var _songData_7_name = "Snowy Cottage - Day";
var _songData_7_perfect_loop = true;
var _songData_7_song_asset_name = "song_course6_day";
var _songData_8_name = "Snowy Cottage - Night";
var _songData_8_perfect_loop = true;
var _songData_8_song_asset_name = "song_course6_night";
var _song1;
if (_songData_0_perfect_loop) {
    _song1 = song_create(_songData_0_name, _songData_0_song_asset_name);
    var _songName = song_get_name(_song1);
    g_game_song_map[?_songName] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName;
}
if (_songData_1_perfect_loop) {
    _song1 = song_create(_songData_1_name, _songData_1_song_asset_name);
    var _songName1 = song_get_name(_song1);
    g_game_song_map[?_songName1] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName1;
}
if (_songData_2_perfect_loop) {
    _song1 = song_create(_songData_2_name, _songData_2_song_asset_name);
    var _songName2 = song_get_name(_song1);
    g_game_song_map[?_songName2] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName2;
}
if (_songData_3_perfect_loop) {
    _song1 = song_create(_songData_3_name, _songData_3_song_asset_name);
    var _songName3 = song_get_name(_song1);
    g_game_song_map[?_songName3] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName3;
}
if (_songData_4_perfect_loop) {
    _song1 = song_create(_songData_4_name, _songData_4_song_asset_name);
    var _songName4 = song_get_name(_song1);
    g_game_song_map[?_songName4] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName4;
}
if (_songData_5_perfect_loop) {
    _song1 = song_create(_songData_5_name, _songData_5_song_asset_name);
    var _songName5 = song_get_name(_song1);
    g_game_song_map[?_songName5] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName5;
}
if (_songData_6_perfect_loop) {
    _song1 = song_create(_songData_6_name, _songData_6_song_asset_name);
    var _songName6 = song_get_name(_song1);
    g_game_song_map[?_songName6] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName6;
}
if (_songData_7_perfect_loop) {
    _song1 = song_create(_songData_7_name, _songData_7_song_asset_name);
    var _songName7 = song_get_name(_song1);
    g_game_song_map[?_songName7] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName7;
}
if (_songData_8_perfect_loop) {
    _song1 = song_create(_songData_8_name, _songData_8_song_asset_name);
    var _songName8 = song_get_name(_song1);
    g_game_song_map[?_songName8] = _song1;
    g_game_song_name_list[@array_length_1d(g_game_song_name_list)] = _songName8;
}

#define game_initialize_book_data
// game_initialize_book_data()
var _bookData_0_text = "This land was once a lifeless wasteland until recently, when a mage came and cast a powerful spell on it. Many Red and Blue Youseis have been residing here since then.";
var _bookData_0_title = "Brand New Grassland";
var _bookData_0_asset = "spr_land_page1_asset";
var _bookData_1_text = "86 underground caves form together and make up the massive Hachi-Roku system. 30 years ago, youseis found large amounts of potable water and Kokuseki sediments within the caverns, and an effort has been made since then to extract both.";
var _bookData_1_title = "Hachi-Roku Caverns";
var _bookData_1_asset = "spr_land_page2_asset";
var _bookData_2_text = "To more easily explore the Hachi-Roku caverns, the yousei built an infrastructure that would directly connect The Old Grasslands to one of the caves. Since they can fly, not much effort was put into making it safe.";
var _bookData_2_title = "Fantastic Cloud Infrastructure";
var _bookData_2_asset = "spr_land_page3_asset";
var _bookData_3_text = "The Old Grasslands are rural fields that belong to the yousei. One landmark is the Awakara Bridge, which is the main route for transporting coffee and other commodities. It is safe to cross at daytime, but clowns might come to prank you at night.";
var _bookData_3_title = "The Old Grasslands";
var _bookData_3_asset = "spr_land_page4_asset";
var _bookData_4_text = "The Amber Forest is famous for maintaining its colors as different seasons pass by. It is mostly habituated by wild animals and youseis that work on coffee production.";
var _bookData_4_title = "Amber Forest";
var _bookData_4_asset = "spr_land_page5_asset";
var _bookData_5_text = "A collection of villages exist in the North Mountains. These mountains form the barrier between youkai and yousei lands. The area was named by yousei, who noticed that the snow naturally tasted like vanilla ice cream.";
var _bookData_5_title = "Vanilla Ice: Part 1";
var _bookData_5_asset = "spr_land_page6_asset";
var _bookData_6_text = "Some yousei also speak cryptically of voids when asked. This has not been observed anywhere else, and could be related to living with youkais.";
var _bookData_6_title = "Vanilla Ice: Part 2";
var _bookData_6_asset = "spr_land_page6_asset";
var _title;
_title = _bookData_0_title;
library_book_add_page(g_library_manager_lands, _bookData_0_title, _bookData_0_text, _bookData_0_asset);
_title = _bookData_1_title;
library_book_add_page(g_library_manager_lands, _bookData_1_title, _bookData_1_text, _bookData_1_asset);
_title = _bookData_2_title;
library_book_add_page(g_library_manager_lands, _bookData_2_title, _bookData_2_text, _bookData_2_asset);
_title = _bookData_3_title;
library_book_add_page(g_library_manager_lands, _bookData_3_title, _bookData_3_text, _bookData_3_asset);
_title = _bookData_4_title;
library_book_add_page(g_library_manager_lands, _bookData_4_title, _bookData_4_text, _bookData_4_asset);
_title = _bookData_5_title;
library_book_add_page(g_library_manager_lands, _bookData_5_title, _bookData_5_text, _bookData_5_asset);
_title = _bookData_6_title;
library_book_add_page(g_library_manager_lands, _bookData_6_title, _bookData_6_text, _bookData_6_asset);
_title = "Blue Yousei";
library_book_add_page(g_library_manager_creatures, _title, "Blue Youseis are a cast of youseis that are born with natural magic aptitude. Only a few properly study magic, however, and many prefer to spend their time playing childish pranks. Legend says that their hearts never grow old.", "spr_creatures_page1_asset");
_title = "Red Yousei";
library_book_add_page(g_library_manager_creatures, _title, "Red Youseis are a cast of low magic aptitude youseis, perceived as more intelligent by other casts of yousei. Physically, they are undistinguishable from Blue Youseis, to the point where the two classes often live together.", "spr_creatures_page2_asset");
_title = "Spooky: Part 1";
library_book_add_page(g_library_manager_creatures, _title, "Spookies are artificial creatures with humanoid bodies and nocturnal habits. They don't have to eat, have a lifespan of 40 years, and possess intelligence comparable to that of humans.", "spr_creatures_page3_asset");
_title = "Spooky: Part 2";
library_book_add_page(g_library_manager_creatures, _title, "The very first Spooky was created by a human scientist who tried to create a slave. He died due to illness, and his creation became free.#Spookies are genderless, and thus it is unknown how they multiply.", "spr_creatures_page3_asset");
_title = "Mage Clown: Part 1";
library_book_add_page(g_library_manager_creatures, _title, "200 years ago, a civil war between human mages happened. One branch wanted to share their research with yousei casts, but another group saw this as a risk, as they used that very magic to produce goods that were sold to youseis. ", "spr_creatures_page4_asset");
_title = "Mage Clown: Part 2";
library_book_add_page(g_library_manager_creatures, _title, "The war was ended when an elder yousei unleashed a surprise attack on the humans that opposed them. The soul splitting attack was so powerful, that the affected humans ended up having their souls mix with members of a passing circus. ", "spr_creatures_page4_asset");
_title = "Mage Clown: Part 3";
library_book_add_page(g_library_manager_creatures, _title, "Even 200 years later, Mage Clown folk come out at night to entertain visitors in the area. It's not clear whether they age or not, but they don't act like they will move out from this world any time soon.", "spr_creatures_page4_asset");
_title = "Araikuma";
library_book_add_page(g_library_manager_creatures, _title, "The Araikumas are wild animals that live in the Amber Forest. They are very friendly and can be domesticated. If an Araikuma likes someone, it will mindlessly jump at them. If well fed, an Araikuma can grow to massive sizes.", "spr_creatures_page5_asset");
_title = "Ice Servant: Part 1";
library_book_add_page(g_library_manager_creatures, _title, "Sentient constructs of pure steel, made by yousei as protectors. It takes a yousei with strong levels of magic to make one, though after creation they are fully autonomous and can be commanded even by lesser yousei casts.", "spr_creatures_page6_asset");
_title = "Ice Servant: Part 2";
library_book_add_page(g_library_manager_creatures, _title, "They feature Artificial Intelligence, Yousechain, Fairy of Things, Small Services architecture, Quantum Magic, and many more. Despite this knowledge, no one knows how they work. Even the youseis that built them.", "spr_creatures_page6_asset");
_title = "Yellow Palace: Part 1";
library_book_add_page(g_library_manager_palaces, _title, "During the Great War between the youseis and youkais, the youseis clans united to maximize tool and weapon production. The war ended with no winners - all countries instead having their governments crumble into smaller communities.", "spr_palaces_page1_asset");
_title = "Yellow Palace: Part 2";
library_book_add_page(g_library_manager_palaces, _title, "One of the youseis' most powerful creations at the time were the Power Crowns. They infused the power of elemental warriors into four different crowns, then built four massive factories that would make replicas of these crowns. ", "spr_palaces_page1_asset");
_title = "Yellow Palace: Part 3";
library_book_add_page(g_library_manager_palaces, _title, "Recently, the Yellow Palace, a repurposed Thunder Crown factory, had its interior modified. A village leader, in response to dragons appearing and stealing livestock, is using it to produce mushrooms which could boost one's vitality.", "spr_palaces_page1_asset");
_title = "Red Palace: Part 1";
library_book_add_page(g_library_manager_palaces, _title, "The Red Palace was once a huge factory that replicated the Fire Crown. After the war between youseis and youkais ended, the factory got reformed as a palace and was guarded by Red Youseis since then.", "spr_palaces_page2_asset");
_title = "Red Palace: Part 2";
library_book_add_page(g_library_manager_palaces, _title, "The original Fire Crown was a normal crown which got infused with the soul of Aluza, a reputable martial artist with commendable fire magic and acrobatic skills. The replicas could give their users a fraction of her abilities. ", "spr_palaces_page2_asset");
_title = "Red Palace: Part 3";
library_book_add_page(g_library_manager_palaces, _title, "Nowadays, the Red Palace's entrance is blocked by a mercenary mage of unknown origins. He is instructed by the yousei to not accept bribery in exchange for entry into the palace.", "spr_palaces_page2_asset");

#define game_load_item_data
// game_load_item_data()
collectibles_catalog_add_item(g_game_global_item_catalog, item_create("Bread", "A delicious piece of bread.", 10, "spr_item_bread"));
collectibles_catalog_add_item(g_game_global_item_catalog, item_create("Banana", "Get some energy! This banana restores 1 Life Point.", 10, "spr_item_banana"));
collectibles_catalog_add_item(g_game_global_item_catalog, item_create("Apple", "Just a single apple. It's not worth much.", 5, "spr_item_apple"));
collectibles_catalog_add_item(g_game_global_item_catalog, item_create("Shiny Diamond", "A huge diamond. It's probably worth a lot.", 100, "spr_diamond"));

#define game_load_page_save_data
// game_load_page_save_data(page:Page)
var _page = argument[0];
var _title = library_page_get_title(_page);

#define game_get_course_list
// game_get_course_list()->array<course>
return g_game_courses;

#define game_get_course_by_position
// game_get_course_by_position(pos:int)->course
var _pos = argument[0];
return g_game_courses[_pos];

#define game_get_bonus_course_list
// game_get_bonus_course_list()->array<course>
return g_game_bonus_courses;

#define game_get_course_position
// game_get_course_position(course:course)->int
return ui_array_hx_indexOf(g_game_courses, argument[0], 0);

#define game_run_all_tests
// game_run_all_tests()
gmtest_gmtest_run([tmodules_tmgame_init_create(), tmodules_tmnew_game_create(), tmodules_tmrelease_create(), tmodules_tmsave_system_create()]);

#define game_run_new_game_tests
// game_run_new_game_tests()
gmtest_gmtest_run([tmodules_tmnew_game_create()]);

//}

//{ bowsette.interfaces

#define bowsette_interfaces_signal
// bowsette_interfaces_signal(setting:string, value:any)
var _value = argument[1];
switch (argument[0]) {
    case "Fullscreen Mode":
        trace("src/bowsette/Interfaces.hx:15:", "Switching fullscreen value to " + ui_Std_string(_value));
        window_set_fullscreen(_value);
        save_system_save_config();
        global.fullscreen_mode = _value;
        break;
    case "Scale":
        trace("src/bowsette/Interfaces.hx:21:", "Changing scale to " + (ui_Std_string((_value | 0) + 1) + "x") + ".");
        update_resolution(_value);
        save_system_save_config();
        global.scale = _value;
        break;
    case "Book":
        library_manager_choose_book_to_view(_value);
        reading_a_page = true;
        state = MenuState.LIBRARY;
        current_menu = page_menu;
        pause_menu_update_page();
        trace("src/bowsette/Interfaces.hx:31:", "Opening a book.");
        break;
}

#define bowsette_interfaces_make_pause_menu_resume
// bowsette_interfaces_make_pause_menu_resume()
audio_play_sound(snd_unpause, 0, false);
unpaused = true;
alarm[0] = 20;

#define bowsette_interfaces_make_pause_menu_options_menu
// bowsette_interfaces_make_pause_menu_options_menu()
current_menu = options_menu;
trace("src/bowsette/Interfaces.hx:44:", "Going to options menu.");

#define bowsette_interfaces_make_pause_menu_quit
// bowsette_interfaces_make_pause_menu_quit()
fade_to_room(rm_menu);
audio_sound_gain(global.song, 0, 1000);
trace("src/bowsette/Interfaces.hx:50:", "Quitting from the pause menu.");

#define bowsette_interfaces_make_pause_menu
// bowsette_interfaces_make_pause_menu()->Menu
var _menu = ui_menu_create([ui_action_create("Resume", f_bowsette_interfaces_make_pause_menu_resume), ui_action_create("Library", f_bowsette_interfaces_library_menu_action), ui_action_create("Options", f_bowsette_interfaces_make_pause_menu_options_menu), ui_action_with_confirmation_create("Quit", f_bowsette_interfaces_make_pause_menu_quit, "Are you sure you want to quit?")], f_bowsette_interfaces_signal);
_menu[@6/* backable */] = true;
_menu[@8/* back_singal */] = f_bowsette_interfaces_make_pause_menu_resume;
return _menu;

#define bowsette_interfaces_make_book_menu
// bowsette_interfaces_make_book_menu()->Menu
var _menu = ui_menu_create([ui_multiple_values_create("Book", ["Land", "Species", "Palace"], g_library_manager_current_book_number, true), ui_action_create("Back", f_bowsette_interfaces_pause_menu_action)], f_bowsette_interfaces_signal);
_menu[@6/* backable */] = true;
_menu[@8/* back_singal */] = f_bowsette_interfaces_pause_menu_action;
return _menu;

#define bowsette_interfaces_make_page_menu
// bowsette_interfaces_make_page_menu()->Menu
var _option1 = ui_empty_option_create("Page");
var _option2 = ui_action_create("Back", f_bowsette_interfaces_library_menu_action);
var _menu = ui_menu_create([_option1], f_bowsette_interfaces_signal);
_menu[@6/* backable */] = true;
_menu[@8/* back_singal */] = f_bowsette_interfaces_library_menu_action;
return _menu;

#define bowsette_interfaces_make_settings_menu
// bowsette_interfaces_make_settings_menu(scale:int, isFullscreen:EitherType<int; bool>, musicVolume:int)->Menu
var _menu = ui_menu_create([ui_multiple_values_create("Scale", ["1x", "2x", "4x"], argument[0]), ui_toggle_create("Fullscreen Mode", argument[1]), ui_slider_create("Music Volume", argument[2]), ui_action_create("Back", f_bowsette_interfaces_pause_menu_action)], f_bowsette_interfaces_signal);
_menu[@6/* backable */] = true;
_menu[@8/* back_singal */] = f_bowsette_interfaces_pause_menu_action;
return _menu;

#define bowsette_interfaces_pause_menu_action
// bowsette_interfaces_pause_menu_action()
current_menu = pause_menu;
reading_a_page = false;
state = MenuState.PAUSE;
trace("src/bowsette/Interfaces.hx:104:", "Going back to pause menu.");
obj_hud_controller.hide_hud = false;;

#define bowsette_interfaces_library_menu_action
// bowsette_interfaces_library_menu_action()
reading_a_page = false;
state = MenuState.PAUSE;
current_menu = library_menu;
obj_hud_controller.hide_hud = true;;
audio_play_sound(snd_menu_select, 0, false);

//}

//{ song

#define song_create
// song_create(name:string, songName:string, perfectLoop:null<bool> = true, ?loopStart:real, ?loopEnd:real)
var this; this[1,0/* metatype */] = mt_song;
array_copy(this, 0, mq_song, 0, 5);
var _perfectLoop, _loopStart, _loopEnd;
if (argument_count > 2) _perfectLoop = argument[2]; else _perfectLoop = true;
if (argument_count > 3) _loopStart = argument[3]; else _loopStart = undefined;
if (argument_count > 4) _loopEnd = argument[4]; else _loopEnd = undefined;
this[@0/* name */] = argument[0];
this[@4/* asset */] = asset_get_index(argument[1]);
this[@1/* perfect_loop */] = _perfectLoop;
if (!_perfectLoop) {
    this[@2/* loop_start */] = _loopStart;
    this[@3/* loop_end */] = _loopEnd;
}
return this;

#define song_get_name
// song_get_name(this:song)->string
var this = argument[0];
return this[0/* name */];

#define song_get_perfect_loop
// song_get_perfect_loop(this:song)->bool
var this = argument[0];
return this[1/* perfect_loop */];

#define song_get_loop_start
// song_get_loop_start(this:song)->real
var this = argument[0];
return this[2/* loop_start */];

#define song_get_loop_end
// song_get_loop_end(this:song)->real
var this = argument[0];
return this[3/* loop_end */];

#define song_get_asset
// song_get_asset(this:song)->asset
var this = argument[0];
return this[4/* asset */];

//}

//{ inventory

#define inventory_create
// inventory_create(size:int)
var this = instance_create(0, 0, obj_inventory_controller);
this.__class__ = mt_inventory;
var _size = argument[0];
this.items = ds_list_create();
this.items_quantity = ds_list_create();
this.size_limit = (_size > 0 ? _size : 9999);
this.persistent = true;
return this;

#define inventory_add
// inventory_add(this:inventory, id:string, quantity:int)->bool
var this = argument[0], _id = argument[1], _quantity = argument[2];
var _index = ds_list_find_index(this.items, _id);
if (_index != -1) {
    trace("src/collectibles/Inventory.hx:23:", "Adding " + string(_quantity) + " units of " + _id + " to the inventory.");
    var __g = _index;
    var __g1 = this.items_quantity;
    __g1[|__g] = (__g1[|__g] + _quantity);
    return true;
}
if (ds_list_size(this.items) < this.size_limit) {
    trace("src/collectibles/Inventory.hx:28:", "Adding " + _id + " to the inventory. Quantity: " + string(_quantity) + ". You did not have this item before.");
    ds_list_add(this.items, _id);
    ds_list_add(this.items_quantity, _quantity);
    return true;
}
trace("src/collectibles/Inventory.hx:33:", "Inventory is full.");
return false;

#define inventory_remove
// inventory_remove(this:inventory, id:string, quantity:int)->bool
var this = argument[0], _id = argument[1], _quantity = argument[2];
var _index = ds_list_find_index(this.items, _id);
if (_index != -1) {
    trace("src/collectibles/Inventory.hx:40:", "Removing " + _id + " units of " + _id + " to the inventory.");
    if (ds_list_find_value(this.items_quantity, _index) > _quantity) {
        var __g = _index;
        var __g1 = this.items_quantity;
        __g1[|__g] = (__g1[|__g] - _quantity);
    } else {
        if (ds_list_find_value(this.items_quantity, _index) < _quantity) trace("src/collectibles/Inventory.hx:45:", "Warning: the amount removed is higher than the inventory had.");
        ds_list_delete(this.items, _index);
        ds_list_delete(this.items_quantity, _index);
    }
    return true;
}
return false;

#define inventory_has_item
// inventory_has_item(this:inventory, id:string)->bool
var this = argument[0];
return ds_list_find_index(this.items, argument[1]) != -1;

#define inventory_has_quantity
// inventory_has_quantity(this:inventory, id:string, quantity:int)->bool
var this = argument[0];
var _index = ds_list_find_index(this.items, argument[1]);
return _index != -1 && ds_list_find_value(this.items_quantity, _index) >= argument[2];

#define inventory_get_quantity
// inventory_get_quantity(this:inventory, id:string)->int
var this = argument[0];
var _index = ds_list_find_index(this.items, argument[1]);
if (_index == -1) return 0; else return ds_list_find_value(this.items_quantity, _index);

#define inventory_get_length
// inventory_get_length(this:inventory)->int
var this = argument[0];
return ds_list_size(this.items);

#define inventory_get_item_list
// inventory_get_item_list(this:inventory)->ds_list<string>
var this = argument[0];
return this.items;

#define inventory_get_quantity_list
// inventory_get_quantity_list(this:inventory)->ds_list<int>
var this = argument[0];
return this.items_quantity;

//}

//{ item

#define item_create
// item_create(name:string, desc:string, price:int, spriteName:string, ?audioAsset:string)
var this; this[1,0/* metatype */] = mt_item;
array_copy(this, 0, mq_item, 0, 5);
var _audioAsset;
if (argument_count > 4) _audioAsset = argument[4]; else _audioAsset = undefined;
this[@0/* name */] = argument[0];
this[@1/* desc */] = argument[1];
this[@2/* price */] = argument[2];
this[@3/* spr_asset */] = asset_get_index(argument[3]);
if (_audioAsset != undefined) this[@4/* audio_asset */] = _audioAsset;
return this;

//}

//{ ui.haxe.class

#define ui_haxe_class_create
// ui_haxe_class_create(id:int, name:string)
var this; this[1,0/* metatype */] = "mt_ui_haxe_class";
array_copy(this, 0, mq_ui_haxe_class, 0, 3);
this[@2/* superClass */] = undefined;
this[@0/* index */] = argument[0];
this[@1/* name */] = argument[1];
return this;

//}

//{ gmtest.assert

#define gmtest_assert_is_true
// gmtest_assert_is_true(expression:bool, ?errorMessage:string)
var _errorMessage;
if (argument_count > 1) _errorMessage = argument[1]; else _errorMessage = undefined;
++g_gmtest_gmtest_test_count;
if (argument[0]) {
    ++g_gmtest_gmtest_successes;
} else {
    if (_errorMessage == undefined) _errorMessage = "Context: expression should be true.";
    gmtest_gmtest_throw_test_error(_errorMessage);
}

#define gmtest_assert_is_false
// gmtest_assert_is_false(expression:bool, ?errorMessage:string)
var _errorMessage;
if (argument_count > 1) _errorMessage = argument[1]; else _errorMessage = undefined;
++g_gmtest_gmtest_test_count;
if (argument[0]) {
    if (_errorMessage == undefined) _errorMessage = "Context: expression should be false.";
    gmtest_gmtest_throw_test_error(_errorMessage);
} else g_gmtest_gmtest_successes++;

#define gmtest_assert_is_between
// gmtest_assert_is_between(value:real, min:real, max:real, ?errorMessage:string)
var _value = argument[0], _min1 = argument[1], _max1 = argument[2], _errorMessage;
if (argument_count > 3) _errorMessage = argument[3]; else _errorMessage = undefined;
++g_gmtest_gmtest_test_count;
if (_min1 == _max1) {
    gmtest_gmtest_throw_test_error('Context: minimum and maximum values are the same. Used "equals" instead.');
    return 0;
}
if (_min1 > _max1) {
    gmtest_gmtest_throw_test_error("Context: minimum value is higher than the maximum value..");
    return 0;
}
if (_value >= _min1 && _value <= _max1) {
    ++g_gmtest_gmtest_successes;
} else {
    if (_errorMessage == undefined) gmtest_gmtest_throw_test_error("Context: value is NOT between " + ui_Std_string(_min1) + " and " + ui_Std_string(_max1) + " inclusive.");
    gmtest_gmtest_throw_test_error(_errorMessage);
}

#define gmtest_assert_equals
// gmtest_assert_equals(a:any, b:any, ?errorMessage:string)
var _a = argument[0], _b = argument[1], _errorMessage;
if (argument_count > 2) _errorMessage = argument[2]; else _errorMessage = undefined;
++g_gmtest_gmtest_test_count;
if (_a != _b) {
    if (is_array(_a) && is_array(_b)) {
        if (array_equals(_a, _b)) {
            ++g_gmtest_gmtest_successes;
            return 0;
        }
    }
    if (_errorMessage == undefined) _errorMessage = "Context: " + ui_Std_string(_a) + " and " + ui_Std_string(_b) + " are not equal!";
    gmtest_gmtest_throw_test_error(_errorMessage);
    return 0;
}
++g_gmtest_gmtest_successes;

//}

//{ gmtest.gmtest

#define gmtest_gmtest_run
// gmtest_gmtest_run(modules:array<ITestModule>)
var _modules = argument[0];
show_debug_message(("Running the test suite. A total of " + string(array_length_1d(_modules)) + " test modules were passed to the suite."));
show_debug_message("Modules being tested:");
var _mNumber = 0;
var __g = 0;
while (__g < array_length_1d(_modules)) {
    var _module = _modules[__g];
    ++__g;
    show_debug_message((string(++_mNumber) + ". " + _module[0/* name */]));
}
var _totalTestCount = 0;
var _totalSuccesses = 0;
var _totalErrors = 0;
var _moduleResults = "";
for (var __g1 = 0; __g1 < array_length_1d(_modules); _totalErrors += g_gmtest_gmtest_errors) {
    var _module1 = _modules[__g1];
    ++__g1;
    g_gmtest_gmtest_test_count = 0;
    g_gmtest_gmtest_successes = 0;
    g_gmtest_gmtest_errors = 0;
    g_gmtest_gmtest_current_module = _module1;
    script_execute(_module1[1/* run_tests */], _module1);
    if (g_gmtest_gmtest_errors > 0) _moduleResults += 'Module "' + _module1[0/* name */] + ('" result: NOT PASSED' + chr(10)); else _moduleResults += 'Module "' + _module1[0/* name */] + ('" result: PASSED' + chr(10));
    _totalTestCount += g_gmtest_gmtest_test_count;
    _totalSuccesses += g_gmtest_gmtest_successes;
}
show_debug_message(_moduleResults);
show_debug_message("Assertations: " + string(_totalTestCount));
show_debug_message("Successes: " + string(_totalSuccesses));
show_debug_message("Errors: " + string(_totalErrors));
var _results;
if (_totalErrors > 0) {
    if (_totalErrors == _totalTestCount) _results = "BIG FAILURE."; else _results = "SOME TESTS FAILURES.";
} else _results = "ALL TESTS HAVE PASSED.";
show_debug_message("Results: " + _results);

#define gmtest_gmtest_throw_test_error
// gmtest_gmtest_throw_test_error(context:string)
show_debug_message(("ERROR at test number " + string(g_gmtest_gmtest_test_count) + ' from module "' + g_gmtest_gmtest_current_module[0/* name */] + '": ' + argument[0]));
++g_gmtest_gmtest_errors;

//}

//{ ui.haxe.boot

#define ui_haxe_boot_get_class
// ui_haxe_boot_get_class(o:T)->Class<T>
var _o = argument[0];
if (array_height_2d(_o) > 1) {
    var _r = _o[1,0];
    if (array_height_2d(_r) > 1) return _r;
}
return undefined;

#define ui_haxe_boot_wget
// ui_haxe_boot_wget(arr:array<T>, index:int)->T
var _arr = argument[0], _index = argument[1];
return _arr[_index];

#define ui_haxe_boot_wset
// ui_haxe_boot_wset(arr:array<T>, index:int, value:T)
var _arr = argument[0], _index = argument[1];
_arr[@_index] = argument[2];

//}

//{ library.book

#define library_book_create
// library_book_create(name:string)
var this; this[1,0/* metatype */] = mt_library_book;
array_copy(this, 0, mq_library_book, 0, 3);
this[@0/* name */] = argument[0];
this[@1/* pages */] = array_create(0);
this[@2/* current_page_pos */] = 0;
return this;

#define library_book_add_page
// library_book_add_page(this:book, title:string, text:string, resourceName:string)
var this = argument[0];
ui_haxe_boot_wset(this[1/* pages */], array_length_1d(this[1/* pages */]), library_page_create(argument[1], argument[2], argument[3]));

#define library_book_add_full_page
// library_book_add_full_page(this:book, page:Page)
var this = argument[0];
ui_haxe_boot_wset(this[1/* pages */], array_length_1d(this[1/* pages */]), argument[1]);

#define library_book_get_page_count
// library_book_get_page_count(this:book)->int
var this = argument[0];
return array_length_1d(this[1/* pages */]);

#define library_book_get_page
// library_book_get_page(this:book, index:int)->Page
var this = argument[0], _index = argument[1];
if (_index >= array_length_1d(this[1/* pages */]) || _index < 0) {
    trace("src/library/Book.hx:40:", "Warning: trying to get a page that does not exist. Getting last page instead.");
    return ui_haxe_boot_wget(this[1/* pages */], array_length_1d(this[1/* pages */]) - 1);
}
return ui_haxe_boot_wget(this[1/* pages */], _index);

#define library_book_get_current_page
// library_book_get_current_page(this:book)->Page
var this = argument[0];
return ui_haxe_boot_wget(this[1/* pages */], this[2/* current_page_pos */]);

#define library_book_set_current_page
// library_book_set_current_page(this:book, value:int)->Page
var this = argument[0], _value = argument[1];
this[@2/* current_page_pos */] = _value;
return ui_haxe_boot_wget(this[1/* pages */], _value);

#define library_book_go_left
// library_book_go_left(this:book)
var this = argument[0];
if (this[2/* current_page_pos */] > 0) this[@2/* current_page_pos */]--;

#define library_book_go_right
// library_book_go_right(this:book)
var this = argument[0];
if (this[2/* current_page_pos */] < array_length_1d(this[1/* pages */]) - 1) this[@2/* current_page_pos */]++;

#define library_book_get_name
// library_book_get_name(this:book)->string
var this = argument[0];
return this[0/* name */];

#define library_book_set_name
// library_book_set_name(this:book, value:string)->string
var this = argument[0];
return this[@0/* name */] = argument[1];

//}

//{ library.manager

#define library_manager_choose_book_to_view
// library_manager_choose_book_to_view(bookId:int)->Book
var _bookId = argument[0];
switch (_bookId) {
    case 0: g_library_manager_current_book = g_library_manager_lands; break;
    case 1: g_library_manager_current_book = g_library_manager_creatures; break;
    case 2: g_library_manager_current_book = g_library_manager_palaces; break;
    default: show_debug_message("Warning: unknown book selected.");
}
g_library_manager_current_book_number = _bookId;
if (g_library_manager_current_page_number >= array_length_1d(g_library_manager_current_book[1/* pages */])) g_library_manager_current_page_number = array_length_1d(g_library_manager_current_book[1/* pages */]) - 1;
return g_library_manager_current_book;

#define library_manager_get_current_page
// library_manager_get_current_page()->Page
return ui_haxe_boot_wget(g_library_manager_current_book[1/* pages */], g_library_manager_current_page_number);

#define library_manager_get_current_book
// library_manager_get_current_book()->Book
return g_library_manager_current_book;

#define library_manager_get_current_book_number
// library_manager_get_current_book_number()->int
return g_library_manager_current_book_number;

#define library_manager_set_current_book_number
// library_manager_set_current_book_number(value:int)->int
return g_library_manager_current_book_number = argument[0];

#define library_manager_get_all_books
// library_manager_get_all_books()->array<Book>
return [g_library_manager_lands, g_library_manager_creatures, g_library_manager_palaces];

//}

//{ library.page

#define library_page_create
// library_page_create(title:string, text:string, resourceName:string)
var this; this[1,0/* metatype */] = mt_library_page;
array_copy(this, 0, mq_library_page, 0, 5);
var _title = argument[0];
this[@0/* title */] = _title;
this[@1/* text */] = argument[1];
var _findAsset = asset_get_index(argument[2]);
if (_findAsset == -1) {
    trace("src/library/Page.hx:56:", 'Warning: asset for page with title "' + _title + '" not found. Using placeholder instead.');
    _findAsset = asset_get_index("spr_1up_mushroom");
}
this[@2/* asset */] = _findAsset;
this[@3/* unlocked */] = false;
this[@4/* seen */] = false;
return this;

#define library_page_get_title
// library_page_get_title(this:page)->string
var this = argument[0];
return this[0/* title */];

#define library_page_get_unlocked
// library_page_get_unlocked(this:page)->bool
var this = argument[0];
return this[3/* unlocked */];

#define library_page_get_seen
// library_page_get_seen(this:page)->bool
var this = argument[0];
return this[4/* seen */];

#define library_page_set_seen
// library_page_set_seen(this:page, status:bool)->bool
var this = argument[0];
return this[@4/* seen */] = argument[1];

#define library_page_set_unlocked
// library_page_set_unlocked(this:page, status:bool)->bool
var this = argument[0];
return this[@3/* unlocked */] = argument[1];

#define library_page_get_text
// library_page_get_text(this:page)->string
var this = argument[0];
return this[1/* text */];

#define library_page_get_asset
// library_page_get_asset(this:page)->asset
var this = argument[0];
return this[2/* asset */];

//}

//{ overworld.shop.shop_controller

#define overworld_shop_shop_controller_create
// overworld_shop_shop_controller_create(items:array<string>)
var this; this[1,0/* metatype */] = mt_overworld_shop_shop_controller;
array_copy(this, 0, mq_overworld_shop_shop_controller, 0, 1);
this[@0/* items */] = argument[0];
return this;

//}

//{ tmodules.tmgame_init

#define tmodules_tmgame_init_create
// tmodules_tmgame_init_create()
var this = array_create(2);
this[@1/* run_tests */] = f_tmodules_tmgame_init_run_tests;
this[@0/* name */] = "Init";
return this;

#define tmodules_tmgame_init_run_tests
// tmodules_tmgame_init_run_tests(this:tmgame_init)
gmtest_assert_is_true((global.current_course == game_get_course('Brand New Grassland')), "First course should be Brand New Grassland!");

//}

//{ tmodules.tmnew_game

#define tmodules_tmnew_game_create
// tmodules_tmnew_game_create()
var this = array_create(2);
this[@1/* run_tests */] = f_tmodules_tmnew_game_run_tests;
this[@0/* name */] = "New Game";
return this;

#define tmodules_tmnew_game_run_tests
// tmodules_tmnew_game_run_tests(this:tmnew_game)
gmtest_assert_equals(global.diamonds, 0);
gmtest_assert_equals(global.level, 1);
gmtest_assert_equals(global.exp_count, 0);
gmtest_assert_equals(global.live_count, 5);
gmtest_assert_is_false(global.checkpoint);
gmtest_assert_is_false(global.game_over);
gmtest_assert_is_false(global.night);
gmtest_assert_is_false(global.__cutscene);
gmtest_assert_is_false(global.switch_enabled[0]);
gmtest_assert_is_false(global.switch_enabled[1]);
gmtest_assert_is_false(global.switch_enabled[2]);
gmtest_assert_is_false(global.switch_enabled[3]);

//}

//{ tmodules.tmrelease

#define tmodules_tmrelease_create
// tmodules_tmrelease_create()
var this = array_create(2);
this[@1/* run_tests */] = f_tmodules_tmrelease_run_tests;
this[@0/* name */] = "Release Mode";
return this;

#define tmodules_tmrelease_run_tests
// tmodules_tmrelease_run_tests(this:tmrelease)
gmtest_assert_equals(live_enabled, false);
gmtest_assert_equals(DEBUG_BUILD, false);

//}

//{ tmodules.tmsave_system

#define tmodules_tmsave_system_create
// tmodules_tmsave_system_create()
var this = array_create(2);
this[@1/* run_tests */] = f_tmodules_tmsave_system_run_tests;
this[@0/* name */] = "Save System";
return this;

#define tmodules_tmsave_system_run_tests
// tmodules_tmsave_system_run_tests(this:tmsave_system)
if (file_exists(SAVE_FILE)) file_rename(SAVE_FILE, "_BACKUP");
gmtest_assert_equals(save_system_bool_array_to_string([0,0,0,0,0,0]), "000000");
gmtest_assert_equals(save_system_bool_array_to_string([1,1,1,1,1,1]), "111111");
gmtest_assert_equals(save_system_bool_array_to_string([0,1,0,1,0,1]), "010101");
gmtest_assert_equals(save_system_bool_array_to_string([1,0,1,0,1,0]), "101010");
gmtest_assert_equals(save_system_string_to_bool_array('000000'), [0, 0, 0, 0, 0, 0]);
gmtest_assert_equals(save_system_string_to_bool_array('111111'), [1, 1, 1, 1, 1, 1]);
gmtest_assert_equals(save_system_string_to_bool_array('010101'), [0, 1, 0, 1, 0, 1]);
gmtest_assert_equals(save_system_string_to_bool_array('101010'), [1, 0, 1, 0, 1, 0]);
gmtest_assert_is_false(file_exists(SAVE_FILE));
save_system_load_data();
show_debug_message("Testing the save system's initial state.");
gmtest_assert_equals(global.unlocked_courses, [0, 0, 0, 0, 0, 0]);
gmtest_assert_equals(global.page_read[Book.LAND], [0, 0, 0, 0, 0, 0, 0]);
gmtest_assert_equals(global.page_read[Book.CREATURES], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
gmtest_assert_equals(global.page_read[Book.PALACES], [0, 0, 0, 0, 0, 0]);
gmtest_assert_equals(global.page_unlocked[Book.LAND], [0, 0, 0, 0, 0, 0, 0]);
gmtest_assert_equals(global.page_unlocked[Book.CREATURES], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
gmtest_assert_equals(global.page_unlocked[Book.PALACES], [0, 0, 0, 0, 0, 0]);
show_debug_message("Testing the save system's save and load functions.");
save_system_unlock_page(Book.LAND,0);
gmtest_assert_equals(global.page_unlocked[Book.LAND], [1, 0, 0, 0, 0, 0, 0]);
save_system_read_page(Book.LAND,0);
gmtest_assert_equals(global.page_read[Book.LAND], [1, 0, 0, 0, 0, 0, 0]);
save_system_load_data();
gmtest_assert_equals(global.page_unlocked[Book.LAND], [1, 0, 0, 0, 0, 0, 0]);
gmtest_assert_equals(global.page_read[Book.LAND], [1, 0, 0, 0, 0, 0, 0]);
file_delete(SAVE_FILE);
if (file_exists("_BACKUP")) file_rename("_BACKUP", SAVE_FILE);
save_system_load_data();

//}

//{ ui.menu

#define ui_menu_create
// ui_menu_create(options:array<Option>, signal:function[string; any]:void)
var this = array_create(9);
this[@6/* backable */] = false;
this[@5/* question */] = "";
this[@4/* answer */] = false;
this[@3/* showing_question */] = false;
this[@1/* my_options */] = argument[0];
this[@2/* option_count */] = array_length_1d(this[1/* my_options */]);
this[@7/* signal */] = argument[1];
return this;

#define ui_menu_is_showing_question
// ui_menu_is_showing_question(this:menu)->bool
var this = argument[0];
return this[3/* showing_question */];

#define ui_menu_show_question
// ui_menu_show_question(this:menu, question:string)
var this = argument[0];
this[@5/* question */] = argument[1];
this[@3/* showing_question */] = true;

#define ui_menu_get_question
// ui_menu_get_question(this:menu)->string
var this = argument[0];
return this[5/* question */];

#define ui_menu_get_option
// ui_menu_get_option(this:menu, index:int)->Option
var this = argument[0], _index = argument[1];
return ui_haxe_boot_wget(this[1/* my_options */], _index);

#define ui_menu_get_current_position
// ui_menu_get_current_position(this:menu)->int
var this = argument[0];
return this[0/* current_position */];

#define ui_menu_get_current_option
// ui_menu_get_current_option(this:menu)->Option
var this = argument[0];
return ui_haxe_boot_wget(this[1/* my_options */], this[0/* current_position */]);

#define ui_menu_get_option_count
// ui_menu_get_option_count(this:menu)->int
var this = argument[0];
return this[2/* option_count */];

#define ui_menu_move_up
// ui_menu_move_up(this:menu)
var this = argument[0];
if (this[0/* current_position */] > 0) this[@0/* current_position */]--; else this[@0/* current_position */] = array_length_1d(this[1/* my_options */]) - 1;
audio_play_sound(snd_menu_move, 0, false);

#define ui_menu_move_down
// ui_menu_move_down(this:menu)
var this = argument[0];
if (this[0/* current_position */] < array_length_1d(this[1/* my_options */]) - 1) this[@0/* current_position */]++; else this[@0/* current_position */] = 0;
audio_play_sound(snd_menu_move, 0, false);

#define ui_menu_move_right
// ui_menu_move_right(this:menu)
var this = argument[0];
var _option = ui_haxe_boot_wget(this[1/* my_options */], this[0/* current_position */]);
var _type = ui_haxe_boot_get_class(_option);
if (this[3/* showing_question */]) {
    this[@4/* answer */] = !this[4/* answer */];
} else {
    switch (_type[0/* index */]) {
        case 20: ui_option_toggle(_option); break;
        case 21: ui_option_increase(_option); break;
        case 24: ui_option_increase(_option); break;
        case 25: return 0;
    }
    if (_type != mt_ui_multiple_values || !_option[4/* selectable */]) script_execute(this[7/* signal */], ui_option_get_name(_option), _option[1/* value */]);
}

#define ui_menu_move_left
// ui_menu_move_left(this:menu)
var this = argument[0];
var _option = ui_haxe_boot_wget(this[1/* my_options */], this[0/* current_position */]);
var _type = ui_haxe_boot_get_class(_option);
if (this[3/* showing_question */]) {
    this[@4/* answer */] = !this[4/* answer */];
} else {
    switch (_type[0/* index */]) {
        case 20: ui_option_toggle(_option); break;
        case 21: ui_option_decrease(_option); break;
        case 24: ui_option_decrease(_option); break;
        case 25: return 0;
    }
    if (_type != mt_ui_multiple_values || !_option[4/* selectable */]) script_execute(this[7/* signal */], ui_option_get_name(_option), _option[1/* value */]);
}

#define ui_menu_select
// ui_menu_select(this:menu)
var this = argument[0];
var _option = ui_haxe_boot_wget(this[1/* my_options */], this[0/* current_position */]);
if (this[3/* showing_question */]) {
    if (this[4/* answer */]) {
        script_execute(_option[5/* action */]);
    } else {
        this[@3/* showing_question */] = false;
        this[@5/* question */] = "";
    }
} else switch (ui_haxe_boot_wget(ui_haxe_boot_get_class(_option), 0)) {
    case 25: case 19: return 0;
    case 22:
        script_execute(_option[5/* action */]);
        trace("src/ui/Menu.hx:106:", "Selected a normal action. The current value of currentPosition is " + ui_Std_string(this[0/* current_position */]) + ".");
        audio_play_sound(snd_menu_select, 0, false);
        break;
    case 23:
        this[@3/* showing_question */] = true;
        trace("src/ui/Menu.hx:110:", "Action with confirmation coming.");
        this[@5/* question */] = _option[3/* question */];
        audio_play_sound(snd_menu_select, 0, false);
        break;
    case 24:
        if (_option[4/* selectable */]) {
            trace("src/ui/Menu.hx:115:", "Selecting " + string(_option[1/* value */]) + " from a multiple value option.");
            script_execute(this[7/* signal */], ui_option_get_name(_option), _option[1/* value */]);
        }
        break;
}

//}

//{ ui.option

#define ui_option_new
// ui_option_new(this:option, name:string)
var this = argument[0];
this[@0/* name */] = argument[1];

#define ui_option_create
// ui_option_create(name:string)
var this; this[1,0/* metatype */] = mt_ui_option;
array_copy(this, 0, mq_ui_option, 0, 6);
ui_option_new(this, argument[0]);
return this;

#define ui_option_toggle
// ui_option_toggle(this:option)
var this = argument[0];
this[@1/* value */] = (this[1/* value */] == 1 ? 0 : 1);

#define ui_option_decrease
// ui_option_decrease(this:option)
var this = argument[0];
if (this[1/* value */] > 0) this[@1/* value */]--;

#define ui_option_increase
// ui_option_increase(this:option)
var this = argument[0];
if (this[1/* value */] < this[2/* max_value */]) this[@1/* value */]++;

#define ui_option_get_name
// ui_option_get_name(this:option)->string
var this = argument[0];
return this[0/* name */];

//}

//{ ui.toggle

#define ui_toggle_create
// ui_toggle_create(name:string, value:int)
var this; this[1,0/* metatype */] = mt_ui_toggle;
array_copy(this, 0, mq_ui_toggle, 0, 6);
ui_option_new(this, argument[0]);
this[@1/* value */] = argument[1];
return this;

//}

//{ ui.slider

#define ui_slider_create
// ui_slider_create(name:string, value:int)
var this; this[1,0/* metatype */] = mt_ui_slider;
array_copy(this, 0, mq_ui_slider, 0, 6);
ui_option_new(this, argument[0]);
this[@1/* value */] = argument[1];
this[@2/* max_value */] = 100;
return this;

//}

//{ ui.action

#define ui_action_new
// ui_action_new(this:action, name:string, action:function[]:void)
var this = argument[0], _name = argument[1];
ui_option_new(this, _name);
this[@0/* name */] = _name;
this[@5/* action */] = argument[2];

#define ui_action_create
// ui_action_create(name:string, action:function[]:void)
var this; this[1,0/* metatype */] = mt_ui_action;
array_copy(this, 0, mq_ui_action, 0, 6);
ui_action_new(this, argument[0], argument[1]);
return this;

//}

//{ ui.action_with_confirmation

#define ui_action_with_confirmation_create
// ui_action_with_confirmation_create(name:string, action:function[]:void, question:string)
var this; this[1,0/* metatype */] = mt_ui_action_with_confirmation;
array_copy(this, 0, mq_ui_action_with_confirmation, 0, 6);
ui_action_new(this, argument[0], argument[1]);
this[@3/* question */] = argument[2];
return this;

//}

//{ ui.multiple_values

#define ui_multiple_values_create
// ui_multiple_values_create(name:string, valuesList:array<string>, value:int, selectable:null<bool> = false)
var this; this[1,0/* metatype */] = mt_ui_multiple_values;
array_copy(this, 0, mq_ui_multiple_values, 0, 7);
var _valuesList = argument[1], _selectable;
if (argument_count > 3) _selectable = argument[3]; else _selectable = false;
ui_option_new(this, argument[0]);
this[@6/* values_list */] = _valuesList;
this[@1/* value */] = argument[2];
this[@4/* selectable */] = _selectable;
this[@2/* max_value */] = array_length_1d(_valuesList) - 1;
return this;

//}

//{ ui.empty_option

#define ui_empty_option_create
// ui_empty_option_create(name:string)
var this; this[1,0/* metatype */] = mt_ui_empty_option;
array_copy(this, 0, mq_ui_empty_option, 0, 6);
ui_option_new(this, argument[0]);
return this;

//}
