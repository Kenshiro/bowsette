package bowsette;
import gml.assets.Asset;
@:keep
@:native("song")
@:expose
@:doc
/**
 * This class can be used to add songs to the game song list.
 */
class Song {
    var name : String;
    var perfectLoop : Bool;
    var loopStart : Float;
    var loopEnd : Float;
    var asset : Asset;
	
	public function get_name():String 
	{
		return name;
	}
	
	public function get_perfectLoop():Bool 
	{
		return perfectLoop;
	}
	
	public function get_loopStart():Float 
	{
		return loopStart;
	}
	
	public function get_loopEnd():Float 
	{
		return loopEnd;
	}
	
	public function get_asset():Asset 
	{
		return asset;
	}
	
    public function new (name:String, songName:String, ?perfectLoop:Bool = true, ?loopStart:Float, ?loopEnd:Float) {
        this.name = name;
        this.asset = gml.assets.Asset.find(songName);
        this.perfectLoop = perfectLoop;
        if (!perfectLoop) {
            this.loopStart = loopStart;
            this.loopEnd = loopEnd;
        }
    }
}