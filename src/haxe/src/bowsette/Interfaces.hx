package bowsette;
import library.Manager;
import ui.Option;
import ui.Menu;
import gml.Assets;
import gml.RawAPI;
import gml.Lib;
import gml.Scripts;

@:keep
@:nativeGen class Interfaces {    
    public static function signal(setting:String, value:Any):Void {
        switch (setting) {
            case "Fullscreen Mode":
                trace('Switching fullscreen value to $value');
                RawAPI.window_set_fullscreen(value);
                Scripts.save_system_save_config();
                gml.Lib.global.fullscreen_mode = value;
            case "Scale":
                var newRes = Std.string(Std.int(value) + 1) + "x";
                trace('Changing scale to $newRes.');
                Scripts.update_resolution(value);
                Scripts.save_system_save_config();
                Lib.global.scale = value;
			case "Book":
				Manager.chooseBookToView(value);
				Lib.raw("reading_a_page = true");
				Lib.raw("state = MenuState.LIBRARY");
                Lib.raw("current_menu = page_menu");
				Lib.raw("pause_menu_update_page()");
				trace("Opening a book.");
        }
    }
    public static function makePauseMenu():Menu {
        var resume:Void->Void = function() { 
            gml.RawAPI.audio_play_sound(gml.Assets.snd_unpause, 0, false);
            Lib.raw("unpaused = true");
            Lib.raw("alarm[0] = 20");
        }


        var optionsMenu:Void->Void = function() {
            Lib.raw("current_menu = options_menu");
            trace("Going to options menu.");
        }

        var quit:Void->Void = function () {
            Scripts.fade_to_room(gml.Assets.rm_menu);
            RawAPI.audio_sound_gain(gml.Lib.global.song, 0, 1000);
            trace("Quitting from the pause menu.");
        }
        var option1:Option = new Action("Resume", resume);
        var option2:Option = new Action("Library", libraryMenuAction);
        var option3:Option = new Action("Options", optionsMenu);
        var option4:Option = new ActionWithConfirmation("Quit", quit, "Are you sure you want to quit?");
        var options:Array<Option> = [option1, option2, option3, option4];      
        var menu =  new Menu(options, signal);
        menu.backable = true;
        menu.backSingal = resume;
        return menu;
    }
	
	public static function makeBookMenu():Menu {
		var option1:Option = new MultipleValues("Book", ["Land", "Species", "Palace"], Manager.currentBookNumber, true);
		var option2:Option = new Action("Back", pauseMenuAction);
        var options:Array<Option> = [option1, option2];
        //var option1 = new Toggle
        var menu =  new Menu(options, signal);
        menu.backable = true;
        menu.backSingal = pauseMenuAction;
        return menu;
	}

    /**
     * This menu is an irregular one - the page itself is not handled by it, but rather by obj_pause.
     * @return Menu
     */
    public static function makePageMenu():Menu {
        var option1:Option = new EmptyOption("Page");
        var option2:Option = new Action("Back", libraryMenuAction);
        var menu =  new Menu([option1], signal);
        menu.backable = true;
        menu.backSingal = libraryMenuAction;
        return menu;
    }

    public static function makeSettingsMenu(scale: Int, isFullscreen: haxe.extern.EitherType<Int, Bool>, musicVolume: Int):Menu {
        var option1:Option = new MultipleValues("Scale", ["1x", "2x", "4x"], scale);
        var option2:Option = new Toggle("Fullscreen Mode", isFullscreen);
        var option3:Option = new Slider("Music Volume", musicVolume);
        var option4:Option = new Action("Back", pauseMenuAction);
        var options:Array<Option> = [option1, option2, option3, option4];
        //var option1 = new Toggle
        var menu =  new Menu(options, signal);
        menu.backable = true;
        menu.backSingal = pauseMenuAction;
        return menu;
    }
	
	public static function pauseMenuAction():Void {
		Lib.raw("current_menu = pause_menu");
        Lib.raw("reading_a_page = false");
        Lib.raw("state = MenuState.PAUSE");
		trace("Going back to pause menu.");
        Lib.raw("obj_hud_controller.hide_hud = false;");
	}

    public static function libraryMenuAction():Void {
        Lib.raw("reading_a_page = false");
        Lib.raw("state = MenuState.PAUSE");
        Lib.raw("current_menu = library_menu");
        Lib.raw("obj_hud_controller.hide_hud = true;");
        RawAPI.audio_play_sound(gml.Assets.snd_menu_select, 0, false);
    }
}