package bowsette;
import gml.RawAPI;
import gml.Lib;
import library.Page;
import bowsette.Song;
import library.Manager;
import collectibles.Catalog;
import collectibles.Item;
import gml.assets.Asset;
@:keep
@:native("game")
@:expose
/**
 * This class holds all game courses and songs. You can get courses
 * using the getCourse() and getCourseList() functions. Songs
 */
@:doc class Game
{
	//** This array contains all game courses from first to last.**/
	@:doc static var courses = new Array<Course>();
	//** This array contains all BONUS courses from first to last.**/
	static var bonusCourses = new  Array<Course>();
	//** This map allows you to find a course by its name.**/
	static var courseMap  = new Map<String, Course>();
	static var songMap = new Map<String, Song>();
	static var songNameList  = new Array<String>();
	static var globalItemCatalog = new collectibles.Catalog();
	public static function init()
	{
		initializeCourseData();
		initializeBookData();
		loadItemData();
	}

	/**
	 * Get a course from its name
	 * @param name the course's name
	 * @return Course
	 */
	@:doc public static function getCourse(name:String):Course
	{
		return courseMap[name];
	}

	/**
	 * Get a song using its name
	 * @param name
	 * @return Song
	 */
	@:doc public static function getSong(name:String):Song
	{
		if (songMap[name] != null) {
			return songMap[name];
		} else {
			trace('Warning: unknown song "$name". Returning the first song added instead.');
			return songMap[songNameList[0]];
		}
	}
	
	public static inline function addSong(song:Song):Void {
		var songName:String = song.get_name();
		songMap[songName] = song;
        songNameList[songNameList.length] = songName;
	}

	/**
	 * Imports course data from data/courses.json at compile time
	 */
	public static inline function initializeCourseData():Void {
		var courseData:Array<CourseData> = misc.CompileTimeLoad.json("data/rooms/courses.json");
		var course:Course;
		var j = 0;
		for (i in 0...7)
		{
			course = new Course(courseData[i].name, courseData[i].flavor_texts, courseData[i].room_name, courseData[i].song_name, i);
			courses[courses.length] = course;
			courseMap[courseData[i].name] = course;
		}
		var courseData:Array<CourseData> = misc.CompileTimeLoad.json("data/rooms/bonus_courses.json");
		for (i in 0...3)
		{
			//Library.lands.addPage(courseData[i].name, courseData[i].description, courseData[i].picture_small);
			course = new Course(courseData[i].name, courseData[i].flavor_texts, courseData[i].room_name, courseData[i].song_name, i);
			bonusCourses[bonusCourses.length] = course;
			courseMap[courseData[i].name] = course;
		}
		var songData = misc.CompileTimeLoad.json("data/songs.json");
		var song:Song;
		for (i in 0...9)
		{
			if (songData[i].perfect_loop)
			{
				song = new Song(songData[i].name, songData[i].song_asset_name);
				addSong(song);
			}
		}
	}
	
	public static inline function initializeBookData():Void {
		var bookData:Array<PageData> = misc.CompileTimeLoad.json("data/lore/lands.json");
		var pageSaveData;
		var title:String;
		for (i in 0...7) {
			title = bookData[i].title;
			Manager.lands.addPage(bookData[i].title, bookData[i].text, bookData[i].asset);
		}
		var bookData2:Array<PageData> = misc.CompileTimeLoad.json("data/lore/creatures.json");
		for (i in 0...10) {
			title = bookData2[i].title;
			Manager.creatures.addPage(title, bookData2[i].text, bookData2[i].asset);
		}

		var bookData3:Array<PageData> = misc.CompileTimeLoad.json("data/lore/palaces.json");
		for (i in 0...6) {
			title = bookData3[i].title;
			Manager.palaces.addPage(title, bookData3[i].text, bookData3[i].asset);
		}
	}

	public static inline function loadItemData():Void {
		var items:Array<ItemInfoData> = misc.CompileTimeLoad.json("data/items.json");
		for (i in 0 ... 4) {
			globalItemCatalog.addItem(new Item(items[i].name, items[i].desc, items[i].price, items[i].sprAsset));
		}
	}

	public static inline function loadPageSaveData(page:Page):Void {
		var title = page.get_title();
	}

	public static function get_courseList():Array<Course>
	{
		return courses;
	}

	static function getCourseByPosition(pos:Int):Course
	{
		return courses[pos];
	}

	static function get_bonusCourseList():Array<Course>
	{
		return bonusCourses;
	}

	static function getCoursePosition(course:Course):Int
	{
		return courses.indexOf(course);
	}

	/**
	 * Run the test suit.
	 */
	public static function runAllTests():Void {
        gmtest.GMTest.run([new tmodules.TMGameInit(), new tmodules.TMNewGame(), new tmodules.TMRelease(), new tmodules.TMSaveSystem()]);
    }

	public static function runNewGameTests():Void {
        gmtest.GMTest.run([new tmodules.TMNewGame()]);
    }
}

//useful for compile-time data imports
typedef CourseData =
{
	name : String,
	picture : Int,
	flavor_texts : Array<String>,
	room_name : String,
	song_name : String,
	number : Int
};

typedef PageData = { title: String, text: String, asset: String };
typedef ItemInfoData = { name: String, desc: String, price: Int, sprAsset: String, ?audioAsset: String };