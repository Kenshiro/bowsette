package bowsette;
import gml.assets.Asset;
@:keep
@:native("course")
@:expose
@:doc
class Course implements overworld.GameRoom {
    private static var sideloadedCourseRooms = new Map<String, Asset>();
    public final name : String;
    var flavorTexts : Array<String>;
    public final room : Asset;
    public var number: Int;
    public final sideloaded: Bool;
    var songName : String;
    /**
     * Creates a new course, which has a name, a room and a song.
     * @param name **this** course's name
     * @param roomName The GameMaker's room for this course
     * @param songName The song name (string)
     */
    public function new (name:String, flavorTexts:Array<String>, roomName:String, songName:String, number:Int, ?sideloaded:Bool = false, ?sideloadedRoom:Asset) {
        this.name = name;
        this.flavorTexts = flavorTexts;
        this.songName = songName;
        this.number = number;
        this.sideloaded = sideloaded;
        if (sideloaded) {
            this.room = sideloadedRoom;
            sideloadedCourseRooms[name] = sideloadedRoom;
        } else {
            this.room = Asset.find(roomName);
        }
    }

    /**
     * Adds a flavor text to **this** course
     * @param text flavor phrase to add
     */
    public function addFlavorText(text:String):Void {
        flavorTexts[flavorTexts.length] = text;
    }

    /**
     * Gets a random flavor text for **this** course.
     * @return String
     */
    public function getRandomFlavor():String {
        if (flavorTexts.length > 0) {
            var i = Std.random(flavorTexts.length);
            return flavorTexts[i];
        } else return ""; //no flavor text
    }

    /**
    * Gets the name for **this** course.
    * @return String
    */
	public function get_name():String {
	    return name;
	}
	 
    /**
    * Gets the song for **this** course.
    * @return Song
    */
    public function getCourseSong():Song {
        return Game.getSong(songName);
    }
	
	public function getSongName():String {
		return songName;
	}
	
	public function getRoom():Asset {
		return room;
	}
	
	public function nameEquals(name:String):Bool {
		name = StringTools.trim(name).toUpperCase();
		var myName = StringTools.trim(this.name).toUpperCase();
		return (name == myName);
	}
}