package collectibles;
import gml.ds.ArrayList;

@:keep
@:object("obj_inventory_controller")
@:doc
@:native("inventory")
class Inventory extends gml.Instance {
    private var items:gml.ds.ArrayList<String>;
    private var itemsQuantity:gml.ds.ArrayList<Int>;
    private var sizeLimit:Int;

    public function new(size:Int) {
        items = new gml.ds.ArrayList<String>();
        itemsQuantity = new gml.ds.ArrayList<Int>();
        this.sizeLimit = (size > 0) ? size : 9999;
        this.persistent = true;
    }

    public function add(id:String, quantity:Int):Bool {
        var index = items.indexOf(id);
        if (index != -1) {
            trace('Adding $quantity units of $id to the inventory.');
            itemsQuantity[index] += quantity;
            return true;
        }
        if (items.length < sizeLimit) {
            trace('Adding $id to the inventory. Quantity: $quantity. You did not have this item before.');
            items.add(id);
            itemsQuantity.add(quantity);
            return true;
        }
        trace('Inventory is full.');
        return false;
    }

    public function remove(id:String, quantity:Int):Bool {
        var index = items.indexOf(id);
        if (index != -1) {
            trace('Removing $id units of $id to the inventory.');
            if (itemsQuantity[index] > quantity) {
                itemsQuantity[index] -= quantity;
            } else {
                if (itemsQuantity[index] < quantity) {
                    trace('Warning: the amount removed is higher than the inventory had.');
                }
                items.delete(index);
                itemsQuantity.delete(index);
            }
            return true;
        }

        return false;
    }

    public function hasItem(id:String):Bool {
        //return items.indexOf()
        return (items.indexOf(id) != -1);
    }

    public function hasQuantity(id:String, quantity:Int):Bool {
        var index = items.indexOf(id);
        return (index != -1 && itemsQuantity[index] >= quantity);
    }

    public function getQuantity(id:String):Int {
        var index = items.indexOf(id);
        return (index == -1) ? 0 : itemsQuantity[index];
        
    }

    public function getLength():Int {
        return items.length;
    }

    public function getItemList():ArrayList<String> {
        return items;
    }

    public function getQuantityList():ArrayList<Int> {
        return itemsQuantity;
    }
}