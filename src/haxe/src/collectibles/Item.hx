package collectibles;

@:keep
@:native("item")
@:expose
@:doc
class Item {
    public var name: String;
    public var desc: String;
    public var price: Int;
    public var sprAsset: gml.assets.Asset;
    public var audioAsset: String;

    public function new(name, desc, price, spriteName, ?audioAsset) {
        this.name = name;
        this.desc = desc;
        this.price = price;
        this.sprAsset = gml.assets.Asset.find(spriteName);
        if (audioAsset != null) this.audioAsset = audioAsset;
    }
}