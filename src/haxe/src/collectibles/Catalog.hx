package collectibles;

@:keep
class Catalog {
    private var items:Map<String,Item>;

    public function new() {
        items = new Map<String,Item>();
    }

    public function addItem(item:Item):Void {
        if (items[item.name] == null) {
            items[item.name] = item;
        } else {
            trace('Error: there is already an item with the same name.');
        }
    }

    public function findItem(name:String):Item {
        return items[name];
    }
}