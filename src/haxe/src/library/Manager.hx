package library;

@:doc
@:keep
//@:expose
//@:native("library")
class Manager {
	public static var lands:Book = new Book("Lands");
    public static var creatures:Book = new Book("Creatures");
    public static var palaces:Book = new Book("Palaces");
    public static var currentBook:Book = lands;
	public static var currentBookNumber:Int = 0;
    public static var currentPageNumber:Int = 0;
	
    public static function chooseBookToView(bookId:Int):Book {
        switch bookId {
            case 0:
                currentBook = lands;
            case 1:
                currentBook = creatures;
            case 2:
                currentBook = palaces;
            default:
                Sys.println('Warning: unknown book selected.');
        }
		currentBookNumber = bookId;
		if (currentPageNumber >= currentBook.pages.length) {
			currentPageNumber = currentBook.pages.length - 1;
		}
		return currentBook;
    }
	/*
	public static function goLeft():Void {
		if (currentPageNumber > 0)
			currentPageNumber--;
	}
	
	public static function goRight():Void {
		if (currentPageNumber < currentBook.pages.length - 1)
			currentPageNumber++;
	}*/
	
	public static function getCurrentPage():Page {
		return currentBook.pages[currentPageNumber];
	}
	
	public static function getCurrentBook():Book {
		return currentBook;
	}
	
	static function get_currentBookNumber():Int 
	{
		return currentBookNumber;
	}
	
	static function set_currentBookNumber(value:Int):Int 
	{
		return currentBookNumber = value;
	}

	public static function getAllBooks():Array<Book> {
		return [lands, creatures, palaces];
	}
}