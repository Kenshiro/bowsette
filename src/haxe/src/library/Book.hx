package library;

@:keep
@:doc
class Book {
	public var name:String;
    public var pages:Array<Page>;
	public var currentPagePos:Int;
	private var pageCount(get, null):Int;
    public function new(name:String) {
		this.name = name;
        pages = new Array<Page>();
		currentPagePos = 0;
    }
	
    public function addPage(title:String, text:String, resourceName:String) {
        var page = new Page(title, text, resourceName);
        pages[pages.length] = page;
    }
	
	public function addFullPage(page:Page) {
		pages[pages.length] = page;
	}

	/*public function hasNewPage():Bool {
		var pageSaveData:PageSaveData;
		for (page in pages) {
			pageSaveData = SaveSystem.getPageStatus(this, page);
		}
		return false;
	}*/
	
	public function get_pageCount():Int 
	{
		return pages.length;
	}
	
	public function getPage(index:Int):Page {
		if (index >= pages.length || index < 0) {
			trace('Warning: trying to get a page that does not exist. Getting last page instead.');
			return pages[pages.length - 1];
		}
		return pages[index];
	}
	
	public function get_currentPage():Page 
	{
		return pages[currentPagePos];
	}
	
	public function set_currentPage(value:Int):Page 
	{
		currentPagePos = value;
		return pages[value];
	}
	
	public function goLeft():Void {
		if (currentPagePos > 0)
			currentPagePos--;
	}
	
	public function goRight():Void {
		if (currentPagePos < pages.length - 1)
			currentPagePos++;
	}
	
	function get_name():String 
	{
		return name;
	}
	
	function set_name(value:String):String 
	{
		return name = value;
	}
}