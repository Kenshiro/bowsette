package library;
import gml.assets.Asset;
@:keep
@:doc
class Page {
    var title:String;
    var text:String;
    var asset:Asset;
    var unlocked:Bool;
	var seen:Bool;

	public function get_title():String 
	{
		return title;
	}

    /**
     * Returns whether **this** page was unlocked by the player before or not.
     * @return Bool
     */
    public function get_unlocked():Bool {
        return unlocked;
    }

    /**
     * Returns whether **this** page was seen by the player before or not.
     * @return Bool
     */
    public function get_seen():Bool {
        return seen;
    }

    public function set_seen(status: Bool):Bool {
        return seen = status;
    }    

    public function set_unlocked(status: Bool):Bool {
        return unlocked = status;
    }  

	public function get_text():String 
	{
		return text;
	}
	
	public function get_asset():Asset 
	{
		return asset;
	}
	
    public function new(title:String, text:String, resourceName:String) {
        this.title = title;
        this.text = text;
        var findAsset:Dynamic = Asset.find(resourceName);
		if (findAsset == -1) {
			trace('Warning: asset for page with title "$title" not found. Using placeholder instead.');
			findAsset = Asset.find("spr_1up_mushroom");
		}
		this.asset = findAsset;
        unlocked = false;
        seen = false;
    }
}