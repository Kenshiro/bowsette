package ui;

@:keep
class Option {
    @:doc private var name: String;
    @:doc public var value: Int;
    @:doc public var maxValue: Int;
    @:doc public var question:String;
	public var selectable:Bool;
    public var action: Void->Void;
    public function toggle():Void {
        value = (value == 1) ? 0 : 1;
    }
    public function decrease():Void {
        if (value > 0)
            value--;
    }
    public function increase():Void {
        if (value < maxValue)
            value++;
    }
    public function new(name:String) {
    this.name = name;
    }
    public function getName():String {
        return name;
    }
}

class Toggle extends Option {
    
    public function new(name:String, value:Int) {
        super(name);
        this.value = value;
    }
}

class Slider extends Option {
    public function new(name:String, value:Int) {
    super(name);
    this.value = value;
    maxValue = 100;
    }
}

class Action extends Option {
    //public var action:Void->Void;
    public function new(name:String, action) {
    super(name);
    this.name = name;
    this.action = action;
    }    
}

class ActionWithConfirmation extends Action {
    public function new(name:String, action, question:String) {
    super(name, action);
    this.question = question;
    }
}

class MultipleValues extends Option {
    @:doc public var valuesList: Array<String>;
	//@:doc public var selectable: Bool;
    public function new(name:String, valuesList:Array<String>, value: Int, ?selectable: Bool = false) { 
        super(name);
        this.valuesList = valuesList;
        this.value = value;
		this.selectable = selectable;
        maxValue = valuesList.length - 1;
    }
}

class EmptyOption extends Option {
    public function new(name:String) {
        super(name);
    }
}