package ui;
import ui.Option;
import gml.Assets;
import gml.RawAPI;
@:keep
@:nativeGen
class Menu {
    @:doc public var currentPosition: Int;
    @:doc public var myOptions: Array<Option>;
    public var optionCount: Int;
    @:doc public var showingQuestion:Bool = false;
    @:doc public var answer:Bool = false;
    @:doc public var question:String = "";
    @:doc public var backable:Bool = false;
    public var signal:String->Any->Void;
    @:doc public var backSingal:Void->Void;
    public function isShowingQuestion():Bool {
        return showingQuestion;
    }

    public function showQuestion(question:String) {
        this.question = question;
        showingQuestion = true;
    }

    public function getQuestion():String {
        return question;
    }
    public function getOption(index:Int):Option {
        return myOptions[index];
    }
    public function getCurrentPosition():Int {
        return currentPosition;
    }
	public function getCurrentOption():Option {
		return myOptions[currentPosition];
	}
    public function getOptionCount():Int {
        return optionCount;
    }
    public function moveUp(): Void {
        if (currentPosition > 0)
            currentPosition--;
        else
            currentPosition = myOptions.length - 1;
        RawAPI.audio_play_sound(gml.Assets.snd_menu_move, 0, false); 
    }
    public function moveDown(): Void {
        if (currentPosition < myOptions.length - 1)
            currentPosition++;
        else
            currentPosition = 0;
        RawAPI.audio_play_sound(gml.Assets.snd_menu_move, 0, false);
    }
    public function moveRight() : Void {
		var option = myOptions[currentPosition];
		var type = Type.getClass(option);
        if (!showingQuestion) {
            switch(type) {
                case Toggle:
                    option.toggle();
                case Slider:
                    option.increase();
                case MultipleValues:
                    option.increase();
                case EmptyOption:
                    return;
            }
			if (type != MultipleValues || !option.selectable)
				signal(option.getName(), option.value);
        }
        else {
            answer = !answer;
        }
    }
    public function moveLeft() : Void {
		var option = myOptions[currentPosition];
		var type = Type.getClass(option);
        if (!showingQuestion) {
            switch(type) {
                case Toggle:
                    option.toggle();
                case Slider:
                    option.decrease();
                case MultipleValues:
                    option.decrease();
                case EmptyOption:
                    return;
            }
			if (type != MultipleValues || !option.selectable)
				signal(option.getName(), option.value);
        }
        else {
            answer = !answer;
        }
    }
    public function select():Void {
		var option = myOptions[currentPosition];
        if (!showingQuestion) {
			var elementType = Type.getClass(option);
            switch(elementType) {
                case Option|EmptyOption:
                    return;
				case Action:
					option.action();
					trace("Selected a normal action. The current value of currentPosition is " + Std.string(currentPosition) + ".");
					RawAPI.audio_play_sound(gml.Assets.snd_menu_select, 0, false);
				case ActionWithConfirmation:
					showingQuestion = true;
					trace("Action with confirmation coming.");
					question = option.question;
					RawAPI.audio_play_sound(gml.Assets.snd_menu_select, 0, false);
				case MultipleValues:
					if (option.selectable) {
						trace('Selecting ${option.value} from a multiple value option.');
						signal(option.getName(), option.value);
					}
            }
        }
        else {
            if (answer) {
                option.action();
            }
            else {
                showingQuestion = false;
                question = "";
            }
        }
    }

    public function new(options: Array<Option>, signal:String->Any->Void) {
        myOptions = options;
        optionCount = myOptions.length;
        this.signal = signal;
    }
}