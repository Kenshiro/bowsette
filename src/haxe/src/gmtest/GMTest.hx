package gmtest;

@:keep
@:doc
@:nativeGen
class GMTest {
    public static var testCount = 0;
    public static var successes = 0;
    public static var errors = 0;
    public static var currentModule:ITestModule;
    public static function run(modules:Array<ITestModule>):Void {
        Sys.println('Running the test suite. A total of ${modules.length} test modules were passed to the suite.');
        Sys.println('Modules being tested:');
        var mNumber = 0;
        for (module in modules) {
            Sys.println('${++mNumber}. ${module.name}');
        } 
        var totalTestCount = 0;
        var totalSuccesses = 0;
        var totalErrors = 0;
        var moduleResults:String = "";
        for (module in modules) {
            testCount = 0;
            successes = 0;
            errors = 0;
            currentModule = module;
            module.runTests();
            if (errors > 0) {
                moduleResults += 'Module "${module.name}" result: NOT PASSED\n';
            } else {
                moduleResults += 'Module "${module.name}" result: PASSED\n';
            }
            totalTestCount += testCount;
            totalSuccesses += successes;
            totalErrors += errors;
        }
        Sys.println(moduleResults);
        Sys.println('Assertations: $totalTestCount');
        Sys.println('Successes: $totalSuccesses');
        Sys.println('Errors: $totalErrors');
        var results:String;
        if (totalErrors > 0) {
            if (totalErrors == totalTestCount) {
                results = "BIG FAILURE.";
            } else {
                results = "SOME TESTS FAILURES.";
            }
        } else {
            results = "ALL TESTS HAVE PASSED.";
        }
        Sys.println('Results: $results');
    }

    public static function throwTestError(context:String) {
        Sys.println('ERROR at test number $testCount from module "${currentModule.name}": $context');
        errors++;
    }
}