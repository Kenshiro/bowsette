package gmtest;

interface ITestModule {
    public final name:String;
    public dynamic function runTests():Void;
}