package gmtest;

import gml.NativeArray;
import gml.Lib;
import gml.ds.ArrayList;

@:keep
class Assert {
   // static inline var defaultErrorMessage = 'Error at test number';
    public static function isTrue(expression:Bool, ?errorMessage:String):Void {
        GMTest.testCount++;
        if (expression) {
            GMTest.successes++;
        } else {
            if (errorMessage == null) {
                errorMessage = 'Context: expression should be true.';
            }
            GMTest.throwTestError(errorMessage);
        }
    }

    public static function isFalse(expression:Bool, ?errorMessage:String):Void {
        GMTest.testCount++;
        if (!expression) {
            GMTest.successes++;
        } else {
            if (errorMessage == null) {
                errorMessage = 'Context: expression should be false.';
            }
            GMTest.throwTestError(errorMessage);
        }
    }

    public static function isBetween(value:Float, min:Float, max:Float, ?errorMessage:String):Void {
        GMTest.testCount++;
        if (min == max) {
            GMTest.throwTestError('Context: minimum and maximum values are the same. Used "equals" instead.');
            return;
        }
        if (min > max) {
            GMTest.throwTestError('Context: minimum value is higher than the maximum value..');
            return;            
        }
        if (value >= min && value <= max) {
            GMTest.successes++;
        } else {
            if (errorMessage == null) {
                GMTest.throwTestError('Context: value is NOT between $min and $max inclusive.');
            }
            GMTest.throwTestError(errorMessage);
        }
    }

    public static function equals(a:Any, b:Any, ?errorMessage:String):Void {
        GMTest.testCount++;
        /*if (Type.typeof(a) != Type.typeof(b)) {
            GMTest.errors++;
            if (errorMessage == null)
            errorMessage = 'Error at test number ${GMTest.testCount}: values are of different types.';
            trace(errorMessage);
            return;
        } else {*/
        if (a != b) {
            if (Std.is(a, Array) && Std.is(b, Array)) {
                if (NativeArray.equals(a, b)) {
                    GMTest.successes++;
                    return;
                }
            }
            
            if (errorMessage == null)
                errorMessage = 'Context: $a and $b are not equal!';
            GMTest.throwTestError(errorMessage);
            return;
        }
        GMTest.successes++;
    }
}