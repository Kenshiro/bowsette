package misc;

class CompileTimeLoad
{
	// path is relative to where haxe is executed
	macro public static inline function json(path : String)
	{
		var value = sys.io.File.getContent(path),
			json = haxe.Json.parse(value);
			trace(json.length);
		return macro $v {json};
	}

	macro public static inline function jsonArray(path : String, T:Type)
	{
		var value = sys.io.File.getContent(path),
			json = haxe.Json.parse(value);
			trace(json.length);
		return macro $v {json};
	}
}