package tmodules;

import sys.FileSystem;
import gml.Lib;
import gmtest.TestModule;
import gmtest.Assert;

@:keep
@:nativeGen
class TMSaveSystem implements gmtest.ITestModule {
    public final name = "Save System";
    public function new() {}
    public dynamic function runTests():Void {
        if (FileSystem.exists(Lib.raw("SAVE_FILE"))) {
            FileSystem.rename(Lib.raw("SAVE_FILE"), "_BACKUP");
            //Sys.println("Previous save file existed. It was deleted.");
        }
        Assert.equals(Lib.raw("save_system_bool_array_to_string([0,0,0,0,0,0])"), "000000");
        Assert.equals(Lib.raw("save_system_bool_array_to_string([1,1,1,1,1,1])"), "111111");
        Assert.equals(Lib.raw("save_system_bool_array_to_string([0,1,0,1,0,1])"), "010101");
        Assert.equals(Lib.raw("save_system_bool_array_to_string([1,0,1,0,1,0])"), "101010");
        Assert.equals(Lib.raw("save_system_string_to_bool_array('000000')"), [0,0,0,0,0,0]);
        Assert.equals(Lib.raw("save_system_string_to_bool_array('111111')"), [1,1,1,1,1,1]);
        Assert.equals(Lib.raw("save_system_string_to_bool_array('010101')"), [0,1,0,1,0,1]);
        Assert.equals(Lib.raw("save_system_string_to_bool_array('101010')"), [1,0,1,0,1,0]);
        Assert.isFalse(sys.FileSystem.exists(Lib.raw("SAVE_FILE")));
        Lib.raw("save_system_load_data()");
        Sys.println("Testing the save system's initial state.");
        Assert.equals(Lib.raw("global.unlocked_courses"), [0,0,0,0,0,0]);
        Assert.equals(Lib.raw("global.page_read[Book.LAND]"), [0,0,0,0,0,0,0]);
        Assert.equals(Lib.raw("global.page_read[Book.CREATURES]"), [0,0,0,0,0,0,0,0,0,0]);
        Assert.equals(Lib.raw("global.page_read[Book.PALACES]"), [0,0,0,0,0,0]);
        Assert.equals(Lib.raw("global.page_unlocked[Book.LAND]"), [0,0,0,0,0,0,0]);
        Assert.equals(Lib.raw("global.page_unlocked[Book.CREATURES]"), [0,0,0,0,0,0,0,0,0,0]);
        Assert.equals(Lib.raw("global.page_unlocked[Book.PALACES]"), [0,0,0,0,0,0]);
        Sys.println("Testing the save system's save and load functions.");
        Lib.raw("save_system_unlock_page(Book.LAND,0)");
        Assert.equals(Lib.raw("global.page_unlocked[Book.LAND]"), [1,0,0,0,0,0,0]);
        Lib.raw("save_system_read_page(Book.LAND,0)");
        Assert.equals(Lib.raw("global.page_read[Book.LAND]"), [1,0,0,0,0,0,0]);
        Lib.raw("save_system_load_data()");
        Assert.equals(Lib.raw("global.page_unlocked[Book.LAND]"), [1,0,0,0,0,0,0]);
        Assert.equals(Lib.raw("global.page_read[Book.LAND]"), [1,0,0,0,0,0,0]);

        FileSystem.deleteFile(Lib.raw("SAVE_FILE"));
        if (FileSystem.exists("_BACKUP")) {
            FileSystem.rename("_BACKUP", Lib.raw("SAVE_FILE"));
        }
        Lib.raw("save_system_load_data()");
    }
}