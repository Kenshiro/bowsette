package tmodules;

import gml.Lib;
import gmtest.TestModule;
import gmtest.Assert;

@:keep
@:nativeGen
class TMNewGame implements gmtest.ITestModule {
    public final name = "New Game";
    public function new() {}
    public dynamic function runTests():Void {
        Assert.equals(Lib.raw("global.diamonds"), 0);
        Assert.equals(Lib.raw("global.level"), 1);
        Assert.equals(Lib.raw("global.exp_count"), 0);
        Assert.equals(Lib.raw("global.live_count"), 5);
        Assert.isFalse(Lib.raw("global.checkpoint"));
        Assert.isFalse(Lib.raw("global.game_over"));
        Assert.isFalse(Lib.raw("global.night"));
        Assert.isFalse(Lib.raw("global.__cutscene"));
        for (i in 0...4) {
            Assert.isFalse(untyped __raw__("global.switch_enabled[{0}]", i));
        }
    }
}