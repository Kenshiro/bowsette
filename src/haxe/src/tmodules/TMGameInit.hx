package tmodules;

import gmtest.ITestModule;
import library.Manager;
import ui.Option;
import ui.Menu;
import gml.Assets;
import gml.RawAPI;
import gml.Lib;
import gml.Scripts;
import gmtest.TestModule;

@:keep
@:nativeGen
class TMGameInit implements ITestModule {
    public final name:String = "Init";
    public function new() {}
    public dynamic function runTests():Void {
        gmtest.Assert.isTrue(Lib.raw("(global.current_course == game_get_course('Brand New Grassland'))"), "First course should be Brand New Grassland!");
    }
}