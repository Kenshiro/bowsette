package tmodules;

import gmtest.ITestModule;
import gml.Lib;
import gmtest.TestModule;

@:keep
@:nativeGen
class TMRelease implements ITestModule {
    public final name = "Release Mode";
    public function new() {}
    public dynamic function runTests():Void {
        gmtest.Assert.equals(Lib.raw("live_enabled"), false);
        gmtest.Assert.equals(Lib.raw("DEBUG_BUILD"), false);
    }
}