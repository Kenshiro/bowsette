# Super Bowsette Beta 3 Roadmap

This document describes our roadmap for **Beta 3**, which is planned to be released on September 2020.

## New features

Beta 3 will include many new features, including (but not limited to) new courses, new gameplay mechanics, redesigned UI, new collectibles, and many others.

## Milestones

Those are the milestones we must complete for Beta 3:

* Course 7  
* Course 8  
* Course 9  
* Signette
* New UI
* Gameplay updates

Each of them will be broken into smaller tasks, tracked as issues on this repository.

## Schedule

The schedule for each milestone can be found [here](https://gitlab.com/Kenshiro/bowsette/-/milestones).