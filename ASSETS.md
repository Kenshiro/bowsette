# Asset Design Guidelines

This document contains the asset design guidelines for Super Bowsette. They are meant to:

- Guide artists who want to contribute.
- Help us identify assets that don't fit this project.
- Guarantee that the game has a cohesive look and feel, and no element feels out of place.

These guidelines are meant to be applied to **concept art, game graphics and audio assets**.

## Concept Art
All concept art should take into consideration the keywords defined in the [Game Design Document](GDD.md).

## Game Graphics

Super Bowsette follows a 8-bit art style. In practice, this means that any graphic asset should look like they're from a NES-era game.

If you make a sprite for a game element, **you should try to keep the color palette low, so that they would fit a NES game**. Each sprite can have up to 3 colors, including a black outline. Those colors should come from the palette below:
<div align="center"><img src="images/nes_color_palette.png" /></div>

Notice that there is no palette limitation for multiple elements. That means it is okay to have many objects on the screen with different colors.

When making a background, makes sure they are easy on the eyes and that they "loop". Multiple layers for parallax effects are welcomed, but not required.

Fonts do not have any limitations, but we encourage cartoonish looking fonts.

HUD, menu and other elements have the same limitations as sprites.

For inspiration, we recommend looking at **Super Mario Bros. 2**, **Super Mario Bros. 3** and **Super Mario World**.

## Audio

Audio effects have more or less the same limitations as sprites: there's no limit to bitrate or anything, but they should sound like they're from 
a NES game.

Songs should either have a loop point (so they can be played endlessly) or be considerably long (3+ minutes).

For inspiration, consider looking at **Super Mario Bros. 3** and **Super Mario World**.
