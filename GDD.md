<div align="center"><img src="images/GDD.gif" /></div>

This is the Game Design Document for Super Bowsette. It's currently under heavy development (both the game and this document), so any contributions are welcomed.

Certain parts of this document are split into sub-files. You can see all of them under `/docs/`.

Below are a few pages that are worth taking a look:

 * [Power Crowns](docs/POWER_CROWNS.md)  
 * [Setting](docs/SETTING.md)

# Core Concepts

Super Bowsette is a platform game with traditional gameplay. You control Bowsette and can move her in a 2D plane, jump, fluctuate and breath fire balls. Its main characteristics are platform gameplay, Basic RPG elements (level up, attack damage, items, Health Points, etc.) and simplicity.

You make progress by finishing courses, usually by getting to the end of them. Currently, there are 15 courses planned.

The player dies by losing all their Health Points. If they lose all their lives, it is game over and you have to start again.

# Look and Feel

The game is meant to have a cutesy art style. It uses the NES palette for sprites, and most of them work with the same color count limitation that the NES have. However, the game looks more modernish in comparison to NES game due to the unlimited amount of colors that can be on the screen at the same time.

Gameplay wise, it's feels somewhat like *Super Mario World*, but with some *Megaman*-inspired quirks. Controls are meant to be extremely responsive. If the player has the reflex to do something, this should reflect in-game.

We attempt to make the game and its assets while staying true to the following keywords:
 
- *simple*  
- *comfy*  
- *curious*  
- *cute*

# Project Scope

The game is meant to be relatively short. It is planned so that a skilled player could beat the final version of Super Bowsette under an hour. This does not account for speedruns.

There is a single world map that is divided between 3 competing species: humans, youseis and youkais. Each portion of the map has between 3 and 6 courses, ignoring bonus courses.

Our goal is to make Super Bowsette a short, but challenging platform game with quality and polish comparable to bigger, commercial projects. We adjust our scope and resources to keep in line with this vision.

## Game Elements

### ![Enemies](images/red_fairy.gif) Enemies

Most enemies behave like you expect enemies to behave in platform games: they damage you if you touch them, and you damage/kill them by jumping on top of them. You can also breath fire to damage/kill them.

Everyone course, except the first one, has a signature enemy that the player only meets on that course.

### ![Enemies](images/surprise_block.gif) Surprise Blocks

When you hit them on the bottom, Surprise Blocks give you an item and become empty.

### ![Mushroom](images/mushroom.png) Mushrooms

There are three type of Mushrooms that aid Bowsette by giving her HP, EXP or an extra life. They usually come out of Surprise Blocks.

### ![Fire Crown](images/fire_crown.gif) Power Crowns

The Power Crowns transform Bowsette upon touching her, giving new abilities to the player. These powers affect her floating ability (either by improving it or replacing it with a new ability).

More information can be found [here](docs/power_crowns.md).

### ![End Course Sign](images/end_course_sign.png) *End Course* Sign

It's a sign that marks the end of a course. After touching it, it'll turn green, and you'll be taken to the next course.

### ![Diamond](images/diamond.png) Items

The player may collect items as he goes through courses. Items are kept on Bowsette's inventory. Some of them are consumables, and others may be traded with NPCs to unlock new paths.

## Courses

Each course has its own unique thematic. Below, you can find each course's name, themes, exclusive enemy and completion status. Note that this list changes with time. Make sure you have an updated version of the GDD!

### Course 1: Brand New Grassland 
![Course 1](images/course1.png)  
*"Take it easy, R-E-L-A-X. Wanna have something to drink?"*

 
**Themes:** grassland, introductory level  
**Exclusive enemy:** none  
**Completion status**: 100%  

Collecting all coins unlocks a pipe that takes you to the Yellow Switch Palace.

### Course 2: Mysterious Cavern 
![Course 2](images/course2.png)  
*"I should've brought a Super Repel."*
 
**Themes:** dark cave, bats  
**Exclusive enemy:** ![Zoo Bat](images/zoo_bat.gif)  
**Completion status**: 100%

### Course 3: Princess in the Sky with Diamonds 
![Course 3](images/course3.png)  
*"Don't worry, these diamonds are unbreakable!"*

**Themes:** clouds, moving platforms, vertical movement  
**Exclusive enemy:** ![Red Fairy](images/red_fairy.gif)  
**Completion status**: 100%

Collecting four diamonds in this course you allow you to go to the Red Palace.

### Course 4: Stay the Night  
![Course 4](images/course4.png)  
*"On the night that left me scarred, you saved my life"*  
**Themes:** night course    
**Exclusive enemies:** ![Spooky](images/spooky.gif) ![Mage Clown](images/mage_clown.gif)  
**Completion status**: 100% 

### Course 5: The Fragrance of Black Coffee
![Course 5](images/course5.png)  
*"Blacker than a moonless night, hotter and more bitter than hell itself.#That is coffee."*    
**Themes:** autumn, leaves, calm, relaxing  
**Exclusive enemy:** ![Araikuma](images/araikuma.gif)  
**Completion status**: 100% 

### Course 6: Vanilla Ice
*"Beware the miasma of the void... Vanilla Ice."*    
**Themes:** ice land, ice enemies  
**Exclusive enemies:** Ice Servant, Snow Bunny  
**Completion status**: 50% 

* This course was published in Beta 2, but will be extended for Beta 3.

### Course 7: Sweet Sweet Vibrancy 
**Themes:** candies, monsters 
**Exclusive enemy:** TBD  
**Completion status**: 15% 

* An exclusive enemy hasn't been thought of yet
* Ideas are welcomed  

### Course 8: Rainy Grassland Course
**Name**: TBD  
**Themes:** rain  
**Exclusive enemy:** TBD  
**Completion status**: 0% 

* A tileset/visual reference hasn't been done yet
* An exclusive enemy hasn't been thought of yet
* Ideas are welcomed 

### Course 9: Desert Course
**Name**: TBD  
**Themes:** desert  
**Exclusive enemy:** TBD  
**Completion status**: 0% 

* An tileset/visual reference hasn't been done yet
* An exclusive enemy hasn't been thought of yet
* Ideas are welcomed 

### Course 10: Vertical Course
**Name**: TBD  
**Themes:** fully vertical course  
**Exclusive enemy:** TBD  
**Completion status**: 0% 

* An tileset/visual reference hasn't been done yet
* An exclusive enemy hasn't been thought of yet
* Ideas are welcomed 

### Course 11: Lavafield Course
**Name**: TBD  
**Themes:** lava field, final level  
**Exclusive enemy:** TBD  
**Completion status**: 0% 

* An tileset/visual reference hasn't been done yet
* An exclusive enemy hasn't been thought of yet
* Ideas are welcomed

### Course Ideas
Course ideas are still under discussion and may or may not be added later on.
Here are a few themes that could be used for courses:

* Rainbow
* Underwater

## Library
The game features a collection of "books" which can be accessed through the pause menu. Those books contain lore information related to in-game locations, creatures and key locations.
Those books expand gradually as the player collects pages on each course.