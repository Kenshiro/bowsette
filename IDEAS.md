![Ideas](images/others/ideas.png)

Here, we track ideas for games related to Bowsette and the Super Crown. Feel free to start working on them, or contributing to existing initiatives.

If you started something, we encourage you to find more people to work with. You could also submit your own ideas and see if someone gets interested.

## Related to Super Bowsette

### Power Up ideas

There was feedback that suggered we put power ups in the game. Currently, there's a level system, but no power ups are attached to it yet. They could also be items and work like in most Mario games (that is, you lose them if you get damaged).

#### Ice Power Up

Someone suggested that, for making clones of yourself, we use an Ice-themed power up.

### Course 4: Stay the Night

* Firefly as a thematic enemy. Movement and attack patterns are TBD. They bright up a small circle around them.  
* Lightning effects for fireflies, maybe torches too.

## Other works

An anonymous person wrote a very detailed text describing a Bowsette-themed Bullet Hell. You can find it [here](others/BOWHOW.md). 