![Super Bowsette](images/logo.png)

This is the repository for **Super Bowsette**, a game made with Game Maker: Studio 1.4. We also host ideas and information about other games and projects related to Bowsette and the Super Crown.

For more information about this game, check our [Game Design Document](GDD.md).

For more information about other projects involving Bowsette, check [this file](OTHERS.md).

If you wish to contribute, first check our [Contributing](CONTRIBUTING.md) page. Pull requests are welcomed.

## Releases

The latest release is Beta 2, released on September 19th, 2019. You can get up-to-date builds on [itch.io](https://bowsette-game.itch.io/super-bowsette).

💛
