# Bowsette Bullet Hell(Bowhow)

## Story Ideas
 -Mario and co get kidnapped by [Dark Bowsette/RPG Villain], so its up to Bowsette/Booette to
save their husbandos.

 - Dark Bowsette kidnaps the Mario brothers, forcing Peach to run to Bowsette for help. One of
Dark Bowsette's minions steal the koopacopter and angers Bowsette, who grabs a paratroopa shell and goes off to kick Dark Bowsette's ass. Queen Boo also tags along, as she has a painting with Luigi's name on it. Peach follows along to make sure they don't cause too much damage to the kingdom(to little avail).

 - Mario and Luigi get kidnapped by a villain, who does so to kneecap the Mushroom Kingdom's
defenses. Peach and Daisy try to save them, soon joined by Bowser and Boo. The weapons and
power-ups only work with crown magic, forcing Bowser and Boo to turn into Bowsette and Booette. One pair goes to save the brothers, while the others go head-to-head with the villain's forces.

## Combat
 -Standard forward firing attack with LMB; can be changed into spread, charged, or homing
attack based on power-up/customization. Bowsette shoots a stream of fireballs for her normal
attacks, while Booette shoots orbs or will-o-wisps(small, blue fireballs).

 - Siege mode with RMB, which has effects depending on the character:
 
  - Bowsette has her movement and attack speed slowed, in return for higher DPS(slower, but
much harder hitting fireballs). This also allows for precision movement against massive danmaku patterns, and gives her good damage against bosses.

  - Booette has temporary invisibility and increased movement speed, allowing herself to avoid damage and move to a better position. A shotgun effect of danmaku is dealt once she regains visibility, making it a good screen wiper against normal enemies. This means that she has a good matchup against danmaku-heavy bosses, but struggles against more precision-based ones.
  
  - Peach has a double shot, with a slower rate of fire, and has a slight movement
debuff. Essentially a miniature version of Bowsette's siege mode.

  - Daisy potentially has a bullet-time effect, where the enemies' bullets are slowed by up to half. Her own movement is reduced even more than the bullets, leaving her siege mode to be used when overwhelmed and in need to plan out a safe path.

 - Spread & homing attacks deal 1/2x damage per maku, while charged deals 2x damage at the cost of lower fire-rate. Alternatively, spread can deal the same damage, but have a wide cone of fire, dealing much less damage at range, but more at closer distances.

  ### Specials
  
 - Bowsette: Flings a giant chain chomp(ette) forwards that clears the screen of danmaku and
deals heavy damage. Travels just as fast as her normal shots.

 - Booette: Fires a bullet clearing boo bomb forwards and also launches two boo missiles from
her side that homes in on enemies. Often clears a "W" shaped path into danmaku against bosses.

 - Peach: Uses a toad as a shield that protects her from bullets for a short period of time.

 - Daisy(If implemented): Spin dodges to the opposite side of the screen, launching many bullets that negate the enemies' bullets. Invulnerable while spin dodging.

## Enemies
### Reoccurring  
 - Bullet Bills: Flies in a straight line either downwards from the top of the screen or
sideways from the left or right of the screen.

 - Missile Bills: Flies in from a missile port and homes in on the player. Continually respawns until port is destroyed or out of view.

 - Gusties: Moves at high speeds in a straight line, their path possibly being foreshadowed by
wind effects in the background.

 - Flutters: Moves in a fast serpentine pattern from the sides of the screen.
 
 - Shroob UFOs: Moves in zig-zags and launches blasts at the player. Hangs around for a minute, where it will fly off if not killed. Drops power-ups.

 - Parabones: Flies slowly in a serpentine fashion, throwing bones at the player.

 - Dark Parabones: Flies slowly in a serpentine fashion, firing either fireballs or bones at the player.

 - Paratroopas: Flies in a serpentine fashion as above, instead having no projectile and
charging the player if they get close enough.

 - Fire Parabroette: Hangs around near the top of the screen and fires several fireballs at the player. Will occasionally gain a burst of speed to avoid attacks.

 - Lakitus: Moves in random patterns, throwing spike balls at the player.

 - Toadies: Encountered in straight lines, moving to the side or moving diagonally across the
screen. Fires single bullets at the player.

 - Fuzzies: Stays still or moves in back-and-forth patterns. While invisible to the player,
taking damage from them will restore their health.

 - Fuzzette: Moves around the screen in straight patterns, sometimes charging the player. As
with normal fuzzies, they will be able to drain health from the player.

 - Floating Fuzzies: Moves in a serpentine pattern, often encountered in groups. Interferes with the player's perception when touched.
 
 - Cheep Cheep & Blurps: Moves in circles or across the screen sideways.

 - Bloopers: Moves in down-diagonal patterns as they do in 2d Mario games.

 - Blooper Nannies: Same as above, except smaller bloopers circle around them.

 - Torpedo Tedettes: Hangs around the top of the screen and fires 2-4 torpedo teds at the
player, before charging at them.

 - Podoboos: Move in a back-and-forth pattern. Invisible and has no collision when moving back to its original position(indicating that it is in lava).

 - Pyrosheres: Wisps back and forth above lava/ground. 

### Mid-Bosses 
*Differentiated from real bosses as they only have a single phase and health bar.*

Bowsette's Encounters:

#### Big Wrigglette

Encountered in the Foreboding Forest. Moves underneath the screen and uses her flower to fire danmaku at Bowsette. Will occasionally move along the side of the screen while firing danmaku, allowing Bowsette to damage her.


#### Mecha-Chompette  

Encountered in the Rocky Ruins. Has a central head and two hands that can be
attacked. Head fires lasers in a timed manner, aimed directly at Bowsette. Hands shoot chain
chomp homing missiles. Destroying the head immediately ends the fight, but the hands have much less health and can be destroyed to stop the missiles from being fired.

#### Tornadette

Encountered in the Dusty Desert. Moves in an erratic manner around the screen,
picking up and throwing around pokeys as the background moves. Moves Bowsette's fireballs in an arc towards her, so aiming must be compensated for to land shots.

#### Golden Fuzzette
Encountered in the Sky-high Sky. Floats around the screen, before charging at Bowsette. Can also fire shotgun-spread danmaku blasts. Summons in Fuzzettes, though they will run away when Golden Fuzzette is defeated. Even though it's a Fuzzette, it won't suck up your HP, which is nice.

#### Sledge Broette
 Encountered in the Lava Lake. Slams random areas on the ground to make a shockwave of danmaku. The area struck by her sledge will fill with lava temporarily, damaging Bowsette if she moves over it. Will jump around the screen as well, creating smaller danmaku shockwaves, and can throw her sledge at Bowsette to create a large projectile.

#### Dark Drybonette 
Encountered in the Dark Dungeon. Throws bones that bounce along the screen's edges, and can summon parabones to attack Bowsette.

## Levels & Bosses
  *´I´ indicates boss phases.*

### Bowsette Stages
  
#### 1. Mushroom Kingdom/Castle Level
 No environmental hazards. Has Bullet Belle as the first boss of the game.
 
  I. Bullet Belle sends in bullet bills and occasional missile bills from the top and sides.
After they are cleared, she rushes in herself, allowing the player to damage her.

  II. Bullet Belle drops the back-up idea, and fights solo. In addition to charging, she can
now fly backwards towards the top of the screen and fire off fireballs from her engine that
bounce off the sides of the screen for a few seconds. This gets the player used to Bowsette's
Siege mode.

#### 2. Foreboding Forest
 Treetops occasionally block view. Has Fluttette as the boss.
 
  I. Fluttette moves around erratically, occasionally moving towards Bowsette, but having a
tendency to stay near the middle of the screen. She can rarely fire single-line danmakus.

  II. Fluttette moves to an area where the top portion of the screen is blocked by treetops.
She will hide in the treetops and send in waves of flutters to fly at Bowsette. After they are defeated, she will come down and fire single-line danmaku at Bowsette, this time using them more frequently. After several seconds, she'll move back into the treetops. Rinse and repeat.

#### 3. Rocky Ruins
 Rock arches/formations as environmental blocks. Chompette as the boss.
 
  I. Chompette keeps a running pace near the top of the screen. She will jump back to bite or
move to the side and jump off of the rocky walls to send boulders flying back, before returning to her position near the top. Very rarely, she will jump to the side and jump straight at Bowsette.

  II. The stage now enters a dead end. Now unable to run, Chompette shows the limit of her
acrobatic abilities. She will jump off of walls at Bowsette, before stopping to rest for a
second. She will also toss stone chunks at Bowsette, and can also use her chain as a whip to
deter Bowsette from getting to close. Near the start of this phase, Chompette will move rapidly and combo together her moves, but as her health drops, she will start to rest more often and move slightly slower. After the battle, Bowsette's special is unlocked.

#### 4. Dusty Desert
 Pokies as environmental hazards, intense sunbeams also occasionally razes the area in linear paths, damaging both Bowsette and enemies. Fire Parabroettes appear as mini-bosses. Has Angry Sunette as the boss.

  I. A Fire Parabroette appears and hangs around the top of the screen for a few seconds,
before being shot down by one of the intense rays from earlier. The screen slowly becomes
brighter, as a glimpse of Sunette is seen before the screen flashes to white. Bowsette is moved near the bottom of the screen, and, as the screen returns to normal, Sunette moves to the exact middle of the screen. Boss fight commences, with Sunette moving away from Bowsette's standard line of fire and swooping down in a "U" shaped fashion to attack. She will also occasionally fire off a small volley of fireballs.

  II. Sunette moves towards the top of the screen, and starts to constantly send a flurry of
fireballs downwards. She will move slowly to position herself directly above Bowsette, and can sometimes shoot a beam of fire straight forwards. As her health depletes, she will start to move much faster, and extra fireballs start to come out of her fire beams.

  III. Sunette powers up SSJ-style, and moves to the center of the screen. Fire starts to come out of the ground in straight lines away from her, and are shown to be telegraphs for six massive fire beams which linger in place for a few seconds. Afterwards, the beams start to rotate in a clockwise fashion, keeping Bowsette in a space between two of them. Occasionally, the beams will stop for a few seconds and change directions. Fireballs occasionally spew out from the beams, and are absorbed if they contact another beam. Near the end of this phase, Sunette stops the fire beams and starts to move erratically across the screen. She will charge up her attacks, and launch single fire beams directly at Bowsette, shaking the screen once it hits a side and sending out a shockwave of fireballs back towards the middle.

#### 5. Sky-high Sky
 Clouds limit player movement. Fuzzette appears several times as a mini-boss. Has Lakitette as the boss.

  I. Lakitette floats a constant distance away from Bowsette, getting thrown spike balls from
the side of the screen that she uses to hurl at Bowsette. The spike ball bounces twice off of
the sides of the screen, before breaking into spikes that fly out from its middle. Two spike
balls are thrown in occasionally, having her catch one and hit the other one before throwing
the one she caught. The first spike ball will break at the same time as the second one,
regardless of how many times it bounces.

  II. Four lakitus are summoned in, and spike balls are tossed towards them now, before they
either throw it at Bowsette or toss it to another lakitu. If Lakitette gets one, she will toss it immediately and it will act as it did in the previous phase, while if a normal lakitu throws it, it will fly off-screen and be replaced immediately. Towards the end of the phase, more spike balls are tossed in to multiple lakitus, even while Lakitette's spike ball is bouncing, forcing Bowsette to keep track of each one. During the phase, the normal lakitus can be defeated, taking a few seconds to be replaced.

  III. The lakitus move to the bottom of the screen, unable to be attacked by Bowsette. They
will get spike balls tossed to them, and toss it in turn to Lakitette. Lakitette will then bat it towards Bowsette and have it bounce around the screen as they did in prior phases. Multiple spike balls can be tossed to Lakitette at once, having multiple spike balls with unrelated bounce counters. If Lakitette sustains enough damage, she will start to fall from her cloud, signalling a lakitu to fly in to rebalance her. In this time, Bowsette can attack and defeat the lakitu, with them now not being replaced throughout the rest of the battle. Once all the lakitu are defeated, the battle returns to how it was in phase one, except Lakitette will fall off her cloud into the waters below once she takes enough damage.

#### 6. Open Ocean
 EEL occasionally blocks view. Bowsette's fireballs will curve in arcs a few seconds after being fired, affecting precision. Has Unagette as the boss.
 
   I. Unagette maintains her distance above Bowsette to survey her abilities, only occasionally approaching closer and biting. She will occasionally charge at Bowsette, stopping out of view from the camera and circling around to back to the top of the screen. She will more often shoot water bubbles, giving the player many opportunities to take advantage of Siege mode.

  II. Unagette fires off and maintains two streams of water near the sides that stop Bowsette
from moving too far from the middle. Unagette now starts to charge more frequently, making
straight charges vertically across the screen. She will sometimes stay at the top and fire
fast-moving jets of water in succession. Towards the end of the phase, Unagette can cancel one of her charges and redirect herself towards Bowsette, charging again even faster than before.

  III. Bowsette is lured into a underwater tunnel. The battle's pace starts to kick up, with
the tunnel's narrow space and frequent curves essentially forcing the player to use Siege mode. Unagette will now dart into caves off to the side, and charge out of the next one Bowsette flies past, being vulnerable when moving back to the top of the screen. She will occasionally snake down the tunnel, giving Bowsette little room to dodge. Using Bowsette's special will stop Unagette in her tracks and allow a counterattack.


#### 7. Lava Lake
 Lava serves as environmental hazard. Has Czarette as the boss.

  I. Czarette appears from the lava with a Gaooon! and flies up to the top of the screen. She
will summon pyrosheres that move in a circular pattern around her, also slowly moving back-and-forth towards and away from her as they do. She can also cloak the area in diagonal flames, first telegraphed by sparks. Every once in a while, she'll fire off a volley of fireballs. She will occasionally move in closer to the middle, using the pyroshere's movement pattern to attack Bowsette.

  II. Czarette will absorb the pyrospheres, which cloaks the majority of the area in lava
except for a circle around Bowsette. She will then fire off water blasts that temporarily
replace the lava  with solid rock, with the lava slowly returning around a circle elsewhere
along the blast's path. Czarette will also fire off a breath of flame instead of water, which
will not alter the lava. She can sometimes charge in directly at Bowsette, firing fireballs
from her side as she does.

  III. Czarette will create six beams of fire as Sunette did. Unlike Sunette, Czarette can stop the beams and immediately shoot a single charged beam at Bowsette, before creating the six beams again. Once Czarette starts to lose enough health, she will be able to slowly move around the screen even while the six beams of fire are rotating.

  IV. Czarette dips into the lava, depleting the entire lava lake and returning as Zombette. In this phase, she will move much more quickly, and can summon forth dark parabones. Using the lava she absorbed, she can fire off waving beams of lava that leave temporary paths of lava in their wake. Zombette can also fire off telegraphed lines of lighting, first sending out blue bolts to the side of the screen, and then sending the damaging payload once they connect to the side. Zombette will also use the diagonal flames from phase one, except now she can cross two instances of them over each other. Zombette will also send a swarm of boulders from the top-right of the screen down, which will be used more often as she loses health.
  

#### 8. Dark Dungeon
 Pillars serve as environmental blocks; fires serve as environmental hazards. Chandeliers occasionally block view. Has Dark Bowsette as the boss.

--Booette will potentially have extra stages, or will instead be a pickable character from the start.
