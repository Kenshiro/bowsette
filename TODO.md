<div align="center"><img src="images/to_do.gif" /></div>

## Missing content

These should be available for the next release.

### General

* Add Course 7: Sweet Sweet Vibrancy to the game
* Add Courses 8 and 9
* Add Inventory System
* Add shops
* Add Signette
* Add Ice Crown

## Improvements

These should be improved for the next release.

### Graphics and Aesthetics

* Update P-Button sprite to better match the rest of the game
* Update HUD to for inventory system

### Audio and Sound Effects

* Add sound effects for Owl attacks
* Add sound effects for Bunny jumps and hat attack