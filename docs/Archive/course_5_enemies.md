# Concepts
This document contains a brief description of how each enemy conceived for [**Course 5: The Fragrance of Dark-Colored Coffee**](../GDD.md) should work.

## Bear
<img src = "../concepts/enemies/Bear.png" />

The Bear enemy (name TBD) will stand still until the player gets close. Once it does gets close, it will pursuit the player to get into melee range and attack with its claws.

If the player attempts to jump on top of it, the Bear will do an uppercut attack and damage the player instead.

* It can jump small obstacles
* It takes 5 fireballs to kill.

## Owl
<img src = "../concepts/enemies/Owl.png" />

The Owl enemy (name TDB) will stand still on a tree branch until it spots the player. Once it sees them, its eyes will look towards the player and attack with a beam of some sort. If the player walks below it, it will go down in an attempt to attack it before going outside the screen.

* Can be killed by a fireball or by jumping on top of it.
* Beam is a single beam, not a continuous shot
* How it should look is up to the artist

## Butterfly
<img src = "../concepts/enemies/Butterfly.png">

The Butterfly enemy (name TDB) will have a typical movement pattern, moving horizontally while in the air. It may attack the player by releasing pollen when they are close enough. Upon contact with the pollen, the player gets slowed down and has its jump height halved for some time.

* Can be killed by a fireball or by jumping on top of it.

## Basic Enemy
<img src = "../concepts/enemies/Basic Enemy.png"> 

This basic enemy (name TBD) will replace the basic enemies in [**Course 5: The Fragrance of Dark-Colored Coffee**](../GDD.md). No special behavior intended.

* Can be killed by a fireball or by jumping on top of it.


## Grass Bunny
<img src = "../concepts/enemies/Grass Bunny.png">

The Grass Bunny enemy (name TBD) will replace the grass enemies already implemented in [**Course 5: The Fragrance of Dark-Colored Coffee**](../GDD.md). Their behavior is exactly the same.

* Can be killed by a fireball only
* Graphics for the grass pile are needed