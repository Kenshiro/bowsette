# Quality Assurance and Processes

This document briefly describes how we quality check new features added to Super Bowsette. A "feature" could be a new functionality, a new enemy, a new course, or anything that adds value to the game.

One of the goals of Super Bowsette is to keep the production value as high as our team manages to achieve. So, between lot of unfinished/unpolished features and a small set of tested, high quality features, the latter is more desired.

## Assuring quality when adding a feature

How you evaluate whether the feature has enough quality to be included depends on the nature of it. Below are some checklists that can be used for common features.

Note that a feature doesn't exist alone. A single, high quality feature may be rejected because it does not blend well with existing features. This is emphasized on every check list.

### General
* Make it good looking;
* Make it responsive;
* Make it react to player input;
* Make it testable - know how it should work beforehand, and test edge cases!
* Keep it **simple**.

### New Enemies

When adding a new enemy, it's important to make sure it...

* Has a movement/attack pattern that the player can learn (it can't be completely random)
* Each action the enemy takes has a sound effect and a sprite change that goes with it.
* All its sprites follow the NES palette and its rules
* They match the course thematic

### New Songs

The criteria for songs are simple:
 
 * The song must be 8-bit and have a retro vibe. Extra, non-8bit sound effects on it are **allowed**.
 * The song must match the place they're being placed. A song that doesn't fit anywhere won't be used, even it's really good.
 
### New Courses

Courses have pretty clear requirements for being added:

* They **must** have a unique theme, not used by previous courses. If they share a similar setting, then they must have a gimmick that makes them different. For example, Brand New Grassland and Stay the Night share the same tileset, but the second one is a night course and is structured very differently.
* They **must** have at least one unique enemy that does not appear anywhere else. 
* They **must** have at least one unique theme song. A course can use more than one song: for example, Vanilla Ice has a night version of its theme.
* They either must be at least 13000 pixels wide, *or* have a lot of vertical movement as the player moves towards the end. 