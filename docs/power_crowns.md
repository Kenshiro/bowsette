<div align="center"><img src="../images/power_crowns.png" /></div>

**Power Crowns** are strong Power Ups that Bowsette can pick up. They will change her floating ability and fire skill to completely different move sets, depending on which Power Crown you use.

They can be unlocked by completing the respective Palace to each Crown. The only exception is the Thunder Crown, which a way to unlock it is still to be decided.

### ![Fire Crown](../images/fire_crown.gif) Fire Crown

The Fire Crown becomes available after you beat the Red Palace, which can be reached by collecting all four [Diamonds in Course 3: Princess in the Sky with Diamonds](../GDD.md#course-3-princess-in-the-sky-with-diamonds) and then beating [Course 4: Stay the Night](../GDD.md#course-4-stay-the-night). Touching it will turn you into **Fire Bowsette** ![Fire Bowsette](../images/fire_bowsette.gif), which has the following powers:

- Float skill replaced with a Double Jump + Gliding ability
- Increased fire ratio
- Spin middle air to throw fire balls to both sides

### Ice Crown
TBD

### Thunder Crown
TBD