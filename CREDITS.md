# Credits
Here we credit most of the people who contributed to Super Bowsette. This project wouldn't be possible without them and we are thankful for their work. Certain contributors chose to stay anonymous, so their online handles are not listed here.

Most of these people did not make assets specifically for this game - rather, they shared them on websites like [OpenGameArt](https://www.opengameart.org), [itch.io](https://itch.io) or [MFGG](https://mfgg.net). Some assets they made might have not been used in actual builds, but were used for early development.

As the credits list kept growing, we made the mistake of not attributing an asset to its author. So, unfortunately, we can not pinpoint who did what in this list. This was unintentional.

If you made a contribution and want your name listed here, please create a new issue. If you have not contributed yet but wishes to do so, check [CONTRIBUTING.md](CONTRIBUTING.md).

## Credit List

This list is maintained in alphabetic order.

* ansimuz
* Ashiato
* Ashiato
* AwesomeZack
* ayyk92
* BM44
* Cethiel
* Clint Bellanger
* CodeManu
* DamariomanII
* fireluigi
* Flagpole1up
* gmlscripts.com
* itch.io
* JoeyTheRavioli
* JujuAdams
* Kazufox
* Lyn
* Marvin
* mfgg.net
* NO\_Body\_The_Dragon
* opengameart.org
* Pixelated Pope
* scottarc
* Tadato Miller
* TheChillFather
* vinizinho
* Wolfgang
* YellowAfterlife