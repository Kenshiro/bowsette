# Building
Super Bowsette's source code is composed of the GMS1.4 project and additional code written in Haxe. If you wish to compile from source, follow these instructions:

1. Clone the repo locally
2. Open the project with GameMaker:Studio 1.4.9999
3. Change the configuration to "Release".
5. The project should compile.

### About the `live_call` and `live_call_ext` functions
They are from a development extension called GMLive, written by YellowAfterlife. Since it's proprietary software, we cannot upload them to the repository. However, Super Bowsette doesn't depend on it for running and removing it is completely harmless.

Changing the configuration to "Release" loads a dummy extension that replaces those functions with empty ones.

### Compiling the Haxe code
By default, the Haxe code should be already compiled to GML and added to the source tree once you clone the repo. If you wish to modify the Haxe code, follow [these instructions](https://yal.cc/r/18/sfgml/#setup) to setup Haxe and the GML compiler. Once everything is done, you should be able to call `haxe build.hxml` inside the `/src/haxe` folder. The compiled code is automatically added as an extension.

Haxe code was tested using the latest Release Candidate of Haxe 4. Compiling with earlier versions is unsupported.