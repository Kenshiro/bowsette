![Other Projects](images/others/other_projects.png)

Here, we save and gather information about other projects related to Bowsette and/or the Super Crown. This list was originally compiled by an anonymous contributor on MEGA. [Here is a link to their original PDF](https://mega.nz/#F!cY1WCA4R!lf44OEiPg-oXD4DpMfuBLQ?IAczDCJZ).

## Games

### Mega Bowsette
![Mega Bowsette](images/others/mega_bowsette.png)

A mod of Rockman 7 developed by [The Chill Father](https://www.twitter.com/chill_father). Version 1.0 can be downloaded [here](https://www.mediafire.com/file/zho7scs6s60r0ix/Mega+Bowsette+v1.zip). It's still under active development.

### Flappy Bowsette
![Flappy Bowsette](images/others/flappy_bowsette.png)

A clone of Flappy Bird made by an anonymous developer. Can be played inside a web browser. Click [here](https://cachacacha.com/GAME/flappyppa/) to open its web page. As far as we know, this project is finished.

### Bowsette and the Kidnapping Incident

![Bowsette and the Kidnapping Incident](images/others/bowsette_and_the_kidnapping_incident.png)

A game with hand-drawn graphics. It was made by an anonymous developer using Godot. Version 1.0 can be downloaded [here](https://tinyurl.com/y9u9okuy). This project is under active development.